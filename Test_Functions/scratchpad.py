import math
import csv
import Test_Functions.constdict
import copy
from Basic_functions import dump_list_to_csv
from Test_Functions.read_alamanc_file import get_alamanac,get_almanac_from_raw_navbits_new,return_almanac_sequence
from Test_Functions.subframe_creation import ephemeris_to_subframe,raw_nav_bits_to_subframe_structure_new,ephemeris_to_almanac_subframe ,raw_nav_bits_to_almanac_subframe #,add_time_and_parity,

#ephemeris_from_file = get_alamanac(filename = r"..\UPLOADED_ALAMANAC\March1_2019_yuma.txt")
#print("almnac",ephemeris_from_file)

ephemeris_from_keysight = get_almanac_from_raw_navbits_new(filename="/home/cyronics/Documents/Cyronics Projects/gps_parser/RAW_NAV_BITS/RNBB_L1.csv") #parser3 epho to subframe
#print("keysight",ephemeris_from_keysight)
#subframes_from_almanac = ephemeris_to_subframe(ephemeris_from_file)
subframes_from_log = ephemeris_to_subframe(ephemeris_from_keysight)
print(subframes_from_log)
'''
subframes_from_actual=raw_nav_bits_to_subframe_structure_new(filename=r"..\RAW_NAV_BITS\Keysight.csv")
'''

#ephemeris_from_keysight = get_almanac_from_raw_navbits_new(filename="..\RAW_NAV_BITS\Keysight.csv") #parser3 epho to subframe
#print("keysight",ephemeris_from_keysight)
#alamanc_list_4,alamanc_list_5,ephemeris_list,ephemeris4_25,ephemeris5_25 = return_almanac_sequence(filename="..\RAW_NAV_BITS\Keysight.csv")
#ephemeris_data=ephemeris_list[0]
#print("eph",ephemeris_data)

#eph_data_final=ephemeris_to_almanac_subframe(ephemeris_list[0])
#print("alm 4 and 5::",eph_data_final)

subframes_from_raw4,subframes_from_raw5,subframe_struct_raw=raw_nav_bits_to_almanac_subframe(filename="/home/cyronics/Documents/Cyronics Projects/gps_parser/RAW_NAV_BITS/RNBB_L1.csv")
print("nav 4 ",subframes_from_raw4)
print("nav 5",subframes_from_raw5)
print(subframe_struct_raw)




###########subframe
'''def compare_subframes(logs=[])  :
    csv_list = []
    n =30
    valid_PRNs = []
    for PRN in range(1,26):
        # CHECK IF ALL LOGS HAVE THE SUBFRAME 1 RECORD. IGNORE IF NOT.
        # IT IS ASSUMED THAT IF DATA IN SUBFRAME 1 data is also in subframes 2,3 .
        valid_PRN_flag = 1
        for log in range(len(logs)):
            if logs[log][PRN][4] == "":
                valid_PRN_flag = 0
        if valid_PRN_flag == 1:
            valid_PRNs.append(PRN)
            if PRN == 8 :
                for subframe_id in range(4,6):
                    for log in range(len(logs)):
                        split_string = [logs[log][PRN][subframe_id][k:k + n] for k in range(0, len(logs[log][PRN][subframe_id]), n)]
                        to_append = [PRN , subframe_id ]
                        to_append.extend(split_string)
                        csv_list.append(to_append)
    return valid_PRNs, csv_list
valid_PRNs,csv_list = compare_subframes(logs=[eph_data_final,subframe_struct_raw])
#print(valid_PRNs)
#print(csv_list)
t=dump_list_to_csv(filename="Output\dump_data_nav.csv",dlist=csv_list)
if t==True:
    print("Done writing excel")
else:
    print("bit Unsuccesful")'''

#########################################
'''csv_dump = []
temp = ["TOWC","ALM_4_Sent" , "ALM_4_Logical" ,"ALM_4_Comment", "ALM_5_Sent", "ALM_5_Logical","ALM_5_Comment"]
csv_dump.append(temp)
for i in range(len(alamanc_list_5)):
    temp = [alamanc_list_4[i]["TOWC"], alamanc_list_4[i]["pid"] ,alamanc_list_4[i]["actual_pid"],alamanc_list_4[i]["Comment"],
            alamanc_list_5[i]["pid"],alamanc_list_5[i]["actual_pid"],alamanc_list_5[i]["Comment"]]
    csv_dump.append(temp)

t = dump_list_to_csv(filename=r"..\Output\almanac_compare.csv", dlist=csv_dump)
if t==True:
    print("Done writing excel")
else:
    print("data Unsuccesful")'''


'''csv_dump=[]
keys=list(constdict.ephemeris_template.keys())
print("keys:",keys)
temp = ["Source"]
for key in keys :
    temp.append(key)
print(temp)
csv_dump.append(temp)
for i in range(1,33):
    if ephemeris_from_keysight[i]!={} and ephemeris_data[i]!={} and ephemeris_from_file[i]!={}:
        file2 = [ "From Almanac Parameters"]
        file1 = [ "From Ephemeris Parameters"]
        file3 = ["From YUMA file" ]
        for key in keys:
            #to_append = ['from almanac']
            #to_append.extend(split_string)
            file1.append(ephemeris_data[i][key])
            file2.append(ephemeris_from_keysight[i][key])
            file3.append(ephemeris_from_file[i][key])
        csv_dump.append(file1)
        csv_dump.append(file2)
        csv_dump.append(file3)
        csv_dump.append([])

t = dump_list_to_csv(filename="dump_ephemeris_list.csv", dlist=csv_dump)
if t==True:
    print("Done writing excel")
else:
    print("data Unsuccesful")'''


'''def compare_subframes(logs=[])  :
    csv_list = []
    n =30
    valid_PRNs = []
    for PRN in range(1,33):
        # CHECK IF ALL LOGS HAVE THE SUBFRAME 1 RECORD. IGNORE IF NOT.
        # IT IS ASSUMED THAT IF DATA IN SUBFRAME 1 data is also in subframes 2,3 .
        valid_PRN_flag = 1
        for log in range(len(logs)):
            if logs[log][PRN][1] == "":
                valid_PRN_flag = 0
        if valid_PRN_flag == 1:
            valid_PRNs.append(PRN)
            if PRN == 8 :
                for subframe_id in range(1,4):
                    for log in range(len(logs)):
                        split_string = [logs[log][PRN][subframe_id][k:k + n] for k in range(0, len(logs[log][PRN][subframe_id]), n)]
                        to_append = [PRN , subframe_id ]
                        to_append.extend(split_string)
                        csv_list.append(to_append)
    return valid_PRNs, csv_list
valid_PRNs,csv_list = compare_subframes(logs=[subframes_from_almanac,subframes_from_log,subframes_from_actual])
print(valid_PRNs)
print(csv_list)
t=dump_list_to_csv(filename="dump_data_nav.csv",dlist=csv_list)
if t==True:
    print("Done writing excel")
else:
    print("bit Unsuccesful")





csv_dump=[]
keys=list(ephemeris_from_file[1].keys())
print("keys:",keys)
csv_dump.append(keys)
for i in range(1,33):
    if ephemeris_from_keysight[i]!={}:
        file2 = []
        file1 = []
        for key in keys:
            file1.append(ephemeris_from_file[i][key])
            file2.append(ephemeris_from_keysight[i][key])


        csv_dump.append(file1)
        csv_dump.append(file2)


t = dump_list_to_csv(filename="dump_data_1.csv", dlist=csv_dump)
if t==True:
    print("Done writing excel")
else:
    print("data Unsuccesful")'''



