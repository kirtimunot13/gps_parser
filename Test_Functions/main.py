from SV_functions import read_alamanc_file,subframe_creation,constdict
from Basic_functions import dump_list_to_csv,compare_almanac_bits


ephemeris_from_file = read_alamanc_file.get_alamanac(filename =r"..\UPLOADED_ALAMANAC\March1_2019_yuma.txt")
print("almnac",ephemeris_from_file)
subframes_from_almanac = subframe_creation.ephemeris_to_subframe(ephemeris_from_file)
print(subframes_from_almanac)

ephemeris_from_keysight=read_alamanc_file.get_almanac_from_raw_navbits_new(filename=r"..\RAW_NAV_BITS\Keysight.csv")
print(ephemeris_from_keysight)
subframes_from_key = subframe_creation.ephemeris_to_subframe(ephemeris_from_keysight)
print(subframes_from_key)

csv_dump=[]
not_equal_data=[]
keys=list(constdict.ephemeris_template.keys())
print("keys:",keys)
temp = ["Source"]
flag=0
for key in keys :
    temp.append(key)
print(temp)
csv_dump.append(temp)
for i in range(1,33):
    if ephemeris_from_keysight[i]!={} and ephemeris_from_file[i]!={}:
        file2 = [ "From keysight Parameters"]
        file3 = ["From YUMA file" ]
        for key in keys:
            #to_append = ['from almanac']
            #to_append.extend(split_string)
            file2.append(ephemeris_from_keysight[i][key])
            file3.append(ephemeris_from_file[i][key])
            if ephemeris_from_keysight[i]['PRN']==8:
                if ephemeris_from_keysight[i][key]==ephemeris_from_file[i][key]:
                    flag=1
                else:
                    if key not in not_equal_data:
                        not_equal_data.append(key)
                        flag=0
        csv_dump.append(file2)
        csv_dump.append(file3)
        csv_dump.append([])
if flag==1:
    print("all data matched")
else:
    print("not matched data",not_equal_data)
t = dump_list_to_csv(filename=r"..\Output\dump_ephemeris_list.csv", dlist=csv_dump)
if t==True:
    print("Done writing excel")
else:
    print("data Unsuccesful")


valid_PRNs,csv_list = compare_almanac_bits(logs=[subframes_from_key,subframes_from_almanac])
t=dump_list_to_csv(filename=r"..\Output\dump_data_navalm.csv",dlist=csv_list)
if t==True:
    print("Done writing excel")
else:
    print("bit Unsuccesful")