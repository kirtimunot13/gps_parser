from SV_functions import constdict,gps_constants as constants,read_alamanc_file
from Basic_functions import binary,twos_complement,dump_list_to_csv,binary_data,tows_binary_data
import pandas as pd
import copy

def subframe_3and4_parser_irnss(subframe4):
    ephemeris_template = copy.deepcopy(constdict.ephemeris_template)
    ephemeris_template["TLM"] = (subframe4[0:8])
    ephemeris_template["TOWC"] = binary(subframe4[8:25]) * 12
    ephemeris_template["Alert"] = (subframe4[25:26])
    ephemeris_template["Autonav"] = (subframe4[26:27])
    ephemeris_template["Spare"] = (subframe4[29:30])
    ephemeris_template["msg id"] = binary(subframe4[30:36])
    ephemeris_template["Week Number"] = binary(subframe4[36:46])
    ephemeris_template["e"] = twos_complement(subframe4[46:62]) * constants.POW2_M21
    ephemeris_template["toa"] = binary(subframe4[62:78]) * (2**4)
    ephemeris_template["I_0"] = twos_complement(subframe4[78:102]) * constants.POW2_M23*constants.pi
    ephemeris_template["omegaDot"] = twos_complement(subframe4[102:118]) * constants.POW2_M38 * constants.pi
    ephemeris_template["sqrt_A"] = twos_complement((subframe4[118:142])) * constants.POW2_M11
    ephemeris_template["Omega_0"] = twos_complement(subframe4[142:166]) * constants.POW2_M23 * constants.pi
    ephemeris_template["Omega"] = twos_complement(subframe4[166:190]) * constants.POW2_M23 * constants.pi
    ephemeris_template["M_0"] = twos_complement(subframe4[190:214]) * constants.POW2_M23 * constants.pi
    ephemeris_template["a_f0"] = twos_complement(subframe4[214:225]) * constants.POW2_M20
    ephemeris_template["a_f1"] = twos_complement(subframe4[225:236]) * constants.POW2_M38
    ephemeris_template["PRN_ALM"] = binary(subframe4[236:242])
    ephemeris_template["Inter_Sig_Corr"] = twos_complement(subframe4[242:250]) * constants.POW2_M31
    ephemeris_template["Spare_data"] = binary(subframe4[250:256])
    ephemeris_template["PRN"] = subframe4[256:262]
    return ephemeris_template


def return_almanac_sequence_irnss(filename):
    Keysight_data = pd.read_csv(filename)
    BladeRF_data = Keysight_data.to_dict(orient="record")

    ephemeris_list = []
    almanac_list_3 = []
    almanac_list_4 = []
    Valid_PRNs=[2,3,4,5,6,9]
    if len(Valid_PRNs) > 0:
        Valid_PRN = Valid_PRNs[0]
        ephemeris = copy.deepcopy(constdict.irnss_ephemeris)
        for record in BladeRF_data:
            tempr = {}
            tempr["Bits"] = ""
            for i in range(1, 38):
                tempr["Bits"] += format(record['Raw Navbits – ' + str(i)], '08b')
            tempr['TOWC'] = record['TOWC (corresponding to start of the sub-frame) (s)']
            tempr["Week Number"] = record['Week No (corresponding to start of the sub-frame)']
            tempr["PRN"] = record["PRN"]
            tempr['subframe id'] = record['subframe id']
            #print(tempr)
            if tempr['subframe id'] == 3 and tempr["PRN"] == Valid_PRN:
                mid = binary(tempr["Bits"][30:36])
                temp = {}
                if mid ==7:
                    prn_alm = binary(tempr["Bits"][236:242])
                    temp["Comment"] = "Almanac for SV - " + str(prn_alm)
                    ephemeris_template = subframe_3and4_parser_irnss(tempr["Bits"])
                    ephemeris_template["PRN"] = prn_alm
                    ephemeris[prn_alm] = ephemeris_template
                    temp["PRN_ALM"] = prn_alm
                else:
                    temp["Comment"] = tempr["Bits"]
                    temp["PRN_ALM"] = "NA"
                temp["mid"] = mid
                temp["PRN"] =tempr["PRN"]
                temp["TOWC"] = tempr['TOWC']
                almanac_list_3.append(temp)

            if tempr['subframe id'] == 4  and tempr["PRN"] == Valid_PRN:
                mid = binary(tempr["Bits"][30:36])
                temp = {}
                if mid == 7 :
                    prn_alm_4 = binary(tempr["Bits"][236:242])
                    temp["Comment"] = "Almanac for SV - " +str(prn_alm_4)
                    ephemeris_template = subframe_3and4_parser_irnss(tempr["Bits"])
                    ephemeris_template["PRN"] = prn_alm_4
                    ephemeris[prn_alm] = ephemeris_template
                    temp["PRN_ALM"] = prn_alm

                else:
                    temp["Comment"] = tempr["Bits"]
                    temp["PRN_ALM"] = "NA"
                # print(ephemeris)
                temp["mid"] = mid
                temp["PRN"] = tempr["PRN"]
                temp["TOWC"] = tempr['TOWC']
                almanac_list_4.append(temp)
                ephemeris_list.append(ephemeris)
        return almanac_list_3,almanac_list_4, ephemeris_list


def ephemeris_to_almanac_subframe(ephemeris_data):
    l1=[2,3,4,5,6,7,9]
    alm_bits = ''
    for k in range(0, 300):
        alm_bits += 'p'
    sub_id = {3: '10', 4: '11'}
    subframe_struct = copy.deepcopy(constdict.irnss_almanac_subframes)
    for page_id in l1:
        for alm_subid in range(3, 5):
            if alm_subid == 3:
                almanac_for_sv = alm_subid
                if subframe_struct[page_id][alm_subid] == '':
                    if ephemeris_data[almanac_for_sv] != {}:
                        print("Adding SV ", almanac_for_sv, " Almanac for Page ID ", page_id, " of subframe 4")
                        # -----------------------------------------------------------------------------------
                        subframe_struct[page_id][alm_subid] = copy.deepcopy(alm_bits)
                        subframe_struct[page_id][alm_subid] = ephemeris_data[almanac_for_sv]["TLM"].join([subframe_struct[page_id][alm_subid][:0], subframe_struct[page_id][alm_subid][8:]])
                        subframe_struct[page_id][alm_subid] = binary_data(17, ephemeris_data[almanac_for_sv]["TOWC"] / 12, 0, 1).join([subframe_struct[page_id][alm_subid][:8], subframe_struct[page_id][alm_subid][25:]])
                        subframe_struct[page_id][alm_subid] = ephemeris_data[almanac_for_sv]["Alert"].join([subframe_struct[page_id][alm_subid][:25], subframe_struct[page_id][alm_subid][26:]])
                        subframe_struct[page_id][alm_subid] = ephemeris_data[almanac_for_sv]["Autonav"].join([subframe_struct[page_id][alm_subid][:26], subframe_struct[page_id][alm_subid][27:]])
                        subframe_struct[page_id][alm_subid] = ephemeris_data[almanac_for_sv]["Spare"].join([subframe_struct[page_id][alm_subid][:29], subframe_struct[page_id][alm_subid][30:]])
                        subframe_struct[page_id][alm_subid] = sub_id.get(alm_subid).join([subframe_struct[page_id][alm_subid][:27], subframe_struct[page_id][alm_subid][29:]])
                        # -----------------------------------------------------------------------------------
                        # print("page:",i,"pid:",pid)
                        # Changed by SNB.
                        subframe_struct[page_id][alm_subid] = (
                            binary_data(6, (ephemeris_data[almanac_for_sv]["msg id"]), 0, 1)).join(
                            [subframe_struct[page_id][alm_subid][:30], subframe_struct[page_id][alm_subid][36:]])
                        subframe_struct[page_id][alm_subid] = (binary_data(10, (ephemeris_data[almanac_for_sv]["Week Number"]), 0, 1)).join([subframe_struct[page_id][alm_subid][:36], subframe_struct[page_id][alm_subid][46:]])

                        subframe_struct[page_id][alm_subid] = (tows_binary_data(16, ephemeris_data[almanac_for_sv]["e"], 21, 1)).join([subframe_struct[page_id][alm_subid][:46], subframe_struct[page_id][alm_subid][52:]])
                        subframe_struct[page_id][alm_subid] = (tows_binary_data(16, ephemeris_data[almanac_for_sv]["toa"]/(2**4), 0, 1)).join([subframe_struct[page_id][alm_subid][:62], subframe_struct[page_id][alm_subid][78:]])
                        # I_0 replaced with delta_i
                        subframe_struct[page_id][alm_subid] = (tows_binary_data(24, ephemeris_data[almanac_for_sv]["I_0"], 23, constants.pi)).join([subframe_struct[page_id][alm_subid][:78], subframe_struct[page_id][alm_subid][102:]])

                        subframe_struct[page_id][alm_subid] = (tows_binary_data(16, ephemeris_data[almanac_for_sv]["omegaDot"], 38, constants.pi)).join([subframe_struct[page_id][alm_subid][:102], subframe_struct[page_id][alm_subid][118:]])
                        subframe_struct[page_id][alm_subid] = (tows_binary_data(24, ephemeris_data[almanac_for_sv]["sqrt_A"], 11, 1)).join([subframe_struct[page_id][alm_subid][:118], subframe_struct[page_id][alm_subid][142:]])
                        subframe_struct[page_id][alm_subid] = (tows_binary_data(24, ephemeris_data[almanac_for_sv]["Omega_0"], 23, constants.pi)).join([subframe_struct[page_id][alm_subid][:142], subframe_struct[page_id][alm_subid][166:]])
                        subframe_struct[page_id][alm_subid] = (tows_binary_data(24, ephemeris_data[almanac_for_sv]["Omega"], 23, constants.pi)).join([subframe_struct[page_id][alm_subid][:166], subframe_struct[page_id][alm_subid][190:]])
                        subframe_struct[page_id][alm_subid] = (tows_binary_data(24, ephemeris_data[almanac_for_sv]["M_0"], 23, constants.pi)).join([subframe_struct[page_id][alm_subid][:190], subframe_struct[page_id][alm_subid][214:]])
                        subframe_struct[page_id][alm_subid] = (binary_data(6, (ephemeris_data[almanac_for_sv]["PRN_ALM"]), 0, 1)).join([subframe_struct[page_id][alm_subid][:236], subframe_struct[page_id][alm_subid][242:]])

                        subframe_struct[page_id][alm_subid] =(tows_binary_data(11, ephemeris_data[almanac_for_sv]["a_f0"], 20, 1)).join([subframe_struct[page_id][alm_subid][:214], subframe_struct[page_id][alm_subid][225:]])

                        subframe_struct[page_id][alm_subid] = (tows_binary_data(11, ephemeris_data[almanac_for_sv]["a_f1"], 38, 1)).join([subframe_struct[page_id][alm_subid][:225], subframe_struct[page_id][alm_subid][236:]])
                        subframe_struct[page_id][alm_subid] = (binary_data(6, (ephemeris_data[almanac_for_sv]["Spare_data"]), 0, 1)).join([subframe_struct[page_id][alm_subid][:250], subframe_struct[page_id][alm_subid][256:]])
                        subframe_struct[page_id][alm_subid] = (binary_data(6, ephemeris_data[almanac_for_sv]["PRN"], 20, 1)).join([subframe_struct[page_id][alm_subid][:256], subframe_struct[page_id][alm_subid][262:]])
                        subframe_struct[page_id][alm_subid] = (tows_binary_data(8, ephemeris_data[almanac_for_sv]["Inter_Sig_Corr"], 31, 1)).join([subframe_struct[page_id][alm_subid][:242], subframe_struct[page_id][alm_subid][250:]])
            if alm_subid == 4:
                almanac_for_sv = alm_subid
                if subframe_struct[page_id][alm_subid] == '':
                    if ephemeris_data[almanac_for_sv] != {}:
                        print("Adding SV ", almanac_for_sv, " Almanac for Page ID ", page_id, " of subframe 4")
                        # -----------------------------------------------------------------------------------
                        subframe_struct[page_id][alm_subid] = copy.deepcopy(alm_bits)
                        subframe_struct[page_id][alm_subid] = ephemeris_data[almanac_for_sv]["TLM"].join([subframe_struct[page_id][alm_subid][:0], subframe_struct[page_id][alm_subid][8:]])
                        subframe_struct[page_id][alm_subid] = binary_data(17, ephemeris_data[almanac_for_sv]["TOWC"] / 12, 0,1).join([subframe_struct[page_id][alm_subid][:8], subframe_struct[page_id][alm_subid][25:]])
                        subframe_struct[page_id][alm_subid] = ephemeris_data[almanac_for_sv]["Alert"].join([subframe_struct[page_id][alm_subid][:25], subframe_struct[page_id][alm_subid][26:]])
                        subframe_struct[page_id][alm_subid] = ephemeris_data[almanac_for_sv]["Autonav"].join([subframe_struct[page_id][alm_subid][:26], subframe_struct[page_id][alm_subid][27:]])
                        subframe_struct[page_id][alm_subid] = ephemeris_data[almanac_for_sv]["Spare"].join([subframe_struct[page_id][alm_subid][:29], subframe_struct[page_id][alm_subid][30:]])
                        subframe_struct[page_id][alm_subid] = sub_id.get(alm_subid).join([subframe_struct[page_id][alm_subid][:27], subframe_struct[page_id][alm_subid][29:]])
                        subframe_struct[page_id][alm_subid] = (binary_data(6, (ephemeris_data[almanac_for_sv]["msg id"]), 0, 1)).join([subframe_struct[page_id][alm_subid][:30], subframe_struct[page_id][alm_subid][36:]])
                        # -----------------------------------------------------------------------------------
                        # print("page:",i,"pid:",pid)
                        # Changed by SNB.

                        subframe_struct[page_id][alm_subid] = (binary_data(10, (ephemeris_data[almanac_for_sv]["Week Number"]), 0, 1)).join([subframe_struct[page_id][alm_subid][:36], subframe_struct[page_id][alm_subid][46:]])

                        subframe_struct[page_id][alm_subid] = (tows_binary_data(16, ephemeris_data[almanac_for_sv]["e"], 21, 1)).join([subframe_struct[page_id][alm_subid][:46], subframe_struct[page_id][alm_subid][52:]])
                        subframe_struct[page_id][alm_subid] = (tows_binary_data(16, ephemeris_data[almanac_for_sv]["toa"] / (2 ** 4), 0, 1)).join([subframe_struct[page_id][alm_subid][:62], subframe_struct[page_id][alm_subid][78:]])
                        # I_0 replaced with delta_i
                        subframe_struct[page_id][alm_subid] = (tows_binary_data(24, ephemeris_data[almanac_for_sv]["I_0"], 23, constants.pi)).join([subframe_struct[page_id][alm_subid][:78], subframe_struct[page_id][alm_subid][102:]])

                        subframe_struct[page_id][alm_subid] = (tows_binary_data(16, ephemeris_data[almanac_for_sv]["omegaDot"], 38, constants.pi)).join([subframe_struct[page_id][alm_subid][:102], subframe_struct[page_id][alm_subid][118:]])
                        subframe_struct[page_id][alm_subid] = (tows_binary_data(24, ephemeris_data[almanac_for_sv]["sqrt_A"], 11, 1)).join([subframe_struct[page_id][alm_subid][:118], subframe_struct[page_id][alm_subid][142:]])
                        subframe_struct[page_id][alm_subid] = (tows_binary_data(24, ephemeris_data[almanac_for_sv]["Omega_0"], 23, constants.pi)).join([subframe_struct[page_id][alm_subid][:142], subframe_struct[page_id][alm_subid][166:]])
                        subframe_struct[page_id][alm_subid] = (tows_binary_data(24, ephemeris_data[almanac_for_sv]["Omega"], 23, constants.pi)).join([subframe_struct[page_id][alm_subid][:166], subframe_struct[page_id][alm_subid][190:]])
                        subframe_struct[page_id][alm_subid] = (tows_binary_data(24, ephemeris_data[almanac_for_sv]["M_0"], 23, constants.pi)).join([subframe_struct[page_id][alm_subid][:190], subframe_struct[page_id][alm_subid][214:]])

                        subframe_struct[page_id][alm_subid] = (tows_binary_data(11, ephemeris_data[almanac_for_sv]["a_f0"], 20, 1)).join([subframe_struct[page_id][alm_subid][:214], subframe_struct[page_id][alm_subid][225:]])

                        subframe_struct[page_id][alm_subid] = (tows_binary_data(11, ephemeris_data[almanac_for_sv]["a_f1"], 38, 1)).join([subframe_struct[page_id][alm_subid][:225], subframe_struct[page_id][alm_subid][236:]])
                        subframe_struct[page_id][alm_subid] = (binary_data(6, (ephemeris_data[almanac_for_sv]["PRN_ALM"]), 0, 1)).join([subframe_struct[page_id][alm_subid][:236], subframe_struct[page_id][alm_subid][242:]])
                        subframe_struct[page_id][alm_subid] = (tows_binary_data(8, ephemeris_data[almanac_for_sv]["Inter_Sig_Corr"], 31, 1)).join([subframe_struct[page_id][alm_subid][:242], subframe_struct[page_id][alm_subid][250:]])
                        subframe_struct[page_id][alm_subid] = (binary_data(6, (ephemeris_data[almanac_for_sv]["Spare_data"]), 0, 1)).join([subframe_struct[page_id][alm_subid][:250], subframe_struct[page_id][alm_subid][256:]])
                        subframe_struct[page_id][alm_subid] = (binary_data(6, ephemeris_data[almanac_for_sv]["PRN"], 20, 1)).join([subframe_struct[page_id][alm_subid][:256], subframe_struct[page_id][alm_subid][262:]])


    return subframe_struct

def ephemeris_to_almanac_subframe3(ephemeris_data):
    subframe_struct = ''
    for j in range(0, 300):
        subframe_struct += 'p'
    subframe_struct  = ephemeris_data["TLM"].join(
        [subframe_struct [:0], subframe_struct [8:]])
    subframe_struct  = binary_data(17, ephemeris_data["TOWC"] / 12, 0, 1).join(
        [subframe_struct [:8], subframe_struct [25:]])
    subframe_struct  = ephemeris_data["Alert"].join(
        [subframe_struct [:25], subframe_struct [26:]])
    subframe_struct  = ephemeris_data["Autonav"].join(
        [subframe_struct [:26], subframe_struct [27:]])
    subframe_struct  = ephemeris_data["Spare"].join(
        [subframe_struct [:29], subframe_struct [30:]])
    subframe_struct  ="10".join(
        [subframe_struct [:27], subframe_struct [29:]])
    # -----------------------------------------------------------------------------------
    # print("page:",i,"pid:",pid)
    # Changed by SNB.
    subframe_struct  = (binary_data(10, (ephemeris_data["Week Number"]), 0, 1)).join(
        [subframe_struct [:36], subframe_struct [46:]])

    subframe_struct  = (tows_binary_data(16, ephemeris_data["e"], 21, 1)).join(
        [subframe_struct [:46], subframe_struct [52:]])
    subframe_struct  = (
        tows_binary_data(16, ephemeris_data["toa"] / (2 ** 4), 0, 1)).join(
        [subframe_struct [:62], subframe_struct [78:]])
    # I_0 replaced with delta_i
    subframe_struct  = (
        tows_binary_data(24, ephemeris_data["I_0"], 23, constants.pi)).join(
        [subframe_struct [:78], subframe_struct [102:]])

    subframe_struct  = (
        tows_binary_data(16, ephemeris_data["omegaDot"], 38, constants.pi)).join(
        [subframe_struct [:102], subframe_struct [118:]])
    subframe_struct  = (tows_binary_data(24, ephemeris_data["sqrt_A"], 11, 1)).join(
        [subframe_struct [:118], subframe_struct [142:]])
    subframe_struct  = (
        tows_binary_data(24, ephemeris_data["Omega_0"], 23, constants.pi)).join(
        [subframe_struct [:142], subframe_struct [166:]])
    subframe_struct  = (
        tows_binary_data(24, ephemeris_data["Omega"], 23, constants.pi)).join(
        [subframe_struct [:166], subframe_struct [190:]])
    subframe_struct  = (
        tows_binary_data(24, ephemeris_data["M_0"], 23, constants.pi)).join(
        [subframe_struct [:190], subframe_struct [214:]])
    subframe_struct  = (binary_data(6, (ephemeris_data["PRN_ALM"]), 0, 1)).join(
        [subframe_struct [:236], subframe_struct [242:]])

    subframe_struct  = (tows_binary_data(11, ephemeris_data["a_f0"], 20, 1)).join(
        [subframe_struct [:214], subframe_struct [225:]])

    subframe_struct  = (tows_binary_data(11, ephemeris_data["a_f1"], 38, 1)).join(
        [subframe_struct [:225], subframe_struct [236:]])
    subframe_struct  = (binary_data(6, ephemeris_data["PRN"], 20, 1)).join(
        [subframe_struct [:256], subframe_struct [262:]])
    subframe_struct  = (
        tows_binary_data(8, ephemeris_data["Inter_Sig_Corr"], 31, 1)).join(
        [subframe_struct [:242], subframe_struct [250:]])

def ephemeris_to_almanac_subframe4(ephemeris_data):
    subframe_struct = ''
    for j in range(0, 300):
        subframe_struct += 'p'
    subframe_struct  = ephemeris_data["TLM"].join(
        [subframe_struct [:0], subframe_struct [8:]])
    subframe_struct  = binary_data(17, ephemeris_data["TOWC"] / 12, 0, 1).join(
        [subframe_struct [:8], subframe_struct [25:]])
    subframe_struct  = ephemeris_data["Alert"].join(
        [subframe_struct [:25], subframe_struct [26:]])
    subframe_struct  = ephemeris_data["Autonav"].join(
        [subframe_struct [:26], subframe_struct [27:]])
    subframe_struct  = ephemeris_data["Spare"].join(
        [subframe_struct [:29], subframe_struct [30:]])
    subframe_struct  ="11".join(
        [subframe_struct [:27], subframe_struct [29:]])
    # -----------------------------------------------------------------------------------
    # print("page:",i,"pid:",pid)
    # Changed by SNB.
    subframe_struct  = (binary_data(10, (ephemeris_data["Week Number"]), 0, 1)).join(
        [subframe_struct [:36], subframe_struct [46:]])

    subframe_struct  = (tows_binary_data(16, ephemeris_data["e"], 21, 1)).join(
        [subframe_struct [:46], subframe_struct [52:]])
    subframe_struct  = (
        tows_binary_data(16, ephemeris_data["toa"] / (2 ** 4), 0, 1)).join(
        [subframe_struct [:62], subframe_struct [78:]])
    # I_0 replaced with delta_i
    subframe_struct  = (
        tows_binary_data(24, ephemeris_data["I_0"], 23, constants.pi)).join(
        [subframe_struct [:78], subframe_struct [102:]])

    subframe_struct  = (
        tows_binary_data(16, ephemeris_data["omegaDot"], 38, constants.pi)).join(
        [subframe_struct [:102], subframe_struct [118:]])
    subframe_struct  = (tows_binary_data(24, ephemeris_data["sqrt_A"], 11, 1)).join(
        [subframe_struct [:118], subframe_struct [142:]])
    subframe_struct  = (
        tows_binary_data(24, ephemeris_data["Omega_0"], 23, constants.pi)).join(
        [subframe_struct [:142], subframe_struct [166:]])
    subframe_struct  = (
        tows_binary_data(24, ephemeris_data["Omega"], 23, constants.pi)).join(
        [subframe_struct [:166], subframe_struct [190:]])
    subframe_struct  = (
        tows_binary_data(24, ephemeris_data["M_0"], 23, constants.pi)).join(
        [subframe_struct [:190], subframe_struct [214:]])
    subframe_struct  = (binary_data(6, (ephemeris_data["PRN_ALM"]), 0, 1)).join(
        [subframe_struct [:236], subframe_struct [242:]])

    subframe_struct  = (tows_binary_data(11, ephemeris_data["a_f0"], 20, 1)).join(
        [subframe_struct [:214], subframe_struct [225:]])

    subframe_struct  = (tows_binary_data(11, ephemeris_data["a_f1"], 38, 1)).join(
        [subframe_struct [:225], subframe_struct [236:]])
    subframe_struct  = (binary_data(6, ephemeris_data["PRN"], 20, 1)).join(
        [subframe_struct [:256], subframe_struct [262:]])
    subframe_struct  = (
        tows_binary_data(8, ephemeris_data["Inter_Sig_Corr"], 31, 1)).join(
        [subframe_struct [:242], subframe_struct [250:]])


def return_almanac_sequence_from_ephemeris(ephemeris):
    almanac_subframe_list = []
    sub4list = [2,3,4,5,6,7,9]
    for i in sub4list:
        if ephemeris[i] != {}:
            almanac_subframe = copy.deepcopy(constdict.almanac_subframes)
            for mid in range(7, 8):
                temp = {}
                if mid ==7:
                    # print("e4",pid)
                    actual_pid = ephemeris["PRN_ALM"]
                    temp["Comment"] = "Almanac for SV - " + str(actual_pid)
                    ephemeris[i]["page id"] = actual_pid
                    almanac_subframe[mid][4] = subframe_3and4_parser_irnss(mid, ephemeris[i])
                    # print("e4",ephemeris[i])
                if mid ==7:
                    actual_pid =  ephemeris["PRN_ALM"]
                    temp["Comment"] = "Almanac for SV - " + str(actual_pid)
                    ephemeris[i]["page id"] = actual_pid
                    almanac_subframe[mid][5] = subframe_3and4_parser_irnss(actual_pid, ephemeris[i])
                    # print("e5", almanac_subframe[pid][5])
        almanac_subframe_list.append(almanac_subframe)
    return almanac_subframe_list

almanac_list_3,almanac_list_4, ephemeris_list=return_almanac_sequence_irnss("..\RAW_NAV_BITS\RNBB_L5.csv")
print("3:",(almanac_list_3[0]))
print("4:",(almanac_list_4[0]))
print("eph:",ephemeris_list[0])

eph45=ephemeris_to_almanac_subframe(ephemeris_list[0])
print("45::",eph45)

csv_dump = []
temp = ["TOWC","ALM_3_msg_id" ,"ALM_3_prn_alm", "ALM_3_Logical" ,"ALM_3_Comment", "ALM_4_msg_id","ALM_4_prn_alm", "ALM_4_Logical","ALM_4_Comment"]
csv_dump.append(temp)
for i in range(len(almanac_list_4)):
    temp = [almanac_list_3[i]["TOWC"], almanac_list_3[i]["mid"],almanac_list_3[i]["PRN_ALM"] ,almanac_list_3[i]["PRN"],almanac_list_3[i]["Comment"],
            almanac_list_4[i]["mid"],almanac_list_4[i]["PRN_ALM"] ,almanac_list_4[i]["PRN"],almanac_list_4[i]["Comment"]]
    csv_dump.append(temp)

t = dump_list_to_csv(filename=r"..\Output\almanac_compare_irnss.csv", dlist=csv_dump)
if t==True:
    print("Done writing excel")
else:
    print("data Unsuccesful")


eph=read_alamanc_file.irnss_get_almanac_from_raw_navbits_new("D:\gps_parser\RAW_NAV_BITS\RNBB_L5.csv")
print(eph)

csv_dump=[]
not_equal_data=[]
keys=list(constdict.ephemeris_template.keys())
#print("keys:",keys)
temp = ["Source"]
flag=0
for key in keys :
    temp.append(key)
#print(temp)
csv_dump.append(temp)
for i in range(1,10):
    if eph[i]!={} and ephemeris_list[0][i]!={}:
        file2 = [ "From RNBB Parameters"]
        file1=["From generated Parameters"]
        for key in keys:
            #to_append = ['from almanac']
            #to_append.extend(split_string)
            file2.append(eph[i][key])
            file1.append(ephemeris_list[0][i][key])

        csv_dump.append(file2)
        csv_dump.append(file1)
        csv_dump.append([])

t = dump_list_to_csv(filename=r"..\Output\irnss_dump_ephemeris_list45.csv", dlist=csv_dump)
if t==True:
    print("Done writing excel")
else:
    print("data Unsuccesful")