
ephemeris_template["IODE_sf2"]=binary(nav_bits[60:68])
ephemeris_template["C_rs"]=twos_complement(nav_bits[68:84])*(2**(-5))
ephemeris_template["Deltan"] = twos_complement(nav_bits[90:106])*(2**(-43))*pi
ephemeris_template["M_0"] = (twos_complement(nav_bits[106:114]+nav_bits[120:144])*(2**(-31))) * pi

ephemeris_template["C_uc"] = twos_complement((nav_bits[150:166]) )*(2**(-29))
ephemeris_template["e"] = binary((nav_bits[166:174]+nav_bits[180:204]))*(2**(-33))
ephemeris_template["C_us"] = twos_complement(nav_bits[210:266])* (2 ** -29)
ephemeris_template["sqrt_A"] = binary(nav_bits[226:234]+nav_bits[240:264]) * (2 ** -19)
ephemeris_template["t_oe"] = binary(nav_bits[270:286]) * (2 ** 4)

# New Code
ephemeris_template["FIT INTERVAL"] = nav_bits[286:287]  # if ephemeris is older than 4 hours then 1
ephemeris_template["AODO"] = nav_bits[287:292]