# pass yuma file and get ephemeris
# ephemeris to subframes 1,2,3
# ephemeris to alamanc structure

# set towc, toe, toc and week number

# generate GPS CA Codes = Satish Sir
# generate actual raw nav_bits for 30 seconds  - SV_functions.generate_raw_nav_bits_for_30_seconds  - Test to be complete by KM till 2.30 for keysight TOWC
# generate satellite parameters for 30 seconds - SV_functions.generate_satellite_parameters_for_30_seconds


from SV_functions import read_alamanc_file,generate_satellite_parameters_for_30_seconds,subframe_creation
from SV_functions import convert


ephemeris_from_file = read_alamanc_file.get_alamanac(filename ="UPLOADED_ALAMANAC\March1_2019_yuma.txt")
subframes_123_from_ephemeris = subframe_creation.ephemeris_to_subframe(ephemeris_from_file)

# Set scenario settings
time_parameters = {
    "seconds" : 0,
    "Week" : 1019,
}
ephemeris = read_alamanc_file.add_time_parameters_to_ephemeris(ephemeris_from_file,time_parameters)
x,y,z = convert.gps_to_ecef(18.516726,73.856255,500)

# Replace time parameters in ephemeris
print(ephemeris[1])

# get satellite parameters for 30 seconds
satellite_parameters = 0

# get satellite parameters for 30 seconds
satellite_parameters = generate_satellite_parameters_for_30_seconds.generate_satellite_parameters_for_30_seconds(ephemeris_from_file, time_parameters["seconds"], [x,y,z] )

# generation of excel
heading = ["Time"]
for sv in range (1,33):
    if sv in satellite_parameters :
        heading.append( "Pseudorange_" + str(sv))
        heading.append( "codephases_" + str(sv))
        heading.append("Dopplers_" + str(sv) )

csv_dump = []
csv_dump.append(heading)
#print(csv_dump)
#len(satellite_parameters[1]["Times"])
for time in range(len(satellite_parameters[1]["Times"])):
    row = []
    for sv in range(1, 33):
        if sv in satellite_parameters :
            if sv == 1 :
                row.append(satellite_parameters[sv]["Times"][time])
            row.append(satellite_parameters[sv]["Pseudorange"][time])
            row.append(satellite_parameters[sv]["codephases"][time])
            row.append(satellite_parameters[sv]["Dopplers"][time])
    csv_dump.append(row)


#print(csv_dump)
from Basic_functions import dump_list_to_csv
t=dump_list_to_csv(filename="Output\satellite_params.csv",dlist=csv_dump)
if t==True:
    print("Done writing excel")
else:
    print("bit Unsuccesful")

# actual comparison of satellite parameters
# concrete test code
# date to gps time
# signal generation


# irnss parser
# same functions as gps
# input as raw nav bits ; output as ephemeris
# ephemeris to subframe creation
# fec encoder
# checksum generator


