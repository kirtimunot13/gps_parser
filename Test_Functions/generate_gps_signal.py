#input yuma file get ephemeris
#generate subframe 1 2 and 3
#generate almnac subframe structure 4 and 5 subframe
#towc=current time(by default 0)
#page_id,i=1
#while i<26 (now consider for 25 sets of ephemaris)
    #page_id=page_id+1
    #if page_id==26:
        #page_id=1
    #call a function:genertae_nav_for_6_sec(ephemaris,almnac struct,page_id,towc_subframe1,Week_no) to generate subframes 1 to 5 raw navbits for 6 seconds
    #towc=towc+30

#def genertae_nav_for_6_sec(ephemaris,almnac struct,page_id,towc_subframe1,Week_no)
    #input ephemeris and almnac sub struct
    #input page_id index
    #add subframes 4 and 5 common to all satellites in ephemaris structure
    #generate towc for all 5 subframes (add 6 in each subframe)
    #calculate toe and toc , formula=ceil((towc/7200)*7200)
    #calculate mk based on M_0 and replace in ephemaris
    #insert towc, toe, toc and week number
    #generate parity
    #take 1's complement of 30 bit word whose previous word ends with one
    #return ephemaris of 32 satellites with all 5 subframes

from Basic_functions import dump_list_to_csv
import constdict
import copy,math
from Basic_functions import tows_binary_data,binary_data,binary
from read_alamanc_file import get_alamanac,return_almanac_sequence
from subframe_creation import ephemeris_to_subframe,ephemeris_to_almanac_subframe,return_almanac_sequence_from_ephemeris #,add_time_and_parity


def generate_nav_for_6_sec(subframes_from_almanac123, almnac_struct45, page_id, towc, Week_no):
    #subframe_all= {copy.deepcopy(constdict.subframes)}
    for svid in range(1,33):
        if subframes_from_almanac123[svid][1]!='':
            print("in fun", svid)
            subframes_from_almanac123[svid].update(almnac_struct45[page_id])

            #subframes_from_almanac123.update(almnac_struct45[page_id][5])
            sv_towc = towc
            for subframe in range(1,6):
                sv_towc = sv_towc + 6
                toe = math.ceil((sv_towc / 7200) * 7200)
                toc = math.ceil((sv_towc / 7200) * 7200)
                subframes_from_almanac123[svid][subframe] = (binary_data(17, sv_towc, 0, 1)).join([subframes_from_almanac123[svid][subframe][:30], subframes_from_almanac123[svid][subframe][47:]])
                if subframe==1:
                    subframes_from_almanac123[svid][subframe]= (binary_data(8, toc, 4,1)).join([ subframes_from_almanac123[svid][subframe][:218],  subframes_from_almanac123[svid][subframe][234:]])
                    subframes_from_almanac123[svid][subframe] = (binary_data(10, Week_no, 0, 1)).join([subframes_from_almanac123[svid][subframe][:60], subframes_from_almanac123[svid][subframe][70:]])
                if subframe == 2:
                    subframes_from_almanac123[svid][subframe]= (binary_data(16, toe, 4,1)).join([ subframes_from_almanac123[svid][subframe][:270],  subframes_from_almanac123[svid][subframe][286:]])
    return subframes_from_almanac123


ephemeris_from_file = get_alamanac(filename = r"UPLOADED_ALAMANAC\March1_2019_yuma.txt")
print("ephemeris almnac data", ephemeris_from_file)
# FUNCTION TO CREATE 3 SUBFRAMES FROM EACH SET OF EPHEMERIS
subframes_from_almanac123 = ephemeris_to_subframe(ephemeris_from_file)
print("subframes almnac data", subframes_from_almanac123)

almnac_struct45=return_almanac_sequence_from_ephemeris(ephemeris_from_file)
print("alm 4 and 5::",almnac_struct45)

flist=[]
towc=0
Week_no=1019
page_id,i=0,1

while i<26:
    page_id = page_id + 1
    '''if page_id==26:
        page_id=1'''
    almanac_for_sub12345=generate_nav_for_6_sec(subframes_from_almanac123, almnac_struct45, page_id, towc, Week_no)
    towc=towc+30
    flist.append(almanac_for_sub12345)
    i=i+1

print(flist)

