import copy
import Test_Functions.constdict as constdict
import math
import pandas as pd
import  SV_functions.gps_constants as constants
from Basic_functions import tows_binary_data,binary_data,binary

# FUNCTION TO CREATE SUBFRAME  1,2,3, in first iteration
# INPUT AS EPHEMERIS is const dict
# RETURN AS SUBFRAMES 300 BIT STRING WITH PPPP at TOWC , TOE, TOC AND PARITY.
def ephemeris_to_subframe(ephemeris_data) :
    alm_bits=''
    for j in range(0, 300):
        alm_bits += 'p'
    pageid = {1: '001', 2: '010', 3: '011', 4: '100', 5: '101'}
    subframe_struct=copy.deepcopy(constdict.subframes)
    for i in range(1,constdict.no_of_satellites+1):
        if ephemeris_data[i] !={}:
            for alm_subid in range(1,4):
                pid = ephemeris_data[i]["page id"]
                subframe_struct[i][alm_subid] = alm_bits
                subframe_struct[i][alm_subid] = ephemeris_data[i]["preamble"].join([subframe_struct[i][alm_subid][:0], subframe_struct[i][alm_subid][8:]])
                subframe_struct[i][alm_subid] = ephemeris_data[i]["TLM_MSG"].join([subframe_struct[i][alm_subid][:8], subframe_struct[i][alm_subid][22:]])
                subframe_struct[i][alm_subid] = ephemeris_data[i]["TLM_Reserved"].join([subframe_struct[i][alm_subid][:22], subframe_struct[i][alm_subid][24:]])
                subframe_struct[i][alm_subid] = ephemeris_data[i]["HOW_Reserved"].join([subframe_struct[i][alm_subid][:30], subframe_struct[i][alm_subid][47:]])
                subframe_struct[i][alm_subid] = (binary_data(2, ephemeris_data[i]["Alert_Antispoof"], 0, 1)).join([subframe_struct[i][alm_subid][:47], subframe_struct[i][alm_subid][49:]])
                subframe_struct[i][alm_subid] = (binary_data(2, ephemeris_data[i]["Parity_bit_solve"], 0, 1)).join([subframe_struct[i][alm_subid][:52], subframe_struct[i][alm_subid][54:]])
                if alm_subid == 1:
                    subframe_struct[i][alm_subid] = pageid.get(alm_subid).join([subframe_struct[i][alm_subid][:49], subframe_struct[i][alm_subid][52:]])
                    # print((tows_binary_data(10, ephemeris_data[i]["Week Number"], 0, 1)))
                    subframe_struct[i][alm_subid] = (binary_data(10, ephemeris_data[i]["Week Number"], 0, 1)).join([subframe_struct[i][alm_subid][:60], subframe_struct[i][alm_subid][70:]])
                    subframe_struct[i][alm_subid] = ephemeris_data[i]["Code Type"].join([subframe_struct[i][alm_subid][:70], subframe_struct[i][alm_subid][72:]])
                    subframe_struct[i][alm_subid] = (binary_data(4, ephemeris_data[i]["Accuracy"], 0, 1)).join([subframe_struct[i][alm_subid][:72], subframe_struct[i][alm_subid][76:]])
                    subframe_struct[i][alm_subid] = (binary_data(6, ephemeris_data[i]["Health"], 0, 1)).join([subframe_struct[i][alm_subid][:76], subframe_struct[i][alm_subid][82:]])
                    tbit =binary_data(10,ephemeris_data[i]["IODC"],0,1)
                    subframe_struct[i][alm_subid] = tbit[0:2].join([subframe_struct[i][alm_subid][:82], subframe_struct[i][alm_subid][84:]])
                    subframe_struct[i][alm_subid] = tbit[2:10].join([subframe_struct[i][alm_subid][:210], subframe_struct[i][alm_subid][218:]])

                    subframe_struct[i][alm_subid] = (binary_data(1, float(ephemeris_data[i]["L2P Flag"]), 0, 1)).join([subframe_struct[i][alm_subid][:90], subframe_struct[i][alm_subid][91:]])
                    subframe_struct[i][alm_subid] = ephemeris_data[i]["Reserved 1"].join([subframe_struct[i][alm_subid][:91], subframe_struct[i][alm_subid][114:]])
                    # print(ephemeris_data[i]["Reserved 1"])
                    subframe_struct[i][alm_subid] = ephemeris_data[i]["Reserved 2"].join( [subframe_struct[i][alm_subid][:120], subframe_struct[i][alm_subid][144:]])
                    subframe_struct[i][alm_subid] = ephemeris_data[i]["Reserved 3"].join([subframe_struct[i][alm_subid][:150], subframe_struct[i][alm_subid][174:]])
                    subframe_struct[i][alm_subid] = ephemeris_data[i]["Reserved 4"].join([subframe_struct[i][alm_subid][:180], subframe_struct[i][alm_subid][196:]])
                    subframe_struct[i][alm_subid] = (tows_binary_data(8, ephemeris_data[i]["T_GD"], 31, 1)).join([subframe_struct[i][alm_subid][:196], subframe_struct[i][alm_subid][204:]])
                    subframe_struct[i][alm_subid] = (tows_binary_data(8, ephemeris_data[i]["a_f2"], 55, 1)).join([subframe_struct[i][alm_subid][:240], subframe_struct[i][alm_subid][248:]])
                    subframe_struct[i][alm_subid] = (tows_binary_data(16, ephemeris_data[i]["a_f1"], 43, 1)).join([subframe_struct[i][alm_subid][:248], subframe_struct[i][alm_subid][264:]])
                    subframe_struct[i][alm_subid] = (tows_binary_data(22, ephemeris_data[i]["a_f0"], 31, 1)).join([subframe_struct[i][alm_subid][:270], subframe_struct[i][alm_subid][292:]])
                if alm_subid==2:
                    subframe_struct[i][alm_subid] = pageid.get(alm_subid).join([subframe_struct[i][alm_subid][:49], subframe_struct[i][alm_subid][52:]])
                    subframe_struct[i][alm_subid] = ephemeris_data[i]["IODE_sf2"].join([subframe_struct[i][alm_subid][:60], subframe_struct[i][alm_subid][68:]])
                    subframe_struct[i][alm_subid] = (tows_binary_data(16, ephemeris_data[i]["C_rs"], 5, 1)).join([subframe_struct[i][alm_subid][:68], subframe_struct[i][alm_subid][84:]])
                    # print(ephemeris_data[i]['Deltan'])
                    subframe_struct[i][alm_subid] = (tows_binary_data(16, (ephemeris_data[i]['Deltan']), 43, constants.pi)).join([subframe_struct[i][alm_subid][:90], subframe_struct[i][alm_subid][106:]])

                    tbits = (tows_binary_data(32, (ephemeris_data[i]['M_0']), 31, 1))
                    subframe_struct[i][alm_subid] = (tbits[0:8]).join([subframe_struct[i][alm_subid][:106], subframe_struct[i][alm_subid][114:]])
                    subframe_struct[i][alm_subid] = (tbits[8:32]).join([subframe_struct[i][alm_subid][:120], subframe_struct[i][alm_subid][144:]])

                    subframe_struct[i][alm_subid] = (tows_binary_data(16, ephemeris_data[i]["C_uc"], 29, 1)).join([subframe_struct[i][alm_subid][:150], subframe_struct[i][alm_subid][166:]])

                    tbits = (tows_binary_data(32, (ephemeris_data[i]['e']), 33, 1))
                    subframe_struct[i][alm_subid] = (tbits[0:8]).join( [subframe_struct[i][alm_subid][:166], subframe_struct[i][alm_subid][174:]])
                    subframe_struct[i][alm_subid] = (tbits[8:32]).join([subframe_struct[i][alm_subid][:180], subframe_struct[i][alm_subid][204:]])

                    subframe_struct[i][alm_subid] = (tows_binary_data(16, ephemeris_data[i]["C_us"], 29, 1)).join([subframe_struct[i][alm_subid][:210], subframe_struct[i][alm_subid][226:]])

                    tbits = (tows_binary_data(32, (ephemeris_data[i]['sqrt_A']), 19, 1))
                    subframe_struct[i][alm_subid] = (tbits[0:8]).join([subframe_struct[i][alm_subid][:226], subframe_struct[i][alm_subid][234:]])
                    subframe_struct[i][alm_subid] = (tbits[8:32]).join([subframe_struct[i][alm_subid][:240], subframe_struct[i][alm_subid][264:]])

                    subframe_struct[i][alm_subid] = (binary_data(1, float(ephemeris_data[i]["FIT INTERVAL"]), 0, 1)).join([subframe_struct[i][alm_subid][:286], subframe_struct[i][alm_subid][287:]])
                    subframe_struct[i][alm_subid] = ephemeris_data[i]["AODO"].join([subframe_struct[i][alm_subid][:287], subframe_struct[i][alm_subid][292:]])
                if alm_subid==3:
                    subframe_struct[i][alm_subid] = pageid.get(alm_subid).join([subframe_struct[i][alm_subid][:49], subframe_struct[i][alm_subid][52:]])
                    subframe_struct[i][alm_subid] = (tows_binary_data(16, ephemeris_data[i]["C_ic"], 29, 1)).join([subframe_struct[i][alm_subid][:60], subframe_struct[i][alm_subid][76:]])
                    tbits = (tows_binary_data(32, (ephemeris_data[i]['Omega_0']), 31, constants.pi))
                    subframe_struct[i][alm_subid] = (tbits[0:8]).join([subframe_struct[i][alm_subid][:76], subframe_struct[i][alm_subid][84:]])
                    subframe_struct[i][alm_subid] = (tbits[8:32]).join([subframe_struct[i][alm_subid][:90], subframe_struct[i][alm_subid][114:]])

                    subframe_struct[i][alm_subid] = (tows_binary_data(16, ephemeris_data[i]["C_is"], 0, 1)).join([subframe_struct[i][alm_subid][:120], subframe_struct[i][alm_subid][136:]])
                    tbits = (tows_binary_data(32, (ephemeris_data[i]['I_0']), 31, constants.pi))
                    subframe_struct[i][alm_subid] = (tbits[0:8]).join([subframe_struct[i][alm_subid][:136], subframe_struct[i][alm_subid][144:]])
                    subframe_struct[i][alm_subid] = (tbits[8:32]).join([subframe_struct[i][alm_subid][:150], subframe_struct[i][alm_subid][174:]])

                    subframe_struct[i][alm_subid] = (tows_binary_data(16, ephemeris_data[i]["C_rc"], 0, 1)).join([subframe_struct[i][alm_subid][:180], subframe_struct[i][alm_subid][196:]])

                    tbits = (tows_binary_data(32, (ephemeris_data[i]['Omega']), 31, constants.pi))
                    subframe_struct[i][alm_subid] = (tbits[0:8]).join([subframe_struct[i][alm_subid][:196], subframe_struct[i][alm_subid][204:]])
                    subframe_struct[i][alm_subid] = (tbits[8:32]).join([subframe_struct[i][alm_subid][:210], subframe_struct[i][alm_subid][234:]])

                    subframe_struct[i][alm_subid] = (tows_binary_data(24, (ephemeris_data[i]['omegaDot']), 43, constants.pi).join([subframe_struct[i][alm_subid][:240], subframe_struct[i][alm_subid][264:]]))

                    subframe_struct[i][alm_subid] = ephemeris_data[i]["IODE_sf3"].join([subframe_struct[i][alm_subid][:270], subframe_struct[i][alm_subid][278:]])
                    subframe_struct[i][alm_subid] = (tows_binary_data(14, ephemeris_data[i]['iDot'], 43, constants.pi)).join([subframe_struct[i][alm_subid][:278], subframe_struct[i][alm_subid][292:]])


    else:
            pass
    return subframe_struct



# INPUT AS 300 BIT STRINGS WITH PPPP at TOWC , TOE, TOC AND PARITY.
# OUTPUT AS FILLED TOWC, TOE, TOC and PARITY
'''def add_time_and_parity(subframes = subframes,toe=toe,towc =towc,toc=toc) :

    return "dummy"'''

# INPUT FILENAME OF RAW NAVBITS
# OUTPUT subframe structure from const dict
def raw_nav_bits_to_subframe_structure_new(filename):
    alm_list = []
    Keysight_data = pd.read_csv(filename)
    BladeRF_data = Keysight_data.to_dict(orient="record")

    Parsed_data = {}  # copy.deepcopy(constdict.subframes)
    # Create a structure of 32 with in satellites each with 5 subframes

    for record in BladeRF_data:
        tempr = {}
        tempr["Bits"] = ""
        for i in range(1, 38):
            tempr["Bits"] += format(record['Raw Navbits – ' + str(i)], '08b')
        tempr['TOWC'] = record['TOWC (corresponding to start of the sub-frame) (s)']
        tempr["WN"] = record['Week No (corresponding to start of the sub-frame)']
        tempr["PRN"] = record["PRN"]

        tempr['subframe id'] = record['subframe id']
        # print(tempr["PRN"],tempr['subframe id'])

        if tempr["PRN"] in Parsed_data:
            if tempr['subframe id'] in Parsed_data[tempr["PRN"]]:
                pass
            else:
                Parsed_data[tempr["PRN"]][tempr["subframe id"]] = tempr
        else:
            if tempr['subframe id'] == 1:
                Parsed_data[tempr["PRN"]] = {}
                Parsed_data[tempr["PRN"]][tempr["subframe id"]] = tempr
    # print(Parsed_data)
    nav_bits=''
    subframe_list=[]
    Valid_PRNs = list(Parsed_data.keys())
    #print("Found PRNs - ", Valid_PRNs)
    subframe_struct = copy.deepcopy(constdict.subframes)
    for Valid_PRN in Valid_PRNs:

        for subframe in range(1, 4):
            nav_bits= "10001011"+ Parsed_data[Valid_PRN][subframe]["Bits"]
            #print(nav_bits)

            if subframe == 1:
                tc=Parsed_data[Valid_PRN][subframe]["TOWC"]
                nav_bits = "10001011" + Parsed_data[Valid_PRN][subframe]["Bits"]
                #print(nav_bits)
                #print(Valid_PRN,"s1",Parsed_data[Valid_PRN][subframe]["TOWC"])
                subframe_struct[Valid_PRN][subframe]=nav_bits

            elif subframe == 2 :
                #print(Valid_PRN,"s2", Parsed_data[Valid_PRN][subframe]["TOWC"])
                nav_bits = "10001011" + Parsed_data[Valid_PRN][subframe]["Bits"]
                subframe_struct[Valid_PRN][subframe] = nav_bits

            elif subframe==3:
                #print(Valid_PRN,"s3", Parsed_data[Valid_PRN][subframe]["TOWC"])
                nav_bits = "10001011" + Parsed_data[Valid_PRN][subframe]["Bits"]
                subframe_struct[Valid_PRN][subframe] = nav_bits

        subframe_list.append(subframe_struct)
    return subframe_struct

def subframe_4_from_ephemeris(actual_pid,ephemeris_data):
    subframe_struct = ''
    for j in range(0, 300):
        subframe_struct += 'p'
    subframe_struct = "100".join([subframe_struct[:49], subframe_struct[52:]])
    subframe_struct = ephemeris_data["preamble"].join([subframe_struct[:0], subframe_struct[8:]])
    subframe_struct = ephemeris_data["TLM_MSG"].join([subframe_struct[:8], subframe_struct[22:]])
    subframe_struct = ephemeris_data["TLM_Reserved"].join([subframe_struct[:22], subframe_struct[24:]])
    subframe_struct = ephemeris_data["HOW_Reserved"].join([subframe_struct[:30], subframe_struct[47:]])
    subframe_struct = (binary_data(2, ephemeris_data["Alert_Antispoof"], 0, 1)).join([subframe_struct[:47], subframe_struct[49:]])
    subframe_struct = (binary_data(2, ephemeris_data["Parity_bit_solve"], 0, 1)).join([subframe_struct[:52], subframe_struct[54:]])

    subframe_struct = binary_data(8, ephemeris_data["page id"], 0, 1).join([subframe_struct[:60], subframe_struct[68:]])
    subframe_struct = (tows_binary_data(16, ephemeris_data["e"], 21, 1)).join([subframe_struct[:68], subframe_struct[84:]])
    subframe_struct = (tows_binary_data(8, ephemeris_data["toa"], 12, 1)).join([subframe_struct[:90], subframe_struct[98:]])
    # Changed by SNB. Replacing I_0 with Delta_i
    subframe_struct = (tows_binary_data(16, ephemeris_data["delta_i"], 19, 1)).join([subframe_struct[:98], subframe_struct[114:]])

    subframe_struct = (tows_binary_data(16, ephemeris_data["omegaDot"], 38, math.pi)).join([subframe_struct[:120], subframe_struct[136:]])
    subframe_struct = (tows_binary_data(24, ephemeris_data["sqrt_A"], 11, 1)).join([subframe_struct[:150], subframe_struct[174:]])
    subframe_struct = (tows_binary_data(24, ephemeris_data["Omega_0"], 23, math.pi)).join([subframe_struct[:180], subframe_struct[204:]])
    subframe_struct = (tows_binary_data(24, ephemeris_data["Omega"], 23, math.pi)).join([subframe_struct[:210], subframe_struct[234:]])
    subframe_struct = (tows_binary_data(24, ephemeris_data["M_0"], 23, math.pi)).join([subframe_struct[:240], subframe_struct[264:]])
    subframe_struct = (binary_data(8, float(ephemeris_data["Health"]), 0, 1)).join([subframe_struct[:136], subframe_struct[144:]])
    tbits = (tows_binary_data(11, ephemeris_data["a_f0"], 20, 1))
    subframe_struct = (tbits[0:8]).join([subframe_struct[:270], subframe_struct[278:]])
    subframe_struct = (tbits[8:]).join([subframe_struct[:289], subframe_struct[292:]])
    subframe_struct = (tows_binary_data(11, ephemeris_data["a_f1"], 38, 1)).join([subframe_struct[:278], subframe_struct[289:]])
    return subframe_struct

def subframe_5_from_ephemeris(actual_pid,ephemeris_data):
    subframe_struct = ''
    for j in range(0, 300):
        subframe_struct += 'p'
    subframe_struct = "100".join([subframe_struct[:49], subframe_struct[52:]])
    subframe_struct = ephemeris_data["preamble"].join([subframe_struct[:0], subframe_struct[8:]])
    subframe_struct = ephemeris_data["TLM_MSG"].join([subframe_struct[:8], subframe_struct[22:]])
    subframe_struct = ephemeris_data["TLM_Reserved"].join([subframe_struct[:22], subframe_struct[24:]])
    subframe_struct = ephemeris_data["HOW_Reserved"].join([subframe_struct[:30], subframe_struct[47:]])
    subframe_struct = (binary_data(2, ephemeris_data["Alert_Antispoof"], 0, 1)).join([subframe_struct[:47], subframe_struct[49:]])
    subframe_struct = (binary_data(2, ephemeris_data["Parity_bit_solve"], 0, 1)).join([subframe_struct[:52], subframe_struct[54:]])

    subframe_struct = binary_data(8, ephemeris_data["page id"], 0, 1).join([subframe_struct[:60], subframe_struct[68:]])
    subframe_struct = (tows_binary_data(16, ephemeris_data["e"], 21, 1)).join([subframe_struct[:68], subframe_struct[84:]])
    subframe_struct = (tows_binary_data(8, ephemeris_data["toa"], 12, 1)).join([subframe_struct[:90], subframe_struct[98:]])
    # Changed by SNB. Chaning I_0 to delta_i
    subframe_struct = (tows_binary_data(16, ephemeris_data["delta_i"], 19, 1)).join([subframe_struct[:98], subframe_struct[114:]])

    subframe_struct = (tows_binary_data(16, ephemeris_data["omegaDot"], 38, math.pi)).join([subframe_struct[:120], subframe_struct[136:]])
    subframe_struct = (tows_binary_data(24, ephemeris_data["sqrt_A"], 11, 1)).join([subframe_struct[:150], subframe_struct[174:]])
    subframe_struct = (tows_binary_data(24, ephemeris_data["Omega_0"], 23, math.pi)).join([subframe_struct[:180], subframe_struct[204:]])
    subframe_struct = (tows_binary_data(24, ephemeris_data["Omega"], 23, math.pi)).join([subframe_struct[:210], subframe_struct[234:]])
    subframe_struct = (tows_binary_data(24, ephemeris_data["M_0"], 23, math.pi)).join([subframe_struct[:240], subframe_struct[264:]])
    subframe_struct = (binary_data(8, float(ephemeris_data["Health"]), 0, 1)).join([subframe_struct[:136], subframe_struct[144:]])
    tbits = (tows_binary_data(11, ephemeris_data["a_f0"], 20, 1))
    subframe_struct = (tbits[0:8]).join([subframe_struct[:270], subframe_struct[278:]])
    subframe_struct = (tbits[8:]).join([subframe_struct[:289], subframe_struct[292:]])
    subframe_struct = (tows_binary_data(11, ephemeris_data["a_f1"], 38, 1)).join([subframe_struct[:278], subframe_struct[289:]])
    return subframe_struct

def return_almanac_sequence_from_ephemeris(ephemeris):
    almanac_subframe_list=[]
    sub4list = {2: 25, 3: 26, 4: 27, 5: 28, 7: 29, 8: 30, 9: 31, 10: 32}
    for i in range(1,33):
        if ephemeris[i]!={}:
            almanac_subframe = copy.deepcopy(constdict.almanac_subframes)
            for pid in range(1,25):
                temp={}
                if pid in sub4list.keys():
                    #print("e4",pid)
                    actual_pid = sub4list[pid]
                    temp["Comment"] = "Almanac for SV - " + str(actual_pid)
                    ephemeris[i]["page id"]=actual_pid
                    almanac_subframe[pid][4]=subframe_4_from_ephemeris(pid,ephemeris[i])
                    #print("e4",ephemeris[i])
                if pid in constants.sub5mapping_SV_map.keys():
                    actual_pid = constants.sub5mapping_SV_map[pid]
                    temp["Comment"] = "Almanac for SV - " + str(actual_pid)
                    ephemeris[i]["page id"] = actual_pid
                    almanac_subframe[pid][5]=subframe_5_from_ephemeris(actual_pid,ephemeris[i])
                    #print("e5", almanac_subframe[pid][5])
        almanac_subframe_list.append(almanac_subframe)
    return almanac_subframe

def ephemeris_to_almanac_subframe(ephemeris_data):
    alm_bits = ''
    for k in range(0, 300):
        alm_bits += 'p'
    sub4list={2: 25, 3: 26, 4: 27, 5: 28, 7: 29, 8: 30, 9: 31, 10: 32}
    subframeid = {4: '100', 5: '101'}
    subframe_struct = copy.deepcopy(constdict.almanac_subframes)
    for i in range(1, 26 ):
        if  ephemeris_data[i]!={}:
            for alm_subid in range(4,6):
                if subframe_struct[i][alm_subid] == '':
                    subframe_struct[i][alm_subid] = copy.deepcopy(alm_bits)
                    subframe_struct[i][alm_subid] = ephemeris_data[i]["preamble"].join([subframe_struct[i][alm_subid][:0], subframe_struct[i][alm_subid][8:]])
                    subframe_struct[i][alm_subid] = ephemeris_data[i]["TLM_MSG"].join([subframe_struct[i][alm_subid][:8], subframe_struct[i][alm_subid][22:]])
                    subframe_struct[i][alm_subid] = ephemeris_data[i]["TLM_Reserved"].join([subframe_struct[i][alm_subid][:22], subframe_struct[i][alm_subid][24:]])
                    subframe_struct[i][alm_subid] = ephemeris_data[i]["HOW_Reserved"].join([subframe_struct[i][alm_subid][:30], subframe_struct[i][alm_subid][47:]])
                    subframe_struct[i][alm_subid] = (binary_data(2, ephemeris_data[i]["Alert_Antispoof"], 0, 1)).join([subframe_struct[i][alm_subid][:47], subframe_struct[i][alm_subid][49:]])
                    subframe_struct[i][alm_subid] = (binary_data(2, ephemeris_data[i]["Parity_bit_solve"], 0, 1)).join([subframe_struct[i][alm_subid][:52], subframe_struct[i][alm_subid][54:]])
                    if alm_subid==4 and i in sub4list.keys():
                        #print("page:",i,"pid:",pid)
                        subframe_struct[i][alm_subid] = subframeid.get(alm_subid).join([subframe_struct[i][alm_subid][:49], subframe_struct[i][alm_subid][52:]])
                        subframe_struct[i][alm_subid] = binary_data(8,ephemeris_data[i]["page id"],0,1).join([subframe_struct[i][alm_subid][:60], subframe_struct[i][alm_subid][68:]])
                        subframe_struct[i][alm_subid] = (tows_binary_data(16, ephemeris_data[i]["e"], 21, 1)).join([ subframe_struct[i][alm_subid][:68],  subframe_struct[i][alm_subid][84:]])
                        subframe_struct[i][alm_subid] = (tows_binary_data(8, ephemeris_data[i]["toa"], 12, 1)).join([subframe_struct[i][alm_subid][:90], subframe_struct[i][alm_subid][98:]])
                        # I_0 replaced with delta_i
                        subframe_struct[i][alm_subid] = (tows_binary_data(16, ephemeris_data[i]["delta_i"], 19, 1)).join([subframe_struct[i][alm_subid][:98], subframe_struct[i][alm_subid][114:]])

                        subframe_struct[i][alm_subid] = (tows_binary_data(16, ephemeris_data[i]["omegaDot"], 38, math.pi)).join([subframe_struct[i][alm_subid][:120], subframe_struct[i][alm_subid][136:]])
                        subframe_struct[i][alm_subid] = (tows_binary_data(24, ephemeris_data[i]["sqrt_A"], 11, 1)).join([subframe_struct[i][alm_subid][:150], subframe_struct[i][alm_subid][174:]])
                        subframe_struct[i][alm_subid] = (tows_binary_data(24, ephemeris_data[i]["Omega_0"], 23, math.pi)).join([subframe_struct[i][alm_subid][:180], subframe_struct[i][alm_subid][204:]])
                        subframe_struct[i][alm_subid] = (tows_binary_data(24, ephemeris_data[i]["Omega"], 23, math.pi)).join([subframe_struct[i][alm_subid][:210], subframe_struct[i][alm_subid][234:]])
                        subframe_struct[i][alm_subid] = (tows_binary_data(24, ephemeris_data[i]["M_0"], 23, math.pi)).join([subframe_struct[i][alm_subid][:240], subframe_struct[i][alm_subid][264:]])
                        subframe_struct[i][alm_subid] = (binary_data(8, float(ephemeris_data[i]["Health"]), 0, 1)).join([subframe_struct[i][alm_subid][:136], subframe_struct[i][alm_subid][144:]])
                        tbits = (tows_binary_data(11, ephemeris_data[i]["a_f0"], 20, 1))
                                        #print(tbits)
                        subframe_struct[i][alm_subid] = (tbits[0:8]).join([subframe_struct[i][alm_subid][:270], subframe_struct[i][alm_subid][278:]])
                        subframe_struct[i][alm_subid] = (tbits[8:]).join([subframe_struct[i][alm_subid][:289], subframe_struct[i][alm_subid][292:]])
                        subframe_struct[i][alm_subid] = (tows_binary_data(11, ephemeris_data[i]["a_f1"], 38, 1)).join([subframe_struct[i][alm_subid][:278], subframe_struct[i][alm_subid][289:]])
                    if alm_subid == 5 and i in constants.sub5mapping_SV_map.keys():
                                    #print("ok 5")
                        subframe_struct[i][alm_subid] = subframeid.get(alm_subid).join([subframe_struct[i][alm_subid][:49], subframe_struct[i][alm_subid][52:]])
                        subframe_struct[i][alm_subid] = binary_data(8, ephemeris_data[i]["page id"], 0,1).join([subframe_struct[i][alm_subid][:60], subframe_struct[i][alm_subid][68:]])
                        subframe_struct[i][alm_subid] = (tows_binary_data(16, ephemeris_data[i]["e"], 21, 1)).join([subframe_struct[i][alm_subid][:68], subframe_struct[i][alm_subid][84:]])
                        subframe_struct[i][alm_subid] = (binary_data(8, ephemeris_data[i]["toa"], 12, 1)).join([subframe_struct[i][alm_subid][:90], subframe_struct[i][alm_subid][98:]])
                        # I_0 replaced with delta_i
                        subframe_struct[i][alm_subid] = (tows_binary_data(16, ephemeris_data[i]["delta_i"], 19, 1)).join([subframe_struct[i][alm_subid][:98], subframe_struct[i][alm_subid][114:]])
                        subframe_struct[i][alm_subid] = (tows_binary_data(16, ephemeris_data[i]["omegaDot"], 38, math.pi)).join([subframe_struct[i][alm_subid][:120], subframe_struct[i][alm_subid][136:]])
                        subframe_struct[i][alm_subid] = (binary_data(8, float(ephemeris_data[i]["Health"]), 0, 1)).join([subframe_struct[i][alm_subid][:136], subframe_struct[i][alm_subid][144:]])
                        subframe_struct[i][alm_subid] = (tows_binary_data(24, ephemeris_data[i]["sqrt_A"], 11, 1)).join([subframe_struct[i][alm_subid][:150], subframe_struct[i][alm_subid][174:]])
                        subframe_struct[i][alm_subid] = (tows_binary_data(24, ephemeris_data[i]["Omega_0"], 23, math.pi)).join([subframe_struct[i][alm_subid][:180], subframe_struct[i][alm_subid][204:]])
                        subframe_struct[i][alm_subid] = (tows_binary_data(24, ephemeris_data[i]["Omega"], 23, math.pi)).join([subframe_struct[i][alm_subid][:210], subframe_struct[i][alm_subid][234:]])
                        subframe_struct[i][alm_subid] = (tows_binary_data(24, ephemeris_data[i]["M_0"], 23, math.pi)).join([subframe_struct[i][alm_subid][:240], subframe_struct[i][alm_subid][264:]])
                        tbits = (tows_binary_data(11, ephemeris_data[i]["a_f0"], 20, 1))
                        subframe_struct[i][alm_subid] = (tbits[0:8]).join( [subframe_struct[i][alm_subid][:270], subframe_struct[i][alm_subid][278:]])
                        subframe_struct[i][alm_subid] = (tbits[8:]).join([subframe_struct[i][alm_subid][:289], subframe_struct[i][alm_subid][293:]])
                        subframe_struct[i][alm_subid] = (tows_binary_data(11, ephemeris_data[i]["a_f1"], 38, 1)).join([subframe_struct[i][alm_subid][:278], subframe_struct[i][alm_subid][289:]])
                else:
                    pass
    return subframe_struct

def raw_nav_bits_to_almanac_subframe(filename):

    Keysight_data = pd.read_csv(filename)
    BladeRF_data = Keysight_data.to_dict(orient="record")

    Parsed_data = {}  # copy.deepcopy(constdict.subframes)
    # Create a structure of 32 with in satellites each with 5 subframes

    for record in BladeRF_data:
        tempr = {}
        tempr["Bits"] = ""
        for i in range(1, 38):
            # print(i)
            tempr["Bits"] += format(record['Raw Navbits – ' + str(i)], '08b')
        tempr['TOWC'] = record['TOWC (corresponding to start of the sub-frame) (s)']
        tempr["WN"] = record['Week No (corresponding to start of the sub-frame)']
        tempr["PRN"] = record["PRN"]

        tempr['subframe id'] = record['subframe id']
        # print(tempr["PRN"],tempr['subframe id'])

        if tempr["PRN"] in Parsed_data:
            if tempr['subframe id'] in Parsed_data[tempr["PRN"]]:
                pass
            else:
                Parsed_data[tempr["PRN"]][tempr["subframe id"]] = tempr
        else:
            if tempr['subframe id'] == 1:
                Parsed_data[tempr["PRN"]] = {}
                Parsed_data[tempr["PRN"]][tempr["subframe id"]] = tempr
    # print(Parsed_data)

    Valid_PRNs = list(Parsed_data.keys())
    print("Found PRNs - ", Valid_PRNs)
    ephemeris_list=[]
    almanac_list_4 = []
    almanac_list_5 = []

    if len(Valid_PRNs) > 0:
        Valid_PRN = Valid_PRNs[1]

        subframe_struct = copy.deepcopy(constdict.almanac_subframes)
        ephemeris4_25=[]
        ephemeris5_25=[]
        for record in BladeRF_data:

            tempr = {}
            tempr["Bits"] = "10001011"
            for i in range(1, 38):
                tempr["Bits"] += format(record['Raw Navbits – ' + str(i)], '08b')
            tempr['TOWC'] = record['TOWC (corresponding to start of the sub-frame) (s)']
            tempr["WN"] = record['Week No (corresponding to start of the sub-frame)']
            tempr["PRN"] = record["PRN"]
            tempr['subframe id'] = record['subframe id']

            if tempr['subframe id'] == 4 and tempr["PRN"] == Valid_PRN:
                pid = binary(tempr["Bits"][62:68])
                temp = {}
                if pid in constants.sub4mapping_SV_map.keys():
                    actual_pid = constants.sub4mapping_actual_SV_map[pid]
                    temp["Comment"] = "Almanac for SV - " + str(actual_pid)
                    temp["Bits"]=tempr["Bits"]
                    print(constants.sub4mapping_SV_map.get(pid))
                    subframe_struct[constants.sub4mapping_SV_map.get(pid)][tempr['subframe id']]=temp["Bits"]


                elif pid in constants.sub4mapping_25_map.keys():
                    actual_pid = constants.sub4mapping_25_map[pid]
                    temp["Comment"] =tempr["Bits"]  # "Sub 25 - All SV Health"
                    #temp["Comment"] = "Almanac for SV - " + str(actual_pid)
                    temp["Bits"] = tempr["Bits"]
                    subframe_struct[constants.sub4mapping_25_map.get(pid)][tempr['subframe id']] = temp["Bits"]



                else:
                    actual_pid = "NA"
                    temp["Comment"] = tempr["Bits"]

                #print(ephemeris)
                temp["pid"] = pid
                temp["actual_pid"] = actual_pid
                temp["TOWC"] = tempr['TOWC']
                almanac_list_4.append(temp)

            if tempr['subframe id'] == 5 and tempr["PRN"] == Valid_PRN:

                pid = binary(tempr["Bits"][62:68])
                temp = {}
                if pid in constants.sub5mapping_SV_map.keys():
                    actual_pid = constants.sub5mapping_SV_map[pid]
                    temp["Comment"] = "Almanac for SV - " + str(actual_pid)
                    #temp["Comment"] = "Almanac for SV - " + str(actual_pid)
                    temp["Bits"] = tempr["Bits"]
                    subframe_struct[constants.sub5mapping_SV_map.get(pid)][tempr['subframe id']] = temp["Bits"]


                elif pid in constants.sub5mapping_25_map.keys():
                    actual_pid = constants.sub5mapping_25_map[pid]
                    temp["Comment"] =  tempr["Bits"] #"Sub 25 - All SV Health"
                    #temp["Comment"] = "Almanac for SV - " + str(actual_pid)
                    temp["Bits"] = tempr["Bits"]
                    subframe_struct[constants.sub5mapping_25_map.get(pid)][tempr['subframe id']] = temp["Bits"]

                else:
                    actual_pid = "NA"
                    temp["Comment"] = tempr["Bits"]

                temp["pid"] = pid
                temp["actual_pid"] = actual_pid
                temp["TOWC"] = tempr['TOWC']

                almanac_list_5.append(temp)

        return almanac_list_4, almanac_list_5,subframe_struct