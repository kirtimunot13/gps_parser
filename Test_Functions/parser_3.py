
import pandas as pd
import math
import csv
import constdict
import copy
#BladeRF_data = pd.read_csv("BladeRF.csv")
Keysight_data = pd.read_csv("RNBB_L1.csv")
#Keysight_data = pd.read_csv("2-3-2019-keysight.csv")
#Keysight_data = pd.read_csv("RNBB_L1.csv")
#Keysight_data = pd.read_csv("RNBB_L1_try1.csv")


BladeRF_data = Keysight_data.to_dict(orient="record")
#print(BladeRF_data[0])

pi=3.1415926535898
def twos_complement(binary_string):
    temp = int(binary_string,2)
    if binary_string[0] == "1" :
        temp = temp - 2**len(binary_string)
    return temp

def binary(binary_string):
    temp = int(binary_string,2)
    return temp    

Parsed_data = []
for record in BladeRF_data :
    temp = {}
    temp["Bits"] = ""
    for i in range(1,38) :
        temp["Bits"] += format( record['Raw Navbits – ' + str(i)] ,'08b')
    temp['TOWC']  = record['TOWC (corresponding to start of the sub-frame) (s)']
    temp["WN"] = record['Week No (corresponding to start of the sub-frame)']
    temp["PRN"] = record["PRN"]
    #temp['System Status-1'] = record['System Status-1']
    #temp['System Status-2'] = record['System Status-2']
    #temp['System Status-3'] = record['System Status-3']
    temp['subframe id'] = record['subframe id']
    #print(temp["Bits"])
    Parsed_data.append(temp)

#print(Parsed_data)
final_data= []

sub4mapping={25:2,26:3,27:4,28:5,29:7,30:8,31:9,32:10}


sub4mapping_new={2:25,3:26,27:4,28:5,29:7,30:8,31:9,32:10}

sub5mapping = {51: 25}
sub4mapping_page25={63:25}
pageid={1:'001',2:'010',3:'011',4:'100',5:'101'}
sub4=sub4mapping.values()
#print(sub4mapping.values())
for i in range(len(Parsed_data)):
    #print(Parsed_data[i])
    temp = copy.deepcopy(constdict.dstruct)

    record = Parsed_data[i]
    #print(record)
    record["Bits"] = "10001011" + record["Bits"]
    nav_bits = record["Bits"]
    ids=record['subframe id']
    #pagen=record['PRN']
    #print(ids,pagen)
    pid= binary(nav_bits[62:68])  #page no

    temp['TOWC'] = record['TOWC']
    temp["PRN"] = record["PRN"]
    #print(record['TOWC'])


    #print(" check",pid)
    temp['preamble'] = binary(nav_bits[0:8])
    temp['TLM_MSG'] = binary(nav_bits[8:22])
    temp['TLM_Reserved'] = binary(nav_bits[22:24])

    temp['HOW_Reserved'] = binary(nav_bits[30:47])

    temp['Alert_Antispoof'] = binary(nav_bits[47:49])
    temp['Parity_bit_solve'] = binary(nav_bits[52:54])

    if ids == 1:
        pid_HOW = pageid.get(ids)
        temp['HOW_Page_id'] = binary(pid_HOW)

    elif ids == 2:
        pid_HOW = pageid.get(ids)
        temp['HOW_Page_id'] = binary(pid_HOW)

    elif ids == 3:
        pid_HOW = pageid.get(ids)
        temp['HOW_Page_id'] = binary(pid_HOW)
    elif ids == 4:
        pid_HOW = pageid.get(ids)
        temp['HOW_Page_id'] = binary(pid_HOW)

    elif ids == 5:
        pid_HOW = pageid.get(ids)
        temp['HOW_Page_id'] = binary(pid_HOW)

    if ids==1:
        temp['subframe id'] = 1
        temp["page id"] = "-"
        temp["Week Number"]=binary(nav_bits[60:70])+1024
        temp["Accuracy"]=binary(nav_bits[72:76])
        temp["Health"] = binary(nav_bits[76:82])
        temp["T_GD"] = binary(nav_bits[196:204])*(2**(-31))
        temp["IODC"] = binary(nav_bits[82:84] + nav_bits[210: 218])
        temp["t_oc"] = binary(nav_bits[218:234])*(2**4)
        temp["a_f2"] = twos_complement(nav_bits[240:248])* (2 ** -55)
        temp["a_f1"] = twos_complement(nav_bits[248:264]) * (2 ** -43)
        temp["a_f0"] = twos_complement(nav_bits[270:292]) * (2 ** -31)
        #New Code
        temp["Code Type"] = nav_bits[70:72] #10 for CA Code 
        temp["L2P Flag"] = nav_bits[90:91] # Must be Zero 
        temp["Reserved 1"] = nav_bits[91:114] # Must be alternate 0 and 1
        temp["Reserved 2"] = nav_bits[120:144] # Must be alternate 0 and 1
        temp["Reserved 3"] = nav_bits[150:174] # Must be alternate 0 and 1
        temp["Reserved 4"] = nav_bits[180:196] # Must be alternate 0 and 1
        final_data.append(temp)
    if ids==2:
        temp['subframe id'] = 2
        temp["page id"] = "-"
        temp["IODE_sf2"]=binary(nav_bits[60:68])
        print(type(temp["IODE_sf2"]))
        temp["C_rs"]=twos_complement(nav_bits[68:84])*(2**(-5))
        print(type(temp["C_rs"]))
        temp["Deltan"] = twos_complement(nav_bits[90:106])*(2**(-43))*pi
        temp["M_0"] = (twos_complement(nav_bits[106:114]+nav_bits[120:144])*(2**(-31))) * pi

        temp["C_uc"] = twos_complement((nav_bits[150:166]) )*(2**(-29))
        temp["e"] = binary((nav_bits[166:174]+nav_bits[180:204]))*(2**(-33))
        temp["C_us"] = twos_complement(nav_bits[210:226])* (2 ** -29)
        temp["sqrt_A"] = binary(nav_bits[226:234]+nav_bits[240:264]) * (2 ** -19)
        temp["t_oe"] = binary(nav_bits[270:286]) * (2 ** 4)

        # New Code
        temp["FIT INTERVAL"] = nav_bits[286:287]  # if ephemeris is older than 4 hours then 1
        temp["AODO"] = nav_bits[287:292]


        #print(temp["PRN"], temp["M_0"], " ", (nav_bits[106:114] + nav_bits[120:144]))
        GM_EARTH = 3.986005 * (10 ** 14)
        n = math.sqrt( GM_EARTH / ((float(temp["sqrt_A"]))**6))


        tk = float(temp["t_oe"]) - 61440
        Mk = -0.4746887819 + n* (tk)
        print(Mk)
        multiplier  = int(Mk/math.pi)
        print(multiplier)
        if multiplier % 2  != 0 :
            Mk = Mk  - (multiplier) * math.pi  + math.pi
        else :
            Mk = Mk - (multiplier) * math.pi

        print(temp["PRN"], temp["M_0"], " Mk Calculated - ", Mk)

        # New Code
        temp["FIT INTERVAL"] = nav_bits[286:287] # if ephemeris is older than 4 hours then 1
        temp["AODO"] =nav_bits[287:292]

        final_data.append(temp)
    if ids == 3:
        temp['subframe id'] = 3
        temp["page id"] = "-"
        temp["C_ic"] = twos_complement(nav_bits[60:76])*(2** -29)
        temp["Omega_0"] = twos_complement(nav_bits[76:84]+nav_bits[90:114]) * (2 ** (-31))*pi
        temp["C_is"] = twos_complement(nav_bits[120:136]) * (2 ** (-31)) * pi
        temp["I_0"] = twos_complement(nav_bits[136:144]+nav_bits[150:174]) * (2 ** (-31)) * pi
        temp["C_rc"] = twos_complement(nav_bits[180:196] ) * (2 ** (-5))
        temp["Omega"] = twos_complement(nav_bits[196:204] + nav_bits[210:234]) * (2 ** (-31))*pi
        temp["omegaDot"] = twos_complement(nav_bits[240:264]) * (2 ** -43)*pi
        temp["IODE_sf3"] = binary( nav_bits[270:278])
        temp["iDot"] = binary(nav_bits[278:292]) * (2 ** -43)*pi
        final_data.append(temp)

    if ids==4 and pid in sub4mapping.keys():
        #if sub4mapping.values()
        #print("in loop 4")
        temp['subframe id']=4
        temp["page id"] = sub4mapping[pid] # 60 : 68
        temp["ALM4_e"] = binary(nav_bits[68:84]) * (2 ** (-21))

        temp["ALM4_toa"] = binary(nav_bits[90:98]) * (2 ** 12)
        temp["ALM4_Delta_I"] = twos_complement(nav_bits[98:114]) * (2 ** (-19)) 
        temp["ALM4_omegaDot"] = twos_complement(nav_bits[120:136]) * (2 ** (-38)) * pi
        temp["ALM4_Sqaure Root A"] = binary((nav_bits[150:174])) * (2 ** (-11))
        temp["ALM4_Omega_0"] = twos_complement(nav_bits[180:204]) * (2**(-23))* pi
        temp["ALM4_Omega"] = twos_complement(nav_bits[210:234]) * (2 ** (-23)) * pi
        temp["ALM4_M_0"] = twos_complement(nav_bits[240:264]) * (2 ** (-23)) * pi
        temp["ALM4_a_f0"] = twos_complement((nav_bits[270:278] + nav_bits[289:292])) * (2 ** (-20))
        temp["ALM4_a_f1"] = twos_complement(nav_bits[278:289]) * (2 ** -38)
        #New Code 
        temp["ALM4_SV HEALTH_4"] = nav_bits[136:144] # all zeros
        final_data.append(temp)


    if ids == 4 and pid in sub4mapping_page25.keys():
        # if sub4mapping.values()
        # print("in loop 4")
        temp['subframe id'] = 4
        temp["page id"] = sub4mapping_page25[pid]
        temp["ALM4_A_SPOOF_SV1"] = binary((nav_bits[68:72]))
        temp["ALM4_A_SPOOF_SV2"] = binary((nav_bits[72:76]))
        temp["ALM4_A_SPOOF_SV3"] = binary((nav_bits[76:80]))
        temp["ALM4_A_SPOOF_SV4"] = binary((nav_bits[80:84]))
        temp["ALM4_A_SPOOF_SV5"] = binary((nav_bits[90:94]))
        temp["ALM4_A_SPOOF_SV6"] = binary((nav_bits[94:98]))
        temp["ALM4_A_SPOOF_SV7"] = binary((nav_bits[98:102]))
        temp["ALM4_A_SPOOF_SV8"] = binary((nav_bits[102:106]))
        temp["ALM4_A_SPOOF_SV9"] = binary((nav_bits[106:110]))
        temp["ALM4_A_SPOOF_SV10"] = binary((nav_bits[110:114]))
        temp["ALM4_A_SPOOF_SV11"] = binary((nav_bits[120:124]))
        temp["ALM4_A_SPOOF_SV12"] = binary((nav_bits[124:128]))
        temp["ALM4_A_SPOOF_SV13"] = binary((nav_bits[128:132]))
        temp["ALM4_A_SPOOF_SV14"] = binary((nav_bits[132:136]))
        temp["ALM4_A_SPOOF_SV15"] = binary((nav_bits[136:140]))
        temp["ALM4_A_SPOOF_SV16"] = binary((nav_bits[140:144]))
        temp["ALM4_A_SPOOF_SV17"] = binary((nav_bits[150:154]))
        temp["ALM4_A_SPOOF_SV18"] = binary((nav_bits[154:158]))
        temp["ALM4_A_SPOOF_SV19"] = binary((nav_bits[158:162]))
        temp["ALM4_A_SPOOF_SV20"] = binary((nav_bits[162:166]))
        temp["ALM4_A_SPOOF_SV21"] = binary((nav_bits[166:170]))
        temp["ALM4_A_SPOOF_SV22"] = binary((nav_bits[170:174]))
        temp["ALM4_A_SPOOF_SV23"] = binary((nav_bits[180:184]))
        temp["ALM4_A_SPOOF_SV24"] = binary((nav_bits[184:188]))
        temp["ALM4_A_SPOOF_SV25"] = binary((nav_bits[188:192]))
        temp["ALM4_A_SPOOF_SV26"] = binary((nav_bits[192:196]))
        temp["ALM4_A_SPOOF_SV27"] = binary((nav_bits[196:200]))
        temp["ALM4_A_SPOOF_SV28"] = binary((nav_bits[200:204]))
        temp["ALM4_A_SPOOF_SV29"] = binary((nav_bits[210:214]))
        temp["ALM4_A_SPOOF_SV30"] = binary((nav_bits[214:218]))
        temp["ALM4_A_SPOOF_SV31"] = binary((nav_bits[218:222]))
        temp["ALM4_A_SPOOF_SV32"] = binary((nav_bits[222:226]))
        temp["ALM4_sub4_Res1"] = nav_bits[226:228]
        temp["ALM4_Health_SV25"] = binary((nav_bits[228:234]))
        temp["ALM4_Health_SV26"] = binary((nav_bits[240:246]))
        temp["ALM4_Health_SV27"] = binary((nav_bits[246:252]))
        temp["ALM4_Health_SV28"] = binary((nav_bits[252:258]))
        temp["ALM4_Health_SV29"] = binary((nav_bits[258:264]))
        temp["ALM4_Health_SV30"] = binary((nav_bits[270:276]))
        temp["ALM4_Health_SV31"] = binary((nav_bits[276:282]))
        temp["ALM4_Health_SV32"] = binary((nav_bits[282:288]))
        temp["ALM4_sub4_Res2"] = nav_bits[288:292]
        final_data.append(temp)

    if (ids==5) and pid in range(1,25):
        #print("in loop 5")
        temp['subframe id'] = 5
        temp["page id"] = pid # 60:68
        temp["ALM5_e"] = binary(nav_bits[68:84])*(2**(-21))
        temp["ALM5_toa"] = binary(nav_bits[90:98])*(2**12)
        temp["ALM5_Delta_I"] = twos_complement(nav_bits[98:114])*(2**(-19))
        temp["ALM5_omegaDot"] = twos_complement(nav_bits[120:136])*(2**(-38))*pi
        temp["ALM5_Sqaure Root A"] = binary((nav_bits[150:174]))*(2**(-11))
        temp["ALM5_Omega_0"] = twos_complement(nav_bits[180:204])*(2**(-23))* pi
        temp["ALM5_Omega"] = twos_complement(nav_bits[210:234]) *(2**(-23))*pi
        temp["ALM5_M_0"] = twos_complement(nav_bits[240:264]) *(2** (-23))*pi
        temp["ALM5_a_f0"] = twos_complement((nav_bits[270:278] +  nav_bits[289: 292]))*(2**(-20))
        temp["ALM5_a_f1"] = twos_complement(nav_bits[278:289]) *(2**-38)
        #New Code 
        temp["ALM5_SV HEALTH_5"] = nav_bits[136:144] # all zeros 
        final_data.append(temp)

    if ids == 5 and pid in sub5mapping.keys():
        temp['subframe id'] = 5
        temp["page id"] = sub5mapping[pid]
        temp["ALM5P25_toa"] = binary(nav_bits[68:76])
        temp["ALM5_Week Number"] = binary(nav_bits[76:84]) + 1024
        temp["ALM5_Health_SV1"] = binary((nav_bits[90:96]))
        temp["ALM5_Health_SV2"] = binary((nav_bits[96:102]))
        temp["ALM5_Health_SV3"] = binary((nav_bits[102:108]))
        temp["ALM5_Health_SV4"] = binary((nav_bits[108:114]))
        temp["ALM5_Health_SV5"] = binary((nav_bits[120:126]))
        temp["ALM5_Health_SV6"] = binary((nav_bits[126:132]))
        temp["ALM5_Health_SV7"] = binary((nav_bits[132:138]))
        temp["ALM5_Health_SV8"] = binary((nav_bits[138:144]))
        temp["ALM5_Health_SV9"] = binary((nav_bits[150:156]))
        temp["ALM5_Health_SV10"] = binary((nav_bits[156:162]))
        temp["ALM5_Health_SV11"] = binary((nav_bits[162:168]))
        temp["ALM5_Health_SV12"] = binary((nav_bits[168:174]))
        temp["ALM5_Health_SV13"] = binary((nav_bits[180:186]))
        temp["ALM5_Health_SV14"] = binary((nav_bits[186:192]))
        temp["ALM5_Health_SV15"] = binary((nav_bits[192:198]))
        temp["ALM5_Health_SV16"] = binary((nav_bits[198:204]))
        temp["ALM5_Health_SV17"] = binary((nav_bits[210:216]))
        temp["ALM5_Health_SV18"] = binary((nav_bits[216:222]))
        temp["ALM5_Health_SV19"] = binary((nav_bits[222:228]))
        temp["ALM5_Health_SV20"] = binary((nav_bits[228:234]))
        temp["ALM5_Health_SV21"] = binary((nav_bits[240:246]))
        temp["ALM5_Health_SV22"] = binary((nav_bits[246:252]))
        temp["ALM5_Health_SV23"] = binary((nav_bits[252:258]))
        temp["ALM5_Health_SV24"] = binary((nav_bits[258:264]))
        temp['ALM5_sub5_Res1']=binary(nav_bits[271:277])
        temp['ALM5_sub5_Res2'] = binary(nav_bits[277:292])
        final_data.append(temp)
#print("final",final_data)

csv_columns=[]
s=final_data[0]
for rec in s:
    csv_columns.append(rec)

#print(csv_columns)
csv_file = "Keysight_Parsed_output.csv"
try:
    with open(csv_file, 'w') as csvfile:
        writer = csv.DictWriter(csvfile,delimiter=',', lineterminator='\n',fieldnames=csv_columns)
        writer.writeheader()
        for data in final_data:
            writer.writerow(data)
except IOError:
    print("I/O error")
print("Done Writing to Excel.")

    #1 010 1101100 010 0 010 111100 010 101  304




