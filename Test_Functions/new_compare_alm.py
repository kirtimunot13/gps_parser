#import pandas as pd
import math
import csv
import constdict
import copy
from Basic_functions import dump_list_to_csv
from read_alamanc_file import get_alamanac,get_almanac_from_raw_navbits_new
from subframe_creation import ephemeris_to_subframe,raw_nav_bits_to_subframe_structure_new #,add_time_and_parity
'''=================CODE TO GENERATE SUBFRAMES FROM ALMANAC FILES==================================================='''

# GET ALL EPHEMERIS
ephemeris_from_file = get_alamanac(filename = r"UPLOADED_ALAMANAC\March1_2019_yuma.txt")
#print("ephemeris almnac data", ephemeris_from_file)
# FUNCTION TO CREATE 3 SUBFRAMES FROM EACH SET OF EPHEMERIS
subframes_from_almanac = ephemeris_to_subframe(ephemeris_from_file)
#print("almnac data",subframes_from_almanac)
# ADD TOE , TOWC, TOC and PARITY
towc = 1000 # PLEASE ADD AS ACTUAL FROM THE FILES HERE
toe = 1000
toc = 1000
#toc = ceil of towc by 7200 factor
#toe = toc
#subframes_from_almanac = add_time_and_parity(subframes = subframes_from_almanac,toe= toe,towc =towc,toc=toc)

'''================================================================================================================'''

'''========================CODE TO REGENERATE SUBFRAMES FROM RAW NAV BITS=========================================='''
# FUNCTION TO READ THE EXISTING RAW NAV BIT LOG TO CONVERT INTO THE EPHEMERIS DICT FROM constdic
ephemeris_from_file2 = get_almanac_from_raw_navbits_new(filename=r"RAW_NAV_BITS\Keysight.csv") #parser3 epho to subframe
print("ephemeris almnac data", ephemeris_from_file)
print("function o/p ephemeris keysight data",ephemeris_from_file2)
# FUNCTION TO CREATE 3 SUBFRAMES FROM EACH SET OF EPHEMERIS
subframes_from_log = ephemeris_to_subframe(ephemeris_from_file2)
#print("keysight data",subframes_from_log)
# ADD TOE , TOWC, TOC and PARITY
towc = 432199 # PLEASE ADD AS ACTUAL FROM THE FILES HERE
toe = 1000
toc = (math.ceil(towc / 7200) * 7200)

#toc = ceil of towc by 7200 factor
#toe = toc
#subframes_from_log = add_time_and_parity(subframes = subframes_from_log,toe= toe,towc =towc,toc=toc)
'''================================================================================================================'''

'''========================CODE TO CONVERT THE RAW NAVBITS INTO THE SUBFRAME FORMAT================================='''
subframes_actual = raw_nav_bits_to_subframe_structure_new(filename=r"RAW_NAV_BITS\Keysight.csv")  #nave_bit
print("function output",subframes_actual[0])
'''================================================================================================================='''

# ONCE ALL OF THE DATE IS IN SAME FORMAT USE FOR LOOP TO CREATE THE CSV
# ALL MATH FUNCTIONS SHOULD BE A MODULE in DIRECTORY oF BASIC FUNCTIONS ONLY AND REUSE WHENEVER REQUIRED
subframes_actual=subframes_actual[0]
csv_dump=[]

for i in range(1,3):
    for j in range(1,33):
        sub_split_from_almanac=[]
        sub_split_from_log=[]
        sub_split_actual=[]
        n=30
        #print(i, j)
        if subframes_from_almanac[j][i] !='' and subframes_from_log[j][i]!='' and subframes_actual[j][i]!='':
            sub_split_from_almanac=[subframes_from_almanac[j][i][k:k+n] for k in range(0,len(subframes_from_almanac[j][i]),n)]
            sub_split_from_almanac.insert(0,i)
            sub_split_from_almanac.insert(1,j)
            sub_split_from_almanac.insert(2,"from almnac")
            sub_split_from_log=[subframes_from_log[j][i][k:k+n] for k in range(0,len(subframes_from_log[j][i]),n)]
            sub_split_from_log.insert(0, i)
            sub_split_from_log.insert(1, j)
            sub_split_from_log.insert(2, "from log   ")
            sub_split_actual=[subframes_actual[j][i][k:k+n] for k in range(0,len(subframes_actual[j][i]),n)]
            sub_split_actual.insert(0, i)
            sub_split_actual.insert(1, j)
            sub_split_actual.insert(2, "from Actual")
            csv_dump.append(sub_split_from_almanac)
            csv_dump.append(sub_split_from_log)
            csv_dump.append(sub_split_actual)


t=dump_list_to_csv(filename="dump_data.csv",dlist=csv_dump)
if t==True:
    print("Done writing excel")
else:
    print("Unsuccesful")
'''print("subframes_from_almanac",sub_split_from_almanac)
print("subframes_from_log",sub_split_from_log)
print("sub_split_actual",sub_split_actual)'''