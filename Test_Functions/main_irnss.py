from SV_functions import read_alamanc_file
from SV_functions import subframe_creation,constdict
from Basic_functions import dump_list_to_csv,compare_almanac_bits


eph=read_alamanc_file.irnss_get_almanac_from_raw_navbits_new("D:\gps_parser\RAW_NAV_BITS\RNBB_L5.csv")
print(eph)

sub=subframe_creation.irnss_ephemeris_to_subframe(eph)
print(sub)

generated_data=read_alamanc_file.irnss_get_almanac_from_generated_navbits_new(sub)
print(generated_data)

raw_sub=subframe_creation.irnss_raw_nav_bits_to_subframe_structure_new("D:\gps_parser\RAW_NAV_BITS\RNBB_L5.csv")
print(raw_sub)


csv_dump=[]
not_equal_data=[]
keys=list(constdict.ephemeris_template.keys())
#print("keys:",keys)
temp = ["Source"]
flag=0
for key in keys :
    temp.append(key)
#print(temp)
csv_dump.append(temp)
for i in range(1,10):
    if eph[i]!={} and generated_data[i]!={}:
        file2 = [ "From RNBB Parameters"]
        file1=["From generated Parameters"]
        for key in keys:
            #to_append = ['from almanac']
            #to_append.extend(split_string)
            file2.append(eph[i][key])
            file1.append(generated_data[i][key])

        csv_dump.append(file2)
        csv_dump.append(file1)
        csv_dump.append([])

t = dump_list_to_csv(filename=r"..\Output\irnss_dump_ephemeris_list.csv", dlist=csv_dump)
if t==True:
    print("Done writing excel")
else:
    print("data Unsuccesful")


valid_PRNs,csv_list = compare_almanac_bits(logs=[sub,raw_sub])
t=dump_list_to_csv(filename="D:\gps_parser\Output\dump_data_irnss.csv",dlist=csv_list)
if t==True:
    print("Done writing excel")
else:
    print("bit Unsuccesful")