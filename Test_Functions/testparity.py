import pandas as pd
import math
import csv
import copy

# Takes the TEST CSV INPUT

parsed_data = pd.read_excel("test.xlsx")
parsed_data = parsed_data.to_dict(orient="record")
#print(parsed_data[0])

a='0'
nparity=[]
def get_parity_bits(actual_bits, bit_29, bit_30):
    p = [0, 0, 0, 0, 0, 0]
    p[0] = bit_29 ^ int(actual_bits[0]) ^ int(actual_bits[1]) ^ int(actual_bits[2]) ^ int(actual_bits[4]) ^ int(actual_bits[5]) ^ int(actual_bits[9]) ^ int(actual_bits[10]) ^ int(actual_bits[11]) ^ int( actual_bits[12]) ^ int(actual_bits[13]) ^ int(actual_bits[16]) ^ int(actual_bits[17]) ^ int(actual_bits[19]) ^ int(actual_bits[22])
    p[1] = bit_30 ^ int(actual_bits[1]) ^ int(actual_bits[2]) ^ int(actual_bits[3]) ^ int(actual_bits[5]) ^ int(actual_bits[6]) ^ int(actual_bits[10]) ^ int(actual_bits[11]) ^ int(actual_bits[12]) ^ int(actual_bits[13]) ^ int(actual_bits[14]) ^ int(actual_bits[17]) ^ int(actual_bits[18]) ^ int(actual_bits[20]) ^ int(actual_bits[23])
    p[2] = bit_29 ^ int(actual_bits[0]) ^ int(actual_bits[2]) ^ int(actual_bits[3]) ^ int(actual_bits[4]) ^ int( actual_bits[6]) ^ int(actual_bits[7]) ^ int(actual_bits[11]) ^ int(actual_bits[12]) ^ int(actual_bits[13]) ^ int(actual_bits[14]) ^ int(actual_bits[15]) ^ int(actual_bits[18]) ^ int(actual_bits[19]) ^ int(actual_bits[21])
    p[3] = bit_30 ^ int(actual_bits[1]) ^ int(actual_bits[3]) ^ int(actual_bits[4]) ^ int(actual_bits[5]) ^ int(actual_bits[7]) ^ int(actual_bits[8]) ^ int(actual_bits[12]) ^ int(actual_bits[13]) ^ int( actual_bits[14]) ^ int(actual_bits[15]) ^ int(actual_bits[16]) ^ int(actual_bits[19]) ^ int(actual_bits[20]) ^ int(actual_bits[22])
    p[4] = bit_30 ^ int(actual_bits[0]) ^ int(actual_bits[2]) ^ int(actual_bits[4]) ^ int(actual_bits[5]) ^ int(actual_bits[6]) ^ int(actual_bits[8]) ^ int(actual_bits[9]) ^ int(actual_bits[13]) ^ int(actual_bits[14]) ^ int(actual_bits[15]) ^ int(actual_bits[16]) ^ int(actual_bits[17]) ^ int(actual_bits[20]) ^ int(actual_bits[21]) ^ int(actual_bits[23])
    p[5] = bit_29 ^ int(actual_bits[2]) ^ int(actual_bits[4]) ^ int(actual_bits[5]) ^ int(actual_bits[7]) ^ int( actual_bits[8]) ^ int(actual_bits[9]) ^ int(actual_bits[10]) ^ int(actual_bits[12]) ^ int(actual_bits[14]) ^ int(actual_bits[18]) ^ int(actual_bits[21]) ^ int(actual_bits[23]) ^ int(actual_bits[22])
    parity = str(p[0]) + str(p[1]) + str(p[2]) + str(p[3]) + str(p[4]) + str(p[5])

    return parity

for j in range(1 ,len(parsed_data) ,2):
    bit_29 = 0
    bit_30 = 0
    for i in range(2, 9):
        if i==2 :
            bit_29 = 0
            bit_30 = 0
            parsed_data[j][22]=1
            parsed_data[j][23]=1

        else:
            old_bits = parsed_data[j][i - 1]
            #print(old_bits)
            bit_29 = int(old_bits[28])
            bit_30 = int(old_bits[29])

        actual_bits = parsed_data[j][i]
        parity = get_parity_bits(actual_bits, bit_29, bit_30)
        print("GENERATED before PARITY- ",j,i,"::", actual_bits)
        actual_bits=parity.join([actual_bits[:24], actual_bits[30:]])
        print("GENERATED after PARITY- ",j,i,"::", actual_bits)
        parsed_data[j][i]=actual_bits
        print(i," ",parsed_data[j][i])


