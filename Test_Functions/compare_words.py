import pandas as pd
import math
import csv
import constdict
import copy
# TO DO ---------------------
#toc, toe toa factor taken an zero


Keysight_data = pd.read_csv("Keysight.csv")
# Keysight_data = pd.read_csv("Keysight.csv")
# Keysight_data = pd.read_csv("2-3-2019-keysight.csv")
# Keysight_data = pd.read_csv("RNBB_L1.csv")
# Keysight_data = pd.read_csv("RNBB_L1_try1.csv")


BladeRF_data = Keysight_data.to_dict(orient="record")
#print(BladeRF_data[0])

pi = 1


def twos_complement(binary_string):
    temp = int(binary_string, 2)
    if binary_string[0] == "1":
        temp = temp - 2 ** len(binary_string)
    return temp


def binary(binary_string):
    temp = int(binary_string, 2)
    return temp

def binary_data(nav_bits,value,fact,pi):
    #print("o:", value)
    #print("b:", nav_bits)
    n = int(len(nav_bits))
    test = int(value/(2**fact)*pi)
    binary_string = '{0:{fill}{width}b}'.format(test, fill='0', width=n)
    #print("expo b", binary_string)
    return binary_string


def tows_binary_data(nav_bits, value,fact,pi):
    #print("-------------------------------------------------")
    #print("o:", value)
    #print("b:", nav_bits)
    n = int(len(nav_bits))
    test = int(value * (2 ** fact)/pi)
    #print("float:", value * (2 ** (fact)))
    #print("test::", test)
    twos_binary_string = '{0:{fill}{width}b}'.format((test + 2 ** n) % 2 ** n, fill='0', width=n)
    #print("2b", twos_binary_string)
    #print("-------------------------------------------------")
    return twos_binary_string





Parsed_data = []
for record in BladeRF_data :
    temp = {}
    temp["Bits"] = ""
    for i in range(1,38) :
        temp["Bits"] += format( record['Raw Navbits – ' + str(i)] ,'08b')
    temp['TOWC']  = record['TOWC (corresponding to start of the sub-frame) (s)']
    temp["WN"] = record['Week No (corresponding to start of the sub-frame)']
    temp["PRN"] = record["PRN"]
    #temp['System Status-1'] = record['System Status-1']
    #temp['System Status-2'] = record['System Status-2']
    #temp['System Status-3'] = record['System Status-3']
    temp['subframe id'] = record['subframe id']
    #print(temp["Bits"])
    Parsed_data.append(temp)

# print(Parsed_data)
final_data = []
sid=[]
original_bits=[]
blist = []
towc=[]
prn=[]
bits = ''
nav_bits=''
for i in range(0, 304):
    bits += 'p'
'''print(bits[0:10])
t='1001100100'
bits=t.join([bits[:0],bits[10:]])
print(bits)'''
sub4mapping = {25: 2, 26: 3, 27: 4, 28: 5, 29: 7, 30: 8, 31: 9, 32: 10}
sub5mapping = {51: 25}
sub4mapping_page25={63:25}
pageid={1:'001',2:'010',3:'011',4:'100',5:'101'}
#sub4 = sub4mapping.values()
#print(sub4mapping.values())

for i in range(len(Parsed_data)):
    
    temp = copy.deepcopy(constdict.dstruct)
    record = Parsed_data[i]
    #print(record)
    record["Bits"] = "10001011" + record["Bits"]
    nav_bits = record["Bits"]
    ids = record['subframe id']
    # pagen=record['PRN']
    # print(ids,pagen)
    pid = binary(nav_bits[62:68])  # page no
    temp['TOWC'] = record['TOWC']
    temp["PRN"] = record["PRN"]
    temp['preamble']=binary(nav_bits[0:8])
    bits = (tows_binary_data(nav_bits[0:8], temp["preamble"], 0, 1)).join([bits[:0], bits[8:]])
    temp['TLM_MSG'] = binary(nav_bits[8:22])
    bits = (tows_binary_data(nav_bits[8:22], temp["TLM_MSG"], 0, 1)).join([bits[:8], bits[22:]])
    temp['TLM_Reserved'] = binary(nav_bits[22:24])
    bits = (tows_binary_data(nav_bits[22:24], temp["TLM_Reserved"], 0, 1)).join([bits[:22], bits[24:]])
    temp['HOW_Reserved']= binary(nav_bits[30:47])
    bits = (tows_binary_data(nav_bits[30:47], temp["TLM_Reserved"], 0, 1)).join([bits[:30], bits[47:]])
    temp['Alert_Antispoof']=binary(nav_bits[47:49])
    bits = (tows_binary_data(nav_bits[47:49], temp["Alert_Antispoof"], 0, 1)).join([bits[:47], bits[49:]])
    temp['Parity_bit_solve'] = binary(nav_bits[52:54])
    bits = (tows_binary_data(nav_bits[52:54], temp["Parity_bit_solve"], 0, 1)).join([bits[:52], bits[54:]])

    if ids == 1:
        pid_HOW =pageid.get(ids)
        temp['HOW_Page_id'] = binary(pid_HOW)
        bits = (binary_data(nav_bits[49:54], temp["HOW_Page_id"], 0, 1)).join([bits[:49], bits[54:]])
    elif ids == 2:
        pid_HOW =pageid.get(ids)
        temp['HOW_Page_id'] = binary(pid_HOW)
        bits = (binary_data(nav_bits[49:54], temp["HOW_Page_id"], 0, 1)).join([bits[:49], bits[54:]])
    elif ids == 3:
        pid_HOW = pageid.get(ids)
        temp['HOW_Page_id'] = binary(pid_HOW)
        bits = (binary_data(nav_bits[49:54], temp["HOW_Page_id"], 0, 1)).join([bits[:49], bits[54:]])
    elif ids == 4:
        pid_HOW = pageid.get(ids)
        temp['HOW_Page_id'] = binary(pid_HOW)
        bits = (binary_data(nav_bits[49:54], temp["HOW_Page_id"], 0, 1)).join([bits[:49], bits[54:]])
    elif ids == 5:
        pid_HOW = pageid.get(ids)
        temp['HOW_Page_id'] = binary(pid_HOW)
        bits = (binary_data(nav_bits[49:54], temp["HOW_Page_id"], 0, 1)).join([bits[:49], bits[54:]])



    #print(" check", pid)
    if ids == 1:
        #print(" start subfram 1------------------------")
        temp['subframe id'] = 1
        temp["page id"] = "-"
        temp["Week Number"] = binary(nav_bits[60:70]) + 1024
        bits = (binary_data(nav_bits[60:70],temp["Week Number"]-1024,0,1)).join([bits[:60], bits[70:]])
        temp["Accuracy"] = binary(nav_bits[72:76])
        bits = (binary_data(nav_bits[72:76], temp["Accuracy"], 0,1)).join([bits[:72], bits[76:]])
        temp["Health"] = binary(nav_bits[76:82])
        bits = (binary_data(nav_bits[76:82], temp["Health"], 0,1)).join([bits[:76], bits[82:]])
        temp["T_GD"] = binary(nav_bits[196:204]) * (2 ** (-31))
        bits = (binary_data(nav_bits[196:204], temp["T_GD"], 31,1)).join([bits[:196], bits[204:]])
        temp["IODC"] = binary(nav_bits[82:84] + nav_bits[210: 218])
       
        #merge_bits = (binary_data(nav_bits[82:84] + nav_bits[196: 204], temp["IODC"], 0,1))
        #bits= (merge_bits[0]).join([bits[:82], bits[84:]])
        #bits = (merge_bits[1:]).join([bits[:196], bits[204:]])
        #tbits = "00000000000000000000000000000000"
        tbits = (binary_data( (nav_bits[82:84] + nav_bits[210: 218]) , temp["IODC"], 0,1))
        bits=(tbits[0:2]).join([bits[:82], bits[84:]])
        bits = (tbits[2:]).join([bits[:210], bits[218:]])
        
        temp["t_oc"] = binary(nav_bits[218:234]) #* (2 ** 4)
        bits = (binary_data(nav_bits[218:234], temp["t_oc"], 0 ,1)).join([bits[:218], bits[234:]])
        temp["a_f2"] = twos_complement(nav_bits[240:248]) * (2 ** -55)
        bits = (tows_binary_data(nav_bits[240:248], temp["a_f2"], 55,1)).join([bits[:240], bits[248:]])
        temp["a_f1"] = twos_complement(nav_bits[248:264]) * (2 ** -43)
        bits = (tows_binary_data(nav_bits[248:264], temp["a_f1"], 43,1)).join([bits[:248], bits[264:]])
        temp["a_f0"] = twos_complement(nav_bits[270:292]) * (2 ** -31)
        bits=(tows_binary_data(nav_bits[270:292], temp["a_f0"], 31,1)).join([bits[:270], bits[292:]])
        #New Code
        temp["Code Type"] = binary(nav_bits[70:72]) #10 for CA Code 
        bits = (binary_data(nav_bits[70:72],temp["Code Type"],0,1)).join([bits[:70], bits[72:]])
        temp["L2P Flag"] = binary(nav_bits[90:91]) # Must be Zero
        bits = (binary_data(nav_bits[90:91],temp["L2P Flag"],0,1)).join([bits[:90], bits[91:]])
        temp["Reserved 1"] = binary(nav_bits[91:114]) # Must be Zero
        bits = (binary_data(nav_bits[91:114],temp["Reserved 1"],0,1)).join([bits[:91], bits[114:]])
        temp["Reserved 2"] = binary(nav_bits[120:144]) # Must be Zero
        bits = (binary_data(nav_bits[120:144],temp["Reserved 2"],0,1)).join([bits[:120], bits[144:]])
        temp["Reserved 3"] = binary(nav_bits[150:174]) # Must be Zero
        bits = (binary_data(nav_bits[150:174],temp["Reserved 3"],0,1)).join([bits[:150], bits[174:]])
        temp["Reserved 4"] = binary(nav_bits[180:196]) # Must be Zero
        bits = (binary_data(nav_bits[180:196],temp["Reserved 4"],0,1)).join([bits[:180], bits[196:]])
                              
        #bits += '0000'
        #print(bits)
        original_bits.append(nav_bits)
        blist.append(bits)
        sid.append(temp['subframe id'])
        final_data.append(temp)
        towc.append(temp['TOWC'])
        prn.append(temp["PRN"])

        bits=''
        for i in range(0, 304):
            bits += 'p'
        #print(" end subfram 1-------------------------------")
    if ids == 2:
        temp['subframe id'] = 2
        temp["page id"] = "-"
        temp["IODE_sf2"] = binary(nav_bits[60:68])
        bits = (binary_data(nav_bits[60:68], temp["IODE_sf2"], 0,1)).join([bits[:60], bits[68:]])
        temp["C_rs"] = twos_complement(nav_bits[68:84]) * (2 ** (-5))
        bits = (tows_binary_data(nav_bits[68:84], temp["C_rs"], 5,1)).join([bits[:68], bits[84:]])
        temp["Deltan"] = twos_complement(nav_bits[90:106]) * (2 ** (-43)) * pi
        bits = (tows_binary_data(nav_bits[90:106], temp["Deltan"], 43,pi )).join([bits[:90], bits[106:]])
        temp["M_0"] = twos_complement(nav_bits[106:114] + nav_bits[120:144])  * (2 ** (-31)) * pi
        #tbits="00000000000000000000000000000000"
        tbits = (tows_binary_data( (nav_bits[106:114] + nav_bits[120:144]), temp["M_0"], 31, pi))
        bits = (tbits[0:8]).join([bits[:106], bits[114:]])
        bits = (tbits[8:]).join([bits[:120], bits[144:]])
        
        #print(nav_bits[106:114] + nav_bits[120:144])
        #print(tbits)
        #print("---------------------------------------")

        temp["C_uc"] = twos_complement((nav_bits[150:166])) * (2 ** (-29))
        bits = (tows_binary_data(nav_bits[150:166], temp["C_uc"], 29,1)).join([bits[:150], bits[166:]])
        temp["e"] = binary((nav_bits[166:174] + nav_bits[180:204])) * (2 ** (-33))
        tbits = (tows_binary_data(nav_bits[166:174] + nav_bits[180:204], temp["e"], 33,1))
        bits=(tbits[0:8]).join([bits[:166], bits[174:]])
        bits = (tbits[8:]).join([bits[:180], bits[204:]])
        temp["C_us"] = twos_complement(nav_bits[210:226]) * (2 ** -29)
        bits = (tows_binary_data(nav_bits[210:226], temp["C_us"], 29,1)).join([bits[:210], bits[226:]])
        temp["sqrt_A"] = binary(nav_bits[226:234] + nav_bits[240:264]) * (2 ** -19)
        tbits = (tows_binary_data(nav_bits[226:234] + nav_bits[240:264], temp["sqrt_A"], 19,1))
        bits = (tbits[0:8]).join([bits[:226], bits[234:]])
        bits = (tbits[8:]).join([bits[:240], bits[264:]])
        temp["t_oe"] = binary(nav_bits[270:286]) #* (2 ** 4)
        bits = (binary_data(nav_bits[270:286], temp["t_oe"], 0,1)).join([bits[:270], bits[286:]])
        # New Code
        temp["FIT INTERVAL"] = binary(nav_bits[286:287]) # if ephemeris is older than 4 hours then 1
        bits = (binary_data(nav_bits[286:287], temp["FIT INTERVAL"],0,1)).join([bits[:286], bits[287:]])
        temp["AODO"] = binary(nav_bits[287:292])
        bits = (binary_data(nav_bits[287:292], temp["AODO"],0,1)).join([bits[:287], bits[292:]])
        towc.append(temp['TOWC'])
        prn.append(temp["PRN"])

        #bits += '0000'
        original_bits.append(nav_bits)
        blist.append(bits)
        sid.append(temp['subframe id'])
        final_data.append(temp)

        bits = ''
        for i in range(0, 304):
            bits += 'p'
        #print(" end subfram 2-------------------------------")
    if ids == 3:
        temp['subframe id'] = 3
        temp["page id"] = "-"
        temp["C_ic"] = twos_complement(nav_bits[60:76]) * (2 ** -29)
        bits = (tows_binary_data(nav_bits[60:76], temp["C_ic"], 29,1)).join([bits[:60], bits[76:]])
        temp["Omega_0"] = twos_complement(nav_bits[76:84] + nav_bits[90:114]) * (2 ** (-31)) * pi
        tbits = (tows_binary_data(nav_bits[76:84] + nav_bits[90:114], temp["Omega_0"], 31, pi))
        bits = (tbits[0:8]).join([bits[:76], bits[84:]])
        bits = (tbits[8:]).join([bits[:90], bits[114:]])
        temp["C_is"] = twos_complement(nav_bits[120:136]) * (2 ** (-31)) * pi
        bits = (tows_binary_data(nav_bits[120:136], temp["C_is"], 31, pi)).join([bits[:120], bits[136:]])
        temp["I_0"] = twos_complement(nav_bits[136:144] + nav_bits[150:174]) * (2 ** (-31)) * pi
        tbits = (tows_binary_data(nav_bits[136:144] + nav_bits[150:174], temp["I_0"], 31, pi))
        bits = (tbits[0:8]).join([bits[:136], bits[144:]])
        bits = (tbits[8:]).join([bits[:150], bits[174:]])
        temp["C_rc"] = twos_complement(nav_bits[180:196]) * (2 ** (-5))
        bits = (tows_binary_data(nav_bits[180:196], temp["C_rc"], 5, 1)).join([bits[:180], bits[196:]])
        temp["Omega"] = twos_complement(nav_bits[196:204] + nav_bits[210:234]) * (2 ** (-31)) * pi
        tbits = (tows_binary_data(nav_bits[196:204]+ nav_bits[210:234], temp["Omega"], 31, pi))
        bits = (tbits[0:8]).join([bits[:196], bits[204:]])
        bits = (tbits[8:]).join([bits[:210], bits[234:]])
        temp["omegaDot"] = twos_complement(nav_bits[240:264]) * (2 ** -43) * pi
        bits = (tows_binary_data(nav_bits[240:264], temp["omegaDot"], 43, pi)).join([bits[:240], bits[264:]])
        temp["IODE_sf3"] = binary(nav_bits[270:278])
        bits = (binary_data(nav_bits[270:278], temp["IODE_sf3"], 0,1)).join([bits[:270], bits[278:]])
        temp["iDot"] = binary(nav_bits[278:292]) * (2 ** -43) * pi
        bits = (tows_binary_data(nav_bits[278:292], temp["iDot"], 43, pi)).join([bits[:278], bits[292:]])
        #bits += '0000'
        original_bits.append(nav_bits)
        blist.append(bits)
        sid.append(temp['subframe id'])
        final_data.append(temp)
        towc.append(temp['TOWC'])
        prn.append(temp["PRN"])
        bits = ''
        for i in range(0, 304):
            bits += 'p'
        #print(" end subfram 3-------------------------------")
    if ids == 4 and pid in sub4mapping.keys():
        # if sub4mapping.values()
        # print("in loop 4")
        temp['subframe id'] = 4
        temp["page id"] = sub4mapping[pid]
        
        temp["data id_1"] = binary(nav_bits[60:62]) #* (2 ** 12)
        bits = (binary_data(nav_bits[60:62], temp["data id_1"], 0, 1)).join([bits[:60], bits[62:]])
        
        temp["page id_1"] = binary(nav_bits[62:68]) #* (2 ** 12)
        bits = (binary_data(nav_bits[62:68], temp["page id_1"], 0, 1)).join([bits[:62], bits[68:]])
        
        temp["ALM4_e"] = binary(nav_bits[68:84]) * (2 ** (-21))
        bits = (tows_binary_data(nav_bits[68:84], temp["ALM4_e"], 21, 1)).join([bits[:68], bits[84:]])
        temp["ALM4_toa"] = binary(nav_bits[90:98]) #* (2 ** 12)
        bits = (binary_data(nav_bits[90:98], temp["ALM4_toa"], 0, 1)).join([bits[:90], bits[98:]])
        temp["ALM4_Delta_I"] = twos_complement(nav_bits[98:114]) * (2 ** (-19))
        bits = (tows_binary_data(nav_bits[98:114], temp["ALM4_Delta_I"], 19, 1)).join([bits[:98], bits[114:]])
        temp["ALM4_omegaDot"] = twos_complement(nav_bits[120:136]) * (2 ** (-38)) * pi
        bits = (tows_binary_data(nav_bits[120:136], temp["ALM4_omegaDot"], 38, pi)).join([bits[:120], bits[136:]])
        temp["ALM4_Sqaure Root A"] = binary((nav_bits[150:174])) * (2 ** (-11))
        bits = (tows_binary_data(nav_bits[150:174], temp["ALM4_Sqaure Root A"], 11, 1)).join([bits[:150], bits[174:]])
        # print(len((nav_bits[150:174])))
        temp["ALM4_Omega_0"] = twos_complement(nav_bits[180:204]) * (2 ** (-23)) * pi
        bits = (tows_binary_data(nav_bits[180:204], temp["ALM4_Omega_0"], 23, pi)).join([bits[:180], bits[204:]])
        temp["ALM4_Omega"] = twos_complement(nav_bits[210:234]) * (2 ** (-23)) * pi
        bits = (tows_binary_data(nav_bits[210:234], temp["ALM4_Omega"], 23, pi)).join([bits[:210], bits[234:]])
        temp["ALM4_M_0"] = twos_complement(nav_bits[240:264]) * (2 ** (-23)) * pi
        bits = (tows_binary_data(nav_bits[240:264], temp["ALM4_M_0"], 23, pi)).join([bits[:240], bits[264:]])
        temp["ALM4_a_f0"] = twos_complement((nav_bits[270:278] + nav_bits[289: 292])) * (2 ** (-20))
        tbits = (tows_binary_data(nav_bits[270:278] + nav_bits[289: 292], temp["ALM4_a_f0"], 20, 1))
        bits = (tbits[0:8]).join([bits[:270], bits[278:]])
        bits = (tbits[8:]).join([bits[:289], bits[292:]])
        temp["ALM4_a_f1"] = twos_complement(nav_bits[278:289]) * (2 ** -38)
        bits = (tows_binary_data(nav_bits[278:289], temp["ALM4_a_f1"], 38, 1)).join([bits[:278], bits[289:]])



        #New Code 
        temp["ALM4_SV HEALTH_4"] = binary(nav_bits[136:144]) # all zeros 
        bits = (binary_data(nav_bits[136:144], temp["ALM4_SV HEALTH_4"],0,1)).join([bits[:136], bits[144:]])
        towc.append(temp['TOWC'])
        prn.append(temp["PRN"])

        original_bits.append(nav_bits)
        blist.append(bits)
        sid.append(temp['subframe id'])
        final_data.append(temp)

        bits = ''
        for i in range(0, 304):
            bits += 'p'
        #print(" end subfram 4-------------------------------")

    if ids == 4 and pid in sub4mapping_page25.keys():
        # if sub4mapping.values()
        # print("in loop 4")
        temp['subframe id'] = 4
        #temp["page id"] = sub4mapping_page25[pid]
        temp["data id_1"] = binary(nav_bits[60:62])  # * (2 ** 12)
        bits = (binary_data(nav_bits[60:62], temp["data id_1"], 0, 1)).join([bits[:60], bits[62:]])

        temp["page id_1"] = binary(nav_bits[62:68])  # * (2 ** 12)
        bits = (binary_data(nav_bits[62:68], temp["page id_1"], 0, 1)).join([bits[:62], bits[68:]])
        temp["ALM4_A_SPOOF_SV1"] = binary((nav_bits[68:72]))
        bits = (binary_data(nav_bits[68:72], temp["ALM4_A_SPOOF_SV1"], 0, 1)).join([bits[:68], bits[72:]])
        temp["ALM4_A_SPOOF_SV2"] = binary((nav_bits[72:76]))
        bits = (binary_data(nav_bits[72:76], temp["ALM4_A_SPOOF_SV1"], 0, 1)).join([bits[:72], bits[76:]])
        temp["ALM4_A_SPOOF_SV3"] = binary((nav_bits[76:80]))
        bits = (binary_data(nav_bits[76:80], temp["ALM4_A_SPOOF_SV3"], 0, 1)).join([bits[:76], bits[80:]])
        temp["ALM4_A_SPOOF_SV4"] = binary((nav_bits[80:84]))
        bits = (binary_data(nav_bits[80:84], temp["ALM4_A_SPOOF_SV4"], 0, 1)).join([bits[:80], bits[84:]])
        temp["ALM4_A_SPOOF_SV5"] = binary((nav_bits[90:94]))
        bits = (binary_data(nav_bits[90:94], temp["ALM4_A_SPOOF_SV5"], 0, 1)).join([bits[:90], bits[94:]])
        temp["ALM4_A_SPOOF_SV6"] = binary((nav_bits[94:98]))
        bits = (binary_data(nav_bits[94:98], temp["ALM4_A_SPOOF_SV6"], 0, 1)).join([bits[:94], bits[98:]])
        temp["ALM4_A_SPOOF_SV7"] = binary((nav_bits[98:102]))
        bits = (binary_data(nav_bits[98:102], temp["ALM4_A_SPOOF_SV7"], 0, 1)).join([bits[:98], bits[102:]])
        temp["ALM4_A_SPOOF_SV8"] = binary((nav_bits[102:106]))
        bits = (binary_data(nav_bits[102:106], temp["ALM4_A_SPOOF_SV8"], 0, 1)).join([bits[:102], bits[106:]])
        temp["ALM4_A_SPOOF_SV9"] = binary((nav_bits[106:110]))
        bits = (binary_data(nav_bits[106:110], temp["ALM4_A_SPOOF_SV9"], 0, 1)).join([bits[:106], bits[110:]])
        temp["ALM4_A_SPOOF_SV10"] = binary((nav_bits[110:114]))
        bits = (binary_data(nav_bits[110:114], temp["ALM4_A_SPOOF_SV10"], 0, 1)).join([bits[:110], bits[114:]])
        temp["ALM4_A_SPOOF_SV11"] = binary((nav_bits[120:124]))
        bits = (binary_data(nav_bits[120:124], temp["ALM4_A_SPOOF_SV11"], 0, 1)).join([bits[:120], bits[124:]])
        temp["ALM4_A_SPOOF_SV12"] = binary((nav_bits[124:128]))
        bits = (binary_data(nav_bits[124:128], temp["ALM4_A_SPOOF_SV12"], 0, 1)).join([bits[:124], bits[128:]])
        temp["ALM4_A_SPOOF_SV13"] = binary((nav_bits[128:132]))
        bits = (binary_data(nav_bits[128:132], temp["ALM4_A_SPOOF_SV13"], 0, 1)).join([bits[:128], bits[132:]])
        temp["ALM4_A_SPOOF_SV14"] = binary((nav_bits[132:136]))
        bits = (binary_data(nav_bits[132:136], temp["ALM4_A_SPOOF_SV14"], 0, 1)).join([bits[:132], bits[136:]])
        temp["ALM4_A_SPOOF_SV15"] = binary((nav_bits[136:140]))
        bits = (binary_data(nav_bits[136:140], temp["ALM4_A_SPOOF_SV15"], 0, 1)).join([bits[:136], bits[140:]])
        temp["ALM4_A_SPOOF_SV16"] = binary((nav_bits[140:144]))
        bits = (binary_data(nav_bits[140:144], temp["ALM4_A_SPOOF_SV16"], 0, 1)).join([bits[:140], bits[144:]])
        temp["ALM4_A_SPOOF_SV17"] = binary((nav_bits[150:154]))
        bits = (binary_data(nav_bits[150:154], temp["ALM4_A_SPOOF_SV17"], 0, 1)).join([bits[:150], bits[154:]])
        temp["ALM4_A_SPOOF_SV18"] = binary((nav_bits[154:158]))
        bits = (binary_data(nav_bits[154:158], temp["ALM4_A_SPOOF_SV18"], 0, 1)).join([bits[:154], bits[158:]])
        temp["ALM4_A_SPOOF_SV19"] = binary((nav_bits[158:162]))
        bits = (binary_data(nav_bits[158:162], temp["ALM4_A_SPOOF_SV19"], 0, 1)).join([bits[:158], bits[162:]])
        temp["ALM4_A_SPOOF_SV20"] = binary((nav_bits[162:166]))
        bits = (binary_data(nav_bits[162:166], temp["ALM4_A_SPOOF_SV20"], 0, 1)).join([bits[:162], bits[166:]])
        temp["ALM4_A_SPOOF_SV21"] = binary((nav_bits[166:170]))
        bits = (binary_data(nav_bits[166:170], temp["ALM4_A_SPOOF_SV21"], 0, 1)).join([bits[:166], bits[170:]])
        temp["ALM4_A_SPOOF_SV22"] = binary((nav_bits[170:174]))
        bits = (binary_data(nav_bits[170:174], temp["ALM4_A_SPOOF_SV22"], 0, 1)).join([bits[:170], bits[174:]])
        temp["ALM4_A_SPOOF_SV23"] = binary((nav_bits[180:184]))
        bits = (binary_data(nav_bits[180:184], temp["ALM4_A_SPOOF_SV23"], 0, 1)).join([bits[:180], bits[184:]])
        temp["ALM4_A_SPOOF_SV24"] = binary((nav_bits[184:188]))
        bits = (binary_data(nav_bits[184:188], temp["ALM4_A_SPOOF_SV24"], 0, 1)).join([bits[:184], bits[188:]])
        temp["ALM4_A_SPOOF_SV25"] = binary((nav_bits[188:192]))
        bits = (binary_data(nav_bits[188:192], temp["ALM4_A_SPOOF_SV25"], 0, 1)).join([bits[:188], bits[192:]])
        temp["ALM4_A_SPOOF_SV26"] = binary((nav_bits[192:196]))
        bits = (binary_data(nav_bits[192:196], temp["ALM4_A_SPOOF_SV26"], 0, 1)).join([bits[:192], bits[196:]])
        temp["ALM4_A_SPOOF_SV27"] = binary((nav_bits[196:200]))
        bits = (binary_data(nav_bits[196:200], temp["ALM4_A_SPOOF_SV27"], 0, 1)).join([bits[:196], bits[200:]])
        temp["ALM4_A_SPOOF_SV28"] = binary((nav_bits[200:204]))
        bits = (binary_data(nav_bits[200:204], temp["ALM4_A_SPOOF_SV27"], 0, 1)).join([bits[:200], bits[204:]])
        temp["ALM4_A_SPOOF_SV29"] = binary((nav_bits[210:214]))
        bits = (binary_data(nav_bits[210:214], temp["ALM4_A_SPOOF_SV29"], 0, 1)).join([bits[:210], bits[214:]])
        temp["ALM4_A_SPOOF_SV30"] = binary((nav_bits[214:218]))
        bits = (binary_data(nav_bits[214:218], temp["ALM4_A_SPOOF_SV30"], 0, 1)).join([bits[:214], bits[218:]])
        temp["ALM4_A_SPOOF_SV31"] = binary((nav_bits[218:222]))
        bits = (binary_data(nav_bits[218:222], temp["ALM4_A_SPOOF_SV31"], 0, 1)).join([bits[:218], bits[222:]])
        temp["ALM4_A_SPOOF_SV32"] = binary((nav_bits[222:226]))
        bits = (binary_data(nav_bits[222:226], temp["ALM4_A_SPOOF_SV32"], 0, 1)).join([bits[:222], bits[226:]])
        temp["ALM4_sub4_Res1"] =binary(nav_bits[226:228])
        bits = (binary_data(nav_bits[226:228], temp["ALM4_sub4_Res1"], 0, 1)).join([bits[:226], bits[228:]])
        temp["ALM4_Health_SV25"] = binary((nav_bits[228:234]))
        bits = (binary_data(nav_bits[228:234], temp["ALM4_Health_SV25"], 0, 1)).join([bits[:228], bits[234:]])
        temp["ALM4_Health_SV26"] = binary((nav_bits[240:246]))
        bits = (binary_data(nav_bits[240:246], temp["ALM4_Health_SV26"], 0, 1)).join([bits[:240], bits[246:]])
        temp["ALM4_Health_SV27"] = binary((nav_bits[246:252]))
        bits = (binary_data(nav_bits[246:252], temp["ALM4_Health_SV27"], 0, 1)).join([bits[:246], bits[252:]])
        temp["ALM4_Health_SV28"] = binary((nav_bits[252:258]))
        bits = (binary_data(nav_bits[252:258], temp["ALM4_A_SPOOF_SV32"], 0, 1)).join([bits[:252], bits[258:]])
        temp["ALM4_Health_SV29"] = binary((nav_bits[258:264]))
        bits = (binary_data(nav_bits[258:264], temp["ALM4_Health_SV29"], 0, 1)).join([bits[:258], bits[264:]])
        temp["ALM4_Health_SV30"] = binary((nav_bits[270:276]))
        bits = (binary_data(nav_bits[270:276], temp["ALM4_Health_SV30"], 0, 1)).join([bits[:270], bits[276:]])
        temp["ALM4_Health_SV31"] = binary((nav_bits[276:282]))
        bits = (binary_data(nav_bits[276:282], temp["ALM4_Health_SV31"], 0, 1)).join([bits[:276], bits[282:]])
        temp["ALM4_Health_SV32"] = binary((nav_bits[282:288]))
        bits = (binary_data(nav_bits[282:288], temp["ALM4_Health_SV32"], 0, 1)).join([bits[:282], bits[288:]])
        temp["ALM4_sub4_Res2"] = binary(nav_bits[288:292])
        bits = (binary_data(nav_bits[288:292], temp["ALM4_sub4_Res2"], 0, 1)).join([bits[:288], bits[292:]])

       # print("temp:",temp)
        original_bits.append(nav_bits)
        blist.append(bits)
        sid.append(temp['subframe id'])
        final_data.append(temp)
        towc.append(temp['TOWC'])
        prn.append(temp["PRN"])
        bits = ''
        for i in range(0, 300):
            bits += 'p'
        #print(" end subfram 4 25-------------------------------")

    if (ids == 5) and pid in range(1, 25):
        # print("in loop 5")
        temp['subframe id'] = 5
        temp["page id"] = pid
        temp["data id_2"] = binary(nav_bits[60:62]) #* (2 ** 12)
        bits = (binary_data(nav_bits[60:62], temp["data id_2"], 0, 1)).join([bits[:60], bits[62:]])
        temp["page id_2"] = binary(nav_bits[62:68]) #* (2 ** 12)
        bits = (binary_data(nav_bits[62:68], temp["page id_2"], 0, 1)).join([bits[:62], bits[68:]])
        temp["ALM5_e"] = binary(nav_bits[68:84]) * (2 ** (-21))
        bits = (tows_binary_data(nav_bits[68:84], temp["ALM5_e"], 21, 1)).join([bits[:68], bits[84:]])
        temp["ALM5_toa"] = binary(nav_bits[90:98]) #* (2 ** 12)
        bits = (binary_data(nav_bits[90:98], temp["ALM5_toa"], 0, 1)).join([bits[:90], bits[98:]])
        temp["ALM5_Delta_I"] = twos_complement(nav_bits[98:114]) * (2 ** (-19))
        bits = (tows_binary_data(nav_bits[98:114], temp["ALM5_Delta_I"], 19, 1)).join([bits[:98], bits[114:]])
        temp["ALM5_omegaDot"] = twos_complement(nav_bits[120:136]) * (2 ** (-38)) * pi
        bits = (tows_binary_data(nav_bits[120:136], temp["ALM5_omegaDot"], 38, pi)).join([bits[:120], bits[136:]])
        temp["ALM5_Sqaure Root A"] = binary((nav_bits[150:174])) * (2 ** (-11))
        bits = (tows_binary_data(nav_bits[150:174], temp["ALM5_Sqaure Root A"], 11, 1)).join([bits[:150], bits[174:]])
        temp["ALM5_Omega_0"] = twos_complement(nav_bits[180:204]) * (2 ** (-23)) * pi
        bits = (tows_binary_data(nav_bits[180:204], temp["ALM5_Omega_0"], 23, pi)).join([bits[:180], bits[204:]])
        temp["ALM5_Omega"] = twos_complement(nav_bits[210:234]) * (2 ** (-23)) * pi
        bits = (tows_binary_data(nav_bits[210:234], temp["ALM5_Omega"], 23, pi)).join([bits[:210], bits[234:]])
        temp["ALM5_M_0"] = twos_complement(nav_bits[240:264]) * (2 ** (-23)) * pi
        bits = (tows_binary_data(nav_bits[240:264], temp["ALM5_M_0"], 23, pi)).join([bits[:240], bits[264:]])
        temp["ALM5_a_f0"] = twos_complement((nav_bits[270:278] + nav_bits[289: 292])) * (2 ** (-20))
        tbits = (tows_binary_data(nav_bits[270:278] + nav_bits[289: 292], temp["ALM5_a_f0"], 20, 1))
        bits = (tbits[0:8]).join([bits[:270], bits[278:]])
        bits = (tbits[8:]).join([bits[:289], bits[292:]])
        temp["ALM5_a_f1"] = twos_complement(nav_bits[278:289]) * (2 ** -38)
        bits = (tows_binary_data(nav_bits[278:289], temp["ALM5_a_f1"], 38, 1)).join([bits[:278], bits[289:]])
        #New Code 
        temp["ALM5_SV HEALTH_5"] = binary(nav_bits[136:144]) # all zeros 
        bits = (binary_data(nav_bits[136:144], temp["ALM5_SV HEALTH_5"],0,1)).join([bits[:136], bits[144:]])
        


        #bits+='0000'
        original_bits.append(nav_bits)
        blist.append(bits)
        sid.append(temp['subframe id'])
        final_data.append(temp)
        towc.append(temp['TOWC'])
        prn.append(temp["PRN"])

        bits = ''
        for i in range(0, 304):
            bits += 'p'
        #print(" end subfram 5-------------------------------")
    if ids == 5 and pid in sub5mapping.keys():
        temp['subframe id'] = 5
        temp["page id"] = sub5mapping[pid]
        temp['Data_id']=binary(nav_bits[60:62])
        bits = (tows_binary_data(nav_bits[60:62], temp["Data_id"], 0, 1)).join([bits[:60], bits[62:]])
        temp['SV(PAGE)_id'] = binary(nav_bits[62:68])
        bits = (tows_binary_data(nav_bits[62:68], temp["SV(PAGE)_id"], 0, 1)).join([bits[:62], bits[68:]])
        temp["ALM5P25_toa"] = binary(nav_bits[68:76])
        bits = (tows_binary_data(nav_bits[68:76], temp["ALM5P25_toa"], 0, 1)).join([bits[:68], bits[76:]])
        temp["ALM5_Week Number"] = binary(nav_bits[76:84]) + 1024
        bits = (tows_binary_data(nav_bits[76:84], temp["ALM5_Week Number"] - 1024, 0, 1)).join([bits[:76], bits[84:]])
        temp["ALM5_Health_SV1"] = binary((nav_bits[90:96]))
        bits = (tows_binary_data(nav_bits[90:96], temp["ALM5_Health_SV1"], 0, 1)).join([bits[:90], bits[96:]])
        temp["ALM5_Health_SV2"] = binary((nav_bits[96:102]))
        bits = (tows_binary_data(nav_bits[96:102], temp["ALM5_Health_SV2"], 0, 1)).join([bits[:96], bits[102:]])
        temp["ALM5_Health_SV3"] = binary((nav_bits[102:108]))
        bits = (tows_binary_data(nav_bits[102:108], temp["ALM5_Health_SV3"], 0, 1)).join([bits[:102], bits[108:]])
        temp["ALM5_Health_SV4"] = binary((nav_bits[108:114]))
        bits = (tows_binary_data(nav_bits[108:114], temp["ALM5_Health_SV4"], 0, 1)).join([bits[:108], bits[114:]])
        temp["ALM5_Health_SV5"] = binary((nav_bits[120:126]))
        bits = (tows_binary_data(nav_bits[120:126], temp["ALM5_Health_SV5"], 0, 1)).join([bits[:120], bits[126:]])
        temp["ALM5_Health_SV6"] = binary((nav_bits[126:132]))
        bits = (tows_binary_data(nav_bits[126:132], temp["ALM5_Health_SV6"], 0, 1)).join([bits[:126], bits[132:]])
        temp["ALM5_Health_SV7"] = binary((nav_bits[132:138]))
        bits = (tows_binary_data(nav_bits[132:138], temp["ALM5_Health_SV7"], 0, 1)).join([bits[:132], bits[138:]])
        temp["ALM5_Health_SV8"] = binary((nav_bits[138:144]))
        bits = (tows_binary_data(nav_bits[138:144], temp["ALM5_Health_SV8"], 0, 1)).join([bits[:138], bits[144:]])
        temp["ALM5_Health_SV9"] = binary((nav_bits[150:156]))
        bits = (tows_binary_data(nav_bits[150:156], temp["ALM5_Health_SV9"], 0, 1)).join([bits[:150], bits[156:]])
        temp["ALM5_Health_SV10"] = binary((nav_bits[156:162]))
        bits = (tows_binary_data(nav_bits[156:162], temp["ALM5_Health_SV10"], 0, 1)).join([bits[:156], bits[162:]])
        temp["ALM5_Health_SV11"] = binary((nav_bits[162:168]))
        bits = (tows_binary_data(nav_bits[162:168], temp["ALM5_Health_SV11"], 0, 1)).join([bits[:162], bits[168:]])
        temp["ALM5_Health_SV12"] = binary((nav_bits[168:174]))
        bits = (tows_binary_data(nav_bits[168:174], temp["ALM5_Health_SV12"], 0, 1)).join([bits[:168], bits[174:]])
        temp["ALM5_Health_SV13"] = binary((nav_bits[180:186]))
        bits = (tows_binary_data(nav_bits[180:186], temp["ALM5_Health_SV13"], 0, 1)).join([bits[:180], bits[186:]])
        temp["ALM5_Health_SV14"] = binary((nav_bits[186:192]))
        bits = (tows_binary_data(nav_bits[186:192], temp["ALM5_Health_SV14"], 0, 1)).join([bits[:186], bits[192:]])
        temp["ALM5_Health_SV15"] = binary((nav_bits[192:198]))
        bits = (tows_binary_data(nav_bits[192:198], temp["ALM5_Health_SV15"], 0, 1)).join([bits[:192], bits[198:]])
        temp["ALM5_Health_SV16"] = binary((nav_bits[198:204]))
        bits = (tows_binary_data(nav_bits[198:204], temp["ALM5_Health_SV16"], 0, 1)).join([bits[:198], bits[204:]])
        temp["ALM5_Health_SV17"] = binary((nav_bits[210:216]))
        bits = (tows_binary_data(nav_bits[210:216], temp["ALM5_Health_SV17"], 0, 1)).join([bits[:210], bits[216:]])
        temp["ALM5_Health_SV18"] = binary((nav_bits[216:222]))
        bits = (tows_binary_data(nav_bits[216:222], temp["ALM5_Health_SV18"], 0, 1)).join([bits[:216], bits[222:]])
        temp["ALM5_Health_SV19"] = binary((nav_bits[222:228]))
        bits = (tows_binary_data(nav_bits[222:228], temp["ALM5_Health_SV19"], 0, 1)).join([bits[:222], bits[228:]])
        temp["ALM5_Health_SV20"] = binary((nav_bits[228:234]))
        bits = (tows_binary_data(nav_bits[228:234], temp["ALM5_Health_SV20"], 0, 1)).join([bits[:228], bits[234:]])
        temp["ALM5_Health_SV21"] = binary((nav_bits[240:246]))
        bits = (tows_binary_data(nav_bits[240:246], temp["ALM5_Health_SV21"], 0, 1)).join([bits[:240], bits[246:]])
        temp["ALM5_Health_SV22"] = binary((nav_bits[246:252]))
        bits = (tows_binary_data(nav_bits[246:252], temp["ALM5_Health_SV22"], 0, 1)).join([bits[:246], bits[252:]])
        temp["ALM5_Health_SV23"] = binary((nav_bits[252:258]))
        bits = (tows_binary_data(nav_bits[252:258], temp["ALM5_Health_SV23"], 0, 1)).join([bits[:252], bits[258:]])
        temp["ALM5_Health_SV24"] = binary((nav_bits[258:264]))
        bits = (tows_binary_data(nav_bits[258:264], temp["ALM5_Health_SV24"], 0, 1)).join([bits[:258], bits[264:]])
        original_bits.append(nav_bits)
        blist.append(bits)
        sid.append(temp['subframe id'])
        final_data.append(temp)
        towc.append(temp['TOWC'])
        prn.append(temp["PRN"])
        bits = ''
        for i in range(0, 300):
            bits += 'p'
        #print(" end subfram 5 .25-------------------------------")

# create excel of actual bits and calculated bits


#print("list orig bits",original_bits)
#print("sid::",sid)
#col_list=['p0','p1', 'p2', 'p3', 'p4', 'p5', 'p6', 'p7', 'p8', 'p9', 'p10',"Subframe id","TOWC","PRN"]
df1=pd.DataFrame()
#split_blist=[]

#print(len(blist),len(sid),len(original_bits))

for i in range(0,len(sid)):
    n=30

    split_strings = [original_bits[i][index: index + n] for index in range(0, len(original_bits[i]), n)]
    split_strings.append(sid[i])
    split_strings.append(towc[i])
    split_strings.append(prn[i])
    split_blist = [blist[i][index: index + n] for index in range(0, len(blist[i]), n)]
    split_blist.append(sid[i])
    split_blist.append(towc[i])
    split_blist.append(prn[i])
    df1 = df1.append([split_strings])
    df1 = df1.append([split_blist])

    #df1.loc[i,['Subframe id']]=sid[i]


df1.to_excel('test.xlsx', index = False)


csv_columns = []
s = final_data[0]
for rec in s:
    csv_columns.append(rec)

#print(csv_columns)
'''csv_file = "try1.csv"
try:
    with open(csv_file, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, delimiter=',', lineterminator='\n', fieldnames=csv_columns)
        writer.writeheader()
        for data in final_data:
            writer.writerow(data)
except IOError:
    print("I/O error")'''

    # 1 010 1101100 010 0 010 111100 010 101  304



print("Done !")
