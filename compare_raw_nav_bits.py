# import tensorflow as tf

import numpy as np
import sys

np.set_printoptions(threshold=sys.maxsize, precision=17, suppress=True)
import copy

from SV_functions import read_alamanc_file, generate_satellite_parameters_for_30_seconds, subframe_creation, \
    generate_nav_bits_for_30_seconds
from SV_functions import convert, generate_SV_Codes, constdict
import SV_functions.gps_constants as constants
import generate_test_scenario
import generate_test_scenario_nav_bits





play_parameters = {
    "Valid PRN": [ 22,7,4,3,8,16,27,11,9 ],
    "satellite_parameters": {},
    "Nav_message": np.zeros((32, 6530)),
    "location_llh": [18.42361111, 73.75750000, 548],
    "samplerate": 5000000,
    "CA_Code": generate_SV_Codes.GPS_Code_20(),
    "satellite_parameter_iter_step" : 0,
}


def initialise_play_parameters():
    play_parameters["Nav_message"] = generate_test_scenario.get_raw_nav_bits(filename="RAW_NAV_BITS/live_signal_bits.csv",
                                     prnlist=play_parameters["Valid PRN"])

    play_parameters["Nav_message_new"] = generate_test_scenario_nav_bits.get_raw_nav_bits()


initialise_play_parameters()
print(np.shape(play_parameters["Nav_message"]))
print(np.shape(play_parameters["Nav_message_new"]))

valid_prn = 26
actual_nav_bits = play_parameters["Nav_message"][valid_prn][0:1500]
generated_nav_bits = play_parameters["Nav_message_new"][valid_prn][0:1500]

actual_nav_bits[actual_nav_bits == -1] = 0
generated_nav_bits[generated_nav_bits == -1 ] = 0


# Segregate in words
actual_words = [actual_nav_bits[i:i + 30] for i in range(0, len(actual_nav_bits), 30)]
generated_words = [generated_nav_bits[i:i + 30] for i in range(0, len(generated_nav_bits), 30)]

# convert words to string
for i in range(len(actual_words)):
    actual_words[i] = ''.join(str(x) for x in actual_words[i])
    generated_words[i] = ''.join(str(x) for x in generated_words[i])

print(actual_words)
print(generated_words)

