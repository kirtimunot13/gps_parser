# import tensorflow as tf

import numpy as np
import time
import pandas as pd
import sys

np.set_printoptions(threshold=sys.maxsize, precision=17, suppress=True)
# np.set_printoptions()
import copy

from SV_functions import read_alamanc_file, generate_satellite_parameters_for_30_seconds, subframe_creation, \
    generate_nav_bits_for_30_seconds
from SV_functions import convert, generate_SV_Codes, constdict
import SV_functions.gps_constants as constants
import generate_test_scenario
import generate_test_scenario_nav_bits
import generate_test_scenario_sat_param
from datetime import datetime
play_parameters = {
    "Valid PRN": [2, 9, 6, 19, 25, 5, 29, 13],
    "satellite_parameters": {},
    "location_llh": [40.0096856, 116.478479, 100],
    "location_xyz": [],
    "samplerate": 5000000,
    "CA_Code": generate_SV_Codes.GPS_Code_20(),
    "satellite_parameter_iter_step" : 0,
    "scenario_time": {
        "seconds": 6,
        "Week": 1006,
    },
    "satellite_parameters_refresh_rate": int(1 / constants.refresh_rate),
    "page_id": 1,
    "Nav_message": np.zeros((32, 1530)),
    "ephemeris_from_file": read_alamanc_file.get_alamanac(filename="UPLOADED_ALAMANAC/December_2018.txt"),
    "almanac_struct45": {},
    "subframes_123_from_ephemeris": {},
    "ephemeris": {},
}


def initial_navbit_array_30(play_parameters):
    all_raw_nav_bits = np.zeros((32, 1530), dtype=np.int)

    x, y, z = convert.gps_to_ecef(play_parameters["location_llh"][0], play_parameters["location_llh"][1],
                                  play_parameters["location_llh"][2])
    play_parameters["location_xyz"] = [x, y, z]
    play_parameters["subframes_123_from_ephemeris"] = subframe_creation.ephemeris_to_subframe(
        play_parameters["ephemeris_from_file"])
    play_parameters["almanac_struct45"] = subframe_creation.ephemeris_to_almanac_subframe(
        play_parameters["ephemeris_from_file"])

    # update ephemeris mk and toe and toc for satellite parameter calculcation
    play_parameters["ephemeris"] = generate_nav_bits_for_30_seconds.add_time_parameters_to_ephemeris(
        copy.deepcopy(play_parameters["ephemeris_from_file"]), play_parameters["scenario_time"])

    # get navigation_bits for 30 seconds , initial last previous word as all zeros
    navigation_message = generate_nav_bits_for_30_seconds.generate_nav_for_30_sec(
        play_parameters["subframes_123_from_ephemeris"],
        play_parameters["almanac_struct45"],
        play_parameters["page_id"],
        play_parameters["scenario_time"]["seconds"],
        play_parameters["scenario_time"]["Week"],
        play_parameters["ephemeris"])

    navbit = generate_nav_bits_for_30_seconds.get_1500_nav_sting(navigation_message, play_parameters["Valid PRN"])

    for Valid_PRN in play_parameters["Valid PRN"]:
        raw_bits_array = np.fromstring(navbit[Valid_PRN], 'u1') - ord('0')
        all_raw_nav_bits[Valid_PRN - 1, 30:1530] = raw_bits_array
        all_raw_nav_bits[Valid_PRN - 1, 0:30] = play_parameters["Nav_message"][Valid_PRN - 1, 0:30]
    all_raw_nav_bits[all_raw_nav_bits == 0] = (-1)
    print("NAV BITS START TIME - ", play_parameters["scenario_time"]["seconds"])

    return all_raw_nav_bits

def next_navbit_array_30(play_parameters):
    all_raw_nav_bits = np.zeros((32, 1530), dtype=np.int)

    # increment scenario time
    play_parameters["scenario_time"]["seconds"] = play_parameters["scenario_time"]["seconds"] + 30

    # increment page id
    play_parameters["page_id"] = play_parameters["page_id"] + 1
    if play_parameters["page_id"] == 26:
        play_parameters["page_id"] = 1

    # update ephemeris mk and toe and toc for satellite parameter calculation
    play_parameters["ephemeris"] = generate_nav_bits_for_30_seconds.add_time_parameters_to_ephemeris(
        copy.deepcopy(play_parameters["ephemeris_from_file"]), play_parameters["scenario_time"])

    # get navigation_bits for 30 seconds , initial last previous word as all zeros
    navigation_message = generate_nav_bits_for_30_seconds.generate_nav_for_30_sec(
        play_parameters["subframes_123_from_ephemeris"],
        play_parameters["almanac_struct45"],
        play_parameters["page_id"],
        play_parameters["scenario_time"]["seconds"],
        play_parameters["scenario_time"]["Week"],
        play_parameters["ephemeris"])

    navbit = generate_nav_bits_for_30_seconds.get_1500_nav_sting(navigation_message, play_parameters["Valid PRN"])

    for Valid_PRN in play_parameters["Valid PRN"]:
        raw_bits_array = np.fromstring(navbit[Valid_PRN], 'u1') - ord('0')
        all_raw_nav_bits[Valid_PRN - 1, 30:1530] = raw_bits_array
        all_raw_nav_bits[Valid_PRN - 1, 0:30] = play_parameters["Nav_message"][Valid_PRN - 1, 0:30]
    all_raw_nav_bits[all_raw_nav_bits == 0] = (-1)
    print("NAV BITS CHNAGED TIME - ", play_parameters["scenario_time"]["seconds"])

    # CHANGE ACTIVE NAV BIT BY 1500
    for Valid_PRN in play_parameters["Valid PRN"]:
        play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][Valid_PRN]["Nav_Bit"] = play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][Valid_PRN]["Nav_Bit"] - 1500


    return all_raw_nav_bits


play_parameters["Nav_message"] = initial_navbit_array_30(play_parameters)
play_parameters["satellite_parameters"] = generate_test_scenario_sat_param.get_satellite_parameters()



Multiplier = (np.arange( play_parameters["samplerate"]* 1 ) + 1).astype(int)

print("--------- Initial Params --------")
print("Number of PRNs > 1500 = ", 0)
for sv in play_parameters["Valid PRN"]:
    print("SV - ", sv, " Bit - ", play_parameters["satellite_parameters"][0][sv]["Nav_Bit"],
          "Code Phase - ", play_parameters["satellite_parameters"][0][sv]["Code_phase"],
          "Carr Phase - ", play_parameters["satellite_parameters"][0][sv]["Carr_phase"],
          "Pseudorange - ", play_parameters["satellite_parameters"][0][sv]["Pseudorange"],
          "Doppler - ", play_parameters["satellite_parameters"][0][sv]["Dopplers"],
          "Step - ", play_parameters["satellite_parameter_iter_step"],
          )
print("-----------------")

start_time = datetime.now()

with open("complete_signal.bin", 'ab+') as b:
    for iter in range(0,200):
        buffer = np.zeros((int(play_parameters["samplerate"] * 1), 2))
        for sv in play_parameters["Valid PRN"]:
            CodePhase_Array = (Multiplier *
                               play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv]['Code_Phase_Step']) + \
                              play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv]["Code_phase"]

            Navbits = (np.floor((CodePhase_Array / (constants.CA_SEQ_LEN * 20)))).astype(int)
            CodePhase_Array = CodePhase_Array - Navbits * (constants.CA_SEQ_LEN * 20)
            # print(CodePhase_Array[0:20460])

            Navbits = Navbits + play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv]["Nav_Bit"]
            CarrPhase_Array = (Multiplier * play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv]['Carr_Phase_Step']) + \
                              play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv]["Carr_phase"]

            # replace the current codephase and navbit in play status
            play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"] + 1][sv][
                "Nav_Bit"] = Navbits[-1]
            #play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"] + 1][sv][
            #    "Code_phase"] = CodePhase_Array[-1]
            play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"] + 1][sv][
                "Carr_phase"] = CarrPhase_Array[-1]

            # Take actual values from CA table and Nav bits
            Actual_codes = np.take(play_parameters["CA_Code"][sv-1], CodePhase_Array.astype(int))
            Actual_nav_bits = np.take(play_parameters["Nav_message"][sv-1], Navbits.astype(int))

            # Calculation for I and Q Components
            Real_Signal = Actual_nav_bits * Actual_codes * 700 # * play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv]["gain"]
            I = Real_Signal * np.cos(2 * np.pi * CarrPhase_Array)
            Q = Real_Signal * np.sin(2 * np.pi * CarrPhase_Array)

            # print(np.shape(I))
            buffer[:, 0] += I
            buffer[:, 1] += Q

        print("Time into Run - ", iter)
        for sv in play_parameters["Valid PRN"]:
            print("SV - ", sv, " Bit - ",
                  play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv][
                      "Nav_Bit"],
                  "Code Phase - ",
                  play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv][
                      "Code_phase"],
                  "Carr Phase - ",
                  play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv][
                      "Carr_phase"],
                  "Pseudorange - ",
                  play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv][
                      "Pseudorange"],
                  "Doppler - ",
                  play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv][
                      "Dopplers"],
                  "Step - ", play_parameters["satellite_parameter_iter_step"],
                  )
        print("-----------------")

        play_parameters["satellite_parameter_iter_step"] = play_parameters["satellite_parameter_iter_step"] + 1
        if play_parameters["satellite_parameter_iter_step"] % 30 == 0:
            # change satellite parameters
            play_parameters["Nav_message"] = next_navbit_array_30(play_parameters)



        b.write(np.array(buffer, dtype=np.int16))
stop_time = datetime.now()
total_time = (stop_time - start_time).total_seconds()

print("TIME FOR RUN - ",total_time)

