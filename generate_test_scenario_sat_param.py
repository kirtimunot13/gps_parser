# import tensorflow as tf

import numpy as np
import time
import pandas as pd
import sys

np.set_printoptions(threshold=sys.maxsize, precision=17, suppress=True)
# np.set_printoptions()
import copy

from SV_functions import read_alamanc_file, subframe_creation, \
    generate_nav_bits_for_30_seconds
from SV_functions import convert, generate_SV_Codes, constdict

import SV_functions.gps_constants as constants
from SV_functions.calculate_range_sv import calculate_range_sv
import SV_functions.constdict
import math
import copy

from SV_functions.calculate_position_sv import calculate_position_sv


def initialise_play_parameters(play_parameters):
    x, y, z = convert.gps_to_ecef(play_parameters["location_llh"][0], play_parameters["location_llh"][1],
                                  play_parameters["location_llh"][2])
    play_parameters["location_xyz"] = [x, y, z]
    play_parameters["subframes_123_from_ephemeris"] = subframe_creation.ephemeris_to_subframe(
        play_parameters["ephemeris_from_file"])
    play_parameters["almanac_struct45"] = subframe_creation.ephemeris_to_almanac_subframe(
        play_parameters["ephemeris_from_file"])

    # update ephemeris mk and toe and toc for satellite parameter calculcation
    play_parameters["ephemeris"] = generate_nav_bits_for_30_seconds.add_time_parameters_to_ephemeris(
        copy.deepcopy(play_parameters["ephemeris_from_file"]), play_parameters["scenario_time"])

    # get navigation_bits for 30 seconds , initial last previous word as all zeros
    navigation_message = generate_nav_bits_for_30_seconds.generate_nav_for_30_sec(
        play_parameters["subframes_123_from_ephemeris"],
        play_parameters["almanac_struct45"],
        play_parameters["page_id"],
        play_parameters["scenario_time"]["seconds"],
        play_parameters["scenario_time"]["Week"],
        play_parameters["ephemeris"])

    to_return = generate_nav_bits_for_30_seconds.get_1500_nav_sting(navigation_message, play_parameters["Valid PRN"])
    return to_return


def generate_next_nav_bits(play_parameters):
    # increment scenario time
    play_parameters["scenario_time"]["seconds"] = play_parameters["scenario_time"]["seconds"] + 30

    # increment page id
    play_parameters["page_id"] = play_parameters["page_id"] + 1
    if play_parameters["page_id"] == 26:
        play_parameters["page_id"] = 1

    # update ephemeris mk and toe and toc for satellite parameter calculation
    play_parameters["ephemeris"] = generate_nav_bits_for_30_seconds.add_time_parameters_to_ephemeris(
        copy.deepcopy(play_parameters["ephemeris_from_file"]), play_parameters["scenario_time"])

    # get navigation_bits for 30 seconds , initial last previous word as all zeros
    navigation_message = generate_nav_bits_for_30_seconds.generate_nav_for_30_sec(
        play_parameters["subframes_123_from_ephemeris"],
        play_parameters["almanac_struct45"],
        play_parameters["page_id"],
        play_parameters["scenario_time"]["seconds"],
        play_parameters["scenario_time"]["Week"],
        play_parameters["ephemeris"])

    to_return = generate_nav_bits_for_30_seconds.get_1500_nav_sting(navigation_message, play_parameters["Valid PRN"])
    return to_return


def generate_satellite_parameters_for_30_seconds(Eph, scenario_time, xyz, samplerate, prnlist):
    refresh_rate = 1
    time_steps = int(30 / refresh_rate)
    T = 1 / samplerate
    scenario_time = scenario_time - 1
    all_satellite_parameters = []

    for time_step in range(0, time_steps):
        current_parameters = {}
        current_time = scenario_time + ((time_step + 1) * refresh_rate)
        for sv in prnlist:
            current_parameters[sv] = copy.deepcopy(SV_functions.constdict.satellite_parameter_template)
            old_time = current_time - refresh_rate
            old_rho = calculate_range_sv(Eph, sv, old_time, xyz)
            current_rho = calculate_range_sv(Eph, sv, current_time, xyz)
            rhorate = (current_rho[sv]["range"] - old_rho[sv]["range"]) / refresh_rate
            doppler = -rhorate / constants.LAMBDA_L1
            codephase = constants.CODE_FREQ + doppler * constants.CARR_TO_CODE

            # Initial code phase and data bit counters.
            time_for_transmission = current_rho[sv]["range"] / constants.SPEED_OF_LIGHT
            number_of_ca_code_bits = time_for_transmission / constants.TIME_FOR_1_CA_BIT

            # print(number_of_ca_code_bits)
            Codephase = (constants.CA_SEQ_LEN * 20) - (number_of_ca_code_bits % (constants.CA_SEQ_LEN * 20))
            Bit_Number = 30 - math.ceil(time_for_transmission / constants.TIME_FOR_1_NAV_BIT)

            current_parameters[sv]["SV"] = sv
            current_parameters[sv]["TOWC"] = current_time
            current_parameters[sv]["Pseudorange"] = current_rho[sv]["range"]
            current_parameters[sv]["coderate"] = codephase
            current_parameters[sv]["Dopplers"] = doppler
            current_parameters[sv]["Code_phase"] = Codephase
            current_parameters[sv]["Nav_Bit"] = Bit_Number
            current_parameters[sv]["Carr_phase"] = 0
            current_parameters[sv]["Code_Phase_Step"] = current_parameters[sv]["coderate"] * T
            current_parameters[sv]["Carr_Phase_Step"] = current_parameters[sv]["Dopplers"] * T
            current_parameters[sv]["gain"] = int(2400 / len(prnlist))

            pos, vel, clk = calculate_position_sv(Eph, sv, current_time)

            current_parameters[sv]["check_data"]["pos"] = pos
            current_parameters[sv]["check_data"]["vel"] = vel
            current_parameters[sv]["check_data"]["clk"] = clk

        all_satellite_parameters.append(current_parameters)
    return all_satellite_parameters

#22,16,4,3
def get_raw_nav_bits(play_parameters=None):
    if play_parameters is None:
        play_parameters = {
            "Valid PRN": [7, 8, 27, 9],
            "scenario_time": {
                "seconds": 209970,
                "Week": 89,
            },
            "page_id": 25,
            "previous_word": np.zeros((32, 30)),
            "satellite_parameters": {},
            "previous_satellite_parameters": {},
            "Nav_message": np.zeros((32, 1530)),
            "ephemeris_from_file": read_alamanc_file.get_alamanac(filename="UPLOADED_ALAMANAC/live_signal_almanac.txt"),
            "almanac_struct45": {},
            "subframes_123_from_ephemeris": {},
            "ephemeris": {},
            "location_llh": [18.42361111, 73.75750000, 550],
            "location_xyz": [],
            "samplerate": 5000000,
            "satellite_parameters_refresh_rate": int(1 / constants.refresh_rate),
            "satellite_parameter_iter_step": 0,
            "CA_Code": generate_SV_Codes.GPS_Code_20()
        }
    all_raw_nav_bits = np.zeros((32, 6030), dtype=np.int)
    navbit = initialise_play_parameters(play_parameters)
    print("TIME - ", play_parameters["scenario_time"]["seconds"], " -  ", len(navbit[play_parameters["Valid PRN"][0]]))
    for iter in range(3):
        next_navbit = generate_next_nav_bits(play_parameters)
        for Valid_PRN in play_parameters["Valid PRN"]:
            navbit[Valid_PRN] = navbit[Valid_PRN] + next_navbit[Valid_PRN]
        print("TIME - ", play_parameters["scenario_time"]["seconds"], " -  ", len(navbit[Valid_PRN]))

    for Valid_PRN in play_parameters["Valid PRN"]:
        raw_bits_array = np.fromstring(navbit[Valid_PRN], 'u1') - ord('0')
        all_raw_nav_bits[Valid_PRN - 1, 30:6030] = raw_bits_array

    all_raw_nav_bits[all_raw_nav_bits == 0] = (-1)
    return all_raw_nav_bits


def get_satellite_parameters(play_parameters=None):
    if play_parameters is None:
        play_parameters = {
            "Valid PRN": [7, 8, 27, 9,22,16,4,3],
            "scenario_time": {
                "seconds": 209970,
                "Week": 89,
            },
            "page_id": 25,
            "previous_word": np.zeros((32, 30)),
            "satellite_parameters": {},
            "previous_satellite_parameters": {},
            "Nav_message": np.zeros((32, 1530)),
            "ephemeris_from_file": read_alamanc_file.get_alamanac(filename="UPLOADED_ALAMANAC/live_signal_almanac.txt"),
            "almanac_struct45": {},
            "subframes_123_from_ephemeris": {},
            "ephemeris": {},
            "location_llh": [18.42361111, 73.75750000, 550],
            "location_xyz": [],
            "samplerate": 5000000,
            "satellite_parameters_refresh_rate": int(1 / constants.refresh_rate),
            "satellite_parameter_iter_step": 0,
            "CA_Code": generate_SV_Codes.GPS_Code_20()
        }
    navbit = initialise_play_parameters(play_parameters)
    all_parameters = []
    new_parameters = generate_satellite_parameters_for_30_seconds(play_parameters["ephemeris"],
                                                                  play_parameters["scenario_time"]["seconds"],
                                                                  play_parameters["location_xyz"],
                                                                  play_parameters["samplerate"],
                                                                  play_parameters["Valid PRN"])
    all_parameters.extend(new_parameters)
    print("TIME - ", play_parameters["scenario_time"]["seconds"], " -  PARAMS -", len(all_parameters))

    for iter in range(10):
        next_navbit = generate_next_nav_bits(play_parameters)
        new_parameters = generate_satellite_parameters_for_30_seconds(play_parameters["ephemeris"],
                                                                      play_parameters["scenario_time"]["seconds"],
                                                                      play_parameters["location_xyz"],
                                                                      play_parameters["samplerate"],
                                                                      play_parameters["Valid PRN"])
        all_parameters.extend(new_parameters)
        print("TIME - ", play_parameters["scenario_time"]["seconds"], " - PARAMS -", len(all_parameters))

    return all_parameters


if __name__ == "__main__":
    nav_bits = get_raw_nav_bits()
    sat_params = get_satellite_parameters()
    print(sat_params[0][2])
