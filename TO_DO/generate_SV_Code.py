import pandas as pd
import SV_functions
from collections import deque
import numpy as np

def generate_SV_Code(filename):
    SV_data=pd.read_excel(filename,header=None)
    svnp=(SV_data.to_numpy())#.transpose()
    print(len(svnp[0]))
    return svnp

def C_Code_ref(prn):
    CA_SEQ_LEN = 1023
    N_DWRD_SBF = 10
    delay=[5,   6,   7,   8,  17,  18, 139, 140, 141, 251,
		252, 254, 255, 256, 257, 258, 469, 470, 471, 472,
		473, 474, 509, 512, 513, 514, 515, 516, 859, 860,
		861, 862]
    g1=[]
    g2=[]
    r1=[]
    r2=[]
    ca=[]
    if (prn < 1 or prn > 32):
        return "invalid PRN";

    for i in range(N_DWRD_SBF):
        r1.append(-1)
        r2.append(-1)

    for i in range(0,(CA_SEQ_LEN)):
        g1.insert(i,r1[9])
        g2.insert(i,r2[9])
        c1 = r1[2] * r1[9]
        #print("c1",c1)

        c2 = r2[1] * r2[2] * r2[5] * r2[7] * r2[8] * r2[9]
        #print("c2", c2)
        for j in reversed(range(1,10)):
            r1[j] = r1[j - 1]
            r2[j] = r2[j - 1]
        r1[0] = c1
        r2[0] = c2
        #print(r1)
        #print(r2)
    j=CA_SEQ_LEN - delay[prn-1]   #1023 - delay[2]
    for i in range(0,CA_SEQ_LEN):
        ca.append(int((1 - g1[i] * g2[j % CA_SEQ_LEN]) / 2))
        j+=1

    for i in range(0,CA_SEQ_LEN):
        if ca[i]==0:
            ca[i]=-1
        else:
            pass
    return ca

ca=C_Code_ref(7)
print("C",ca)

svdata=generate_SV_Code("..\SV_functions\GPS_ca_table.xlsx")
print(svdata)

for i in range(0, 32):
    c = np.array_equal(svdata[i], ca)
    if c == 1:
        print("Same")



'''def getCode(num=0, zero=False, samplesPerChip=1, prn=0):

    Returns a list of bits that form the Gold Code PRN of the designated satellite

    zero flag determines whether 0 or -1 is returned
    
    g1tap = [2, 9]
    g2tap = [1, 2, 5, 7, 8, 9]
    g1 = deque(1 for i in range(10))
    g2 = deque(1 for i in range(10))

    g = []

    for i in range(num):
        val = (g1[9] + g2[0] + g2[1]) % 2
        g.append(val)

        # Shift g1
        g1[9] = sum([g1[i] for i in g1tap]) % 2
        g1.rotate()

        # Shift g2
        g2[9] = sum([g2[i] for i in g2tap]) % 2
        g2.rotate()

    if (zero == False):
        # format GC to have -1 in place of 0
        for n, i in enumerate(g):
            if i == 0:
                g[n] = -1

    if (samplesPerChip > 1):
        # Repeat each chip to match our ADC sample frequency
        gsamp = np.repeat(g, samplesPerChip)
        return gsamp
    return g

ca=getCode(num=1024,zero=False, samplesPerChip=1, prn=30)
print("New",ca)'''