from collections import deque
import numpy as np
import pandas as pd

'''
GPS Gold Code generator. Initialized with the feedback taps for one satellite.
'''

# Feedback taps as defined in GPS spec
g1tap = [2, 9]
g2tap = [1, 2, 5, 7, 8, 9]

sats = [(1, 5), (2, 6), (3, 7), (4, 8), (0, 8), (1, 9), (0, 7), (1, 8), (2, 9), (1, 2),
        (2, 3), (4, 5), (5, 6), (6, 7), (7, 8), (8, 9), (0, 3), (1, 4), (2, 5), (3, 6),
        (4, 7), (5, 8), (0, 2), (3, 5), (4, 6), (5, 7), (6, 8), (7, 9), (0, 5), (1, 6),
        (2, 7), (3, 8), (4, 9), (3, 9), (0, 6), (1, 7), (3, 9)]


def getCode(num, zero=False, samplesPerChip=1, prn=0):
    '''
    Returns a list of bits that form the Gold Code PRN of the designated satellite

    zero flag determines whether 0 or -1 is returned
    '''
    g1 = deque(1 for i in range(10))
    g2 = deque(1 for i in range(10))

    g = []

    for i in range(num):
        val = (g1[9] + g2[prn[0]] + g2[prn[1]]) % 2
        g.append(val)

        # Shift g1
        g1[9] = sum([g1[i] for i in g1tap]) % 2
        g1.rotate()

        # Shift g2
        g2[9] = sum([g2[i] for i in g2tap]) % 2
        g2.rotate()

    if (zero == False):
        # format GC to have -1 in place of 0
        for n, i in enumerate(g):
            if i == 0:
                g[n] = -1

    if (samplesPerChip > 1):
        # Repeat each chip to match our ADC sample frequency
        gsamp = np.repeat(g, samplesPerChip)
        return gsamp
    return g


def getTrackingCode(sat):
    '''
    Returns a code ready to be used by Tracking.py
    '''
    code = np.array(getCode(1023, prn=sats[sat - 1]))
    print(sats)
    # Need to add extra code chips to the ends so that it can 'slide'
    # back and forth during tracking.
    # code = np.append(code, code[0])
    # code = np.insert(code, 0, code[len(code) - 2])
    return code


def getAcquisitionCode(sat, spc):
    '''
    Returns a code ready to be used by Acquisition.py
    '''
    return getCode(1023, samplesPerChip=spc, prn=sats[sat - 1])

def generate_SV_Code(filename):
    SV_data=pd.read_excel(filename,header=None)
    svnp=(SV_data.to_numpy())#.transpose()
    #print(len(svnp[0]))
    return svnp

if __name__ == "__main__":
    codes = getTrackingCode(32)
    print(len(codes))
    print(codes)
    svcode=generate_SV_Code("..\SV_functions\GPS_ca_table.xlsx")
    for i in range(0,32):
        c=np.array_equal(svcode[i],codes)
        if c==1:
            print("Same")







