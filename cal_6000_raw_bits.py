import copy
import pandas as pd
import numpy as np
from SV_functions.read_alamanc_file import read_alm_param
import SV_functions.gps_constants as constants
import math
from SV_functions.parity_code import get_subframe_parity,flip_words_with_d30_as_1


def cal_6000_raw_bits(filename, prnlist):
    df = pd.read_csv(filename)
    df = df.set_index(df['TOWC (corresponding to start of the sub-frame) (s)'])
    tempr = {}
    tempr["Bits"] = ""
    temp = {}
    l = []
    for prn in prnlist:
        is_2 = df['PRN'] == prn
        df_is_2data = df[is_2].to_dict()
        towc = list(df_is_2data['TOWC (corresponding to start of the sub-frame) (s)'].keys())
        # print(towc[0:20])
        sbits = ""
        for j in range(0, 20, 5):
            fbits = ""
            for s in range(5):
                tempr["Bits"] = ""
                bits = ""
                for i in range(1, 38):
                    # print("[j+s]::",j+s)#,"towc[j+s]::",towc[j+s])
                    tempr["Bits"] += format(df_is_2data['Raw Navbits – ' + str(i)][towc[j + s]], '08b')
                bits = "10001011" + tempr["Bits"]
                bits = bits[0:300]
                bits = flip_words_with_d30_as_1(bits)
                fbits += bits[0:300]
            sbits += fbits
        temp[df_is_2data['PRN'][towc[j]]] = sbits
        l.append(temp)
        # print("*************\n",l)
    # print(l[0])
    # return l[0]
    # convert into numpy array
    raw_nav_bits = l[0]
    # print(raw_nav_bits)
    all_raw_nav_bits = np.zeros((32, 6030), dtype=np.int)

    for Valid_PRN in prnlist:
        raw_bits = raw_nav_bits[Valid_PRN]
        raw_bits_array = np.fromstring(raw_bits, 'u1') - ord('0')
        all_raw_nav_bits[Valid_PRN - 1, 30:6030] = raw_bits_array

    all_raw_nav_bits[all_raw_nav_bits == 0] = (-1)
    return all_raw_nav_bits


def filter_satellite_parameters(filename, prnlist, samplerate):
    sat_param = read_alm_param(filename=filename)
    new_satellite_params = []
    for i in range(len(sat_param)):
        current_satellite_params = {}
        for Valid_PRN in prnlist:
            current_satellite_params[Valid_PRN] = sat_param[i][Valid_PRN]

            # add certain satellite parameters
            current_satellite_params[Valid_PRN]["coderate"] = constants.CODE_FREQ + current_satellite_params[Valid_PRN][
                "Dopplers"] * constants.CARR_TO_CODE

            # Initial code phase and data bit counters.
            time_for_transmission = current_satellite_params[Valid_PRN]["Pseudorange"] / constants.SPEED_OF_LIGHT
            number_of_ca_code_bits = time_for_transmission / constants.TIME_FOR_1_CA_BIT

            # print(number_of_ca_code_bits)
            current_satellite_params[Valid_PRN]["Code_phase"] = (constants.CA_SEQ_LEN * 20) - (
                        number_of_ca_code_bits % (constants.CA_SEQ_LEN * 20))
            current_satellite_params[Valid_PRN]["Nav_Bit"] = 30 - math.ceil(
                time_for_transmission / constants.TIME_FOR_1_NAV_BIT)

            T = 1 / samplerate

            current_satellite_params[Valid_PRN]["Code_Phase_Step"] = current_satellite_params[Valid_PRN]["coderate"] * T
            current_satellite_params[Valid_PRN]["Carr_Phase_Step"] = current_satellite_params[Valid_PRN]["Dopplers"] * T
            current_satellite_params[Valid_PRN]["gain"] = int(2400 / len(prnlist))

        new_satellite_params.append(current_satellite_params)
    return new_satellite_params


def test_satellite_params(sat_param):
    keycheck = sat_param[0].keys()
    print(keycheck)
    status = True
    for i in range(len(sat_param)):
        if sat_param[i].keys() == keycheck:
            pass
        else:
            print(sat_param[i].keys())
            status = False
    return status


if __name__ == "__main__":
    raw_nav_bits = cal_6000_raw_bits(filename="RAW_NAV_BITS/test_navigation_bits.csv",
                                     prnlist=[2, 9, 6, 19, 12, 25, 5, 29, 13])
    print(np.shape(raw_nav_bits))

    # START FROM HERE
    sat_param = filter_satellite_parameters(filename="RAW_NAV_BITS/test_satellite_parameters.csv",
                                            prnlist=[2, 9, 6, 19, 12, 25, 5, 29, 13], samplerate=5000000)
    print("Got new satellite params. Test Status - ", test_satellite_params(sat_param))

    print(sat_param[0][2])
