# import tensorflow as tf

import numpy as np
import time
import pandas as pd
import sys

np.set_printoptions(threshold=sys.maxsize, precision=17, suppress=True)
import copy

from SV_functions import read_alamanc_file, generate_satellite_parameters_for_30_seconds, subframe_creation, \
    generate_nav_bits_for_30_seconds
from SV_functions import convert, generate_SV_Codes, constdict
import SV_functions.gps_constants as constants
from Basic_functions import binary

#23:30

play_parameters = {

    "Valid PRN": [2,9,6,19,17,12,25,23,5,29,13],
    "time_parameters": {
        "seconds": 6,
        "Week": 1006,
    },
    "page_id": 1,
    "previous_word": np.zeros((32, 30)),
    "satellite_parameters": {},
    "previous_satellite_parameters": {},
    "Nav_message": np.zeros((32, 1530)),
    "ephemeris_from_file": read_alamanc_file.get_alamanac(filename="UPLOADED_ALAMANAC/December_2018.txt"),
    "almanac_struct45": {},
    "subframes_123_from_ephemeris": {},
    "ephemeris": {},
    "location_llh": [40.0096856, 116.478479, 100],
    "location_xyz": [],
    "samplerate": 5000000,
    "CA_Code": generate_SV_Codes.GPS_Code_20()
}

x, y, z = convert.gps_to_ecef( 40.0096856, 116.478479, 100)
play_parameters["location_xyz"] = [x, y, z]
play_parameters["subframes_123_from_ephemeris"] = subframe_creation.ephemeris_to_subframe(
    play_parameters["ephemeris_from_file"])
play_parameters["almanac_struct45"] = subframe_creation.ephemeris_to_almanac_subframe(
    play_parameters["ephemeris_from_file"])


def renew_play_parameters(initial=False):
    # increment page id
    play_parameters["page_id"] = play_parameters["page_id"] + 1
    if play_parameters["page_id"] == 26:
        play_parameters["page_id"] = 1

    # increment towc by 30
    play_parameters["time_parameters"]["seconds"] = play_parameters["time_parameters"]["seconds"] + 30
    if play_parameters["time_parameters"]["seconds"] > constants.SECONDS_IN_WEEK:
        play_parameters["time_parameters"]["seconds"] = play_parameters["time_parameters"][
                                                            "seconds"] - constants.SECONDS_IN_WEEK
        play_parameters["time_parameters"]["Week"] = play_parameters["time_parameters"]["Week"] + 1

    play_parameters["ephemeris"] = read_alamanc_file.add_time_parameters_to_ephemeris(
        copy.deepcopy(play_parameters["ephemeris_from_file"]), play_parameters["time_parameters"])
    # set previous word as last 30 bits of nav message
    play_parameters["previous_word"] = play_parameters["Nav_message"][:, 1500:1530]

    # get navigation_bits for 30 seconds , initial last previous word as all zeros
    navigation_message = generate_nav_bits_for_30_seconds.generate_nav_for_30_sec(
        play_parameters["subframes_123_from_ephemeris"],
        play_parameters["almanac_struct45"],
        play_parameters["page_id"],
        play_parameters["time_parameters"]["seconds"],
        play_parameters["time_parameters"]["Week"],
        play_parameters["ephemeris"])
    # print(navigation_message[22])
    play_parameters["Nav_message"] = generate_nav_bits_for_30_seconds.get_1500_nav_bit_array(navigation_message,
                                                                                             play_parameters[
                                                                                                 "previous_word"])

    if initial == False:
        play_parameters["previous_satellite_parameters"] = copy.deepcopy(play_parameters["satellite_parameters"])
    # get satellite parameters for 30 seconds
    play_parameters[
        "satellite_parameters"] = generate_satellite_parameters_for_30_seconds.generate_satellite_parameters_for_30_seconds(
        play_parameters["ephemeris"],
        play_parameters["time_parameters"]["seconds"],
        play_parameters["location_xyz"],
        play_parameters["samplerate"])
    if initial == True:
        play_parameters["previous_satellite_parameters"] = copy.deepcopy(play_parameters["satellite_parameters"])
    # keep code phase and carr phase as last known
    for sv in play_parameters["Valid PRN"]:
        play_parameters["satellite_parameters"][sv]["Code_phase"] = \
        play_parameters["previous_satellite_parameters"][sv]["Code_phase"]
        play_parameters["satellite_parameters"][sv]["Carr_phase"] = \
        play_parameters["previous_satellite_parameters"][sv]["Carr_phase"]

    # print(play_parameters["satellite_parameters"][1])
    # print(play_parameters["satellite_parameters"][1])
    # get valid PRNs as per Azimuth and elevation
    # TO DO


new_ephemeris = read_alamanc_file.get_almanac_from_raw_navbits_new(filename="RAW_NAV_BITS/December_new.csv")
print(new_ephemeris[9])

renew_play_parameters(initial=True)
print(play_parameters["satellite_parameters"][9])
#print(play_parameters["ephemeris"][2])