import copy
import pandas as pd
from SV_functions import read_alamanc_file, generate_satellite_parameters_for_30_seconds, subframe_creation, \
    generate_nav_bits_for_30_seconds
from SV_functions import convert, generate_SV_Codes, constdict
import SV_functions.gps_constants as constants

#read_alamanc_file.read_alm_param(filename="RAW_NAV_BITS/SATB_L1.csv")

#read_alamanc_file.get_almanac_from_raw_navbits_compare_new(filename="RAW_NAV_BITS/RNBB_L1.csv")
def cal_6000_raw_bits(filename):
    df=pd.read_csv(filename)
    df=df.set_index(df['TOWC (corresponding to start of the sub-frame) (s)'])
    #print("1==============",df[is_2].tail())
    #print("2==============",df_is_2data)
    tempr={}
    tempr["Bits"]=""
    temp={}
    l=[]
    prnlist=[2,9,6,19,12,25,23,5,29,13]
    for prn in prnlist:
        is_2 = df['PRN'] == prn
        df_is_2data = df[is_2].to_dict()
        towc = list(df_is_2data['TOWC (corresponding to start of the sub-frame) (s)'].keys())
        #print(towc[0:20])
        sbits = ""
        for j in range(0,20,5):
            fbits=""
            for s in range(5):
                tempr["Bits"]=""
                bits=""
                for i in range(1, 38):
                    #print("[j+s]::",j+s)#,"towc[j+s]::",towc[j+s])
                    tempr["Bits"]+=format(df_is_2data['Raw Navbits – ' + str(i)][towc[j+s]], '08b')
                bits="10001011"+tempr["Bits"]
                #print("---",bits)
                fbits+=bits[0:300]
            sbits+=fbits
        temp[df_is_2data['PRN'][towc[j]]]=sbits
        l.append(temp)
        #print("*************\n",l)
    print(l[0])
    return l[0]



cal_6000_raw_bits(filename="RAW_NAV_BITS/RNBB_L1.csv")