def ones_complement(binary_string):
    temp = ""
    for i in range(len(binary_string)):
        if binary_string[i] == "0":
            temp += "1"
        elif binary_string[i] == "1":
            temp += "0"
        else:
            pass
    return temp
