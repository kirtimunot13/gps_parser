def twos_complement(binary_string):
    temp = int(binary_string, 2)
    if binary_string[0] == "1":
        temp = temp - 2 ** len(binary_string)
    return temp

