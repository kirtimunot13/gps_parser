def tows_binary_data(length_nav_bits, value, fact, pi):
    n=length_nav_bits
    test = int(value * (2**(fact) / pi))
    twos_binary_string = '{0:{fill}{width}b}'.format((test + 2 ** n) % 2 ** n, fill='0', width=n)
    return twos_binary_string