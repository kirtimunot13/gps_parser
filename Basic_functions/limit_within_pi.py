import math
import SV_functions.gps_constants as constants

def limit_within_pi(Mk) :
    multiplier = int(Mk / math.pi)
    print(multiplier)
    if multiplier % 2 != 0:
        Mk = Mk - (multiplier) * math.pi + math.pi
    else:
        Mk = Mk - (multiplier) * math.pi
    return Mk