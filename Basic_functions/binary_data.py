def binary_data(len_nav_bits,value,fact,pi):
    n=len_nav_bits
    test = int(value/(2**(fact))*pi)
    binary_string = '{0:{fill}{width}b}'.format(test, fill='0', width=n)
    #print("expo b", binary_string)
    return binary_string