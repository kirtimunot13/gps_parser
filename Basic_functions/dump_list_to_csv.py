import csv
def dump_list_to_csv(filename="Output.csv",dlist=[]):
    try:
        with open(filename, 'w') as csvfile:
            writer = csv.writer(csvfile, delimiter=',', lineterminator='\n')
            for i in range(len(dlist)):
                writer.writerow(dlist[i])

        #print("Done Writing to Excel.")
        return True
        # writer.close()
    except IOError:
        return False