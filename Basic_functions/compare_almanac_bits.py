def compare_almanac_bits(logs=[])  :
    csv_list = []
    n =30
    valid_PRNs = []
    for PRN in range(1,10):
        # CHECK IF ALL LOGS HAVE THE SUBFRAME 1 RECORD. IGNORE IF NOT.
        # IT IS ASSUMED THAT IF DATA IN SUBFRAME 1 data is also in subframes 2,3 .
        valid_PRN_flag = 1
        for log in range(len(logs)):
            if logs[log][PRN][1] == "":
                valid_PRN_flag = 0
        if valid_PRN_flag == 1:
            valid_PRNs.append(PRN)
            if PRN == 3 :
                for subframe_id in range(1,3):
                    for log in range(len(logs)):
                        split_string = [logs[log][PRN][subframe_id][k:k + n] for k in range(0, len(logs[log][PRN][subframe_id]), n)]
                        to_append = [PRN , subframe_id ]
                        to_append.extend(split_string)
                        csv_list.append(to_append)
    return valid_PRNs, csv_list
