from SV_functions import gps_constants
pow_dict={43:gps_constants.POW2_M43,31:gps_constants.POW2_M31}
#,4:gps_constants.POW2_M4,55:gps_constants.POW2_M55,5:gps_constants.POW2_M5,0:0}
def tows_binary_data_new(length_nav_bits, value, fact, pi):
    n=length_nav_bits
    test = int(value * (pow_dict.get(fact) / pi))
    twos_binary_string = '{0:{fill}{width}b}'.format((test + 2 ** n) % 2 ** n, fill='0', width=n)
    return twos_binary_string
