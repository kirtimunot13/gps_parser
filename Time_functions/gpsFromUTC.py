def gpsFromUTC(year, month, day, hour, min, sec, leapSecs=14):
    """converts UTC to: gpsWeek, secsOfWeek, gpsDay, secsOfDay 95 96
    a good reference is: http://www.oc.nps.navy.mil/~jclynch/timsys.html 97 98
    This is based on the following facts (see reference above): 99 100
    GPS time is basically measured in (atomic) seconds since 101
    January 6, 1980, 00:00:00.0 (the GPS Epoch) 102 103
    The GPS week starts on Saturday midnight (Sunday morning), and runs 104
    for 604800 seconds. 105 106
    Currently, GPS time is 13 seconds ahead of UTC (see above reference). 107
    While GPS SVs transmit this difference and the date when another leap 108
    second takes effect, the use of leap seconds cannot be predicted. This 109
    routine is precise until the next leap second is introduced and has to be 110
    updated after that. 111 112
    SOW = Seconds of Week 113
    SOD = Seconds of Day 114 115 N
    ote: Python represents time in integer seconds, fractions are lost!!! 116
    """
    import datetime
    import time, math

    secsInWeek = 604800
    secsInDay = 86400
    gpsEpoch = (1980, 1, 6, 0, 0, 0)  # (year, month, day, hh, mm, ss)

    secFract = sec % 1
    epochTuple = gpsEpoch + (-1, -1, 0)
    t0 = time.mktime(epochTuple)
    t = time.mktime((year, month, day, hour, min, int(sec), -1, -1, 0))
    # Note: time.mktime strictly works in localtime and to yield UTC, it should be 122
    # corrected with time.timezone 123
    # However, since we use the difference, this correction is unnecessary. 124
    # Warning: trouble if daylight savings flag is set to -1 or 1 !!! 125
    t = t + leapSecs
    tdiff = t - t0
    gpsSOW = (tdiff % secsInWeek) + secFract
    gpsWeek = int(math.floor(tdiff / secsInWeek))
    gpsDay = int(math.floor(gpsSOW / secsInDay))
    gpsSOD = (gpsSOW % secsInDay)
    return (gpsWeek, gpsSOW, gpsDay, gpsSOD)
