import time
def UTCFromGps(gpsWeek, SOW, leapSecs=14):
    """converts gps week and seconds to UTC 136 137
    see comments of inverse function! 138 139
    SOW = seconds of week 140
    gpsWeek is the full number (not modulo 1024) 141
    """
    secsInWeek = 604800
    secsInDay = 86400
    gpsEpoch = (1980, 1, 6, 0, 0, 0)  # (year, month, day, hh, mm, ss)
    secFract = SOW % 1
    epochTuple = gpsEpoch + (-1, -1, 0)
    t0 = time.mktime(epochTuple) - time.timezone #mktime is localtime, correct for UTC 145
    tdiff = (gpsWeek * secsInWeek) + SOW - leapSecs
    t = t0 + tdiff
    (year, month, day, hh, mm, ss, dayOfWeek, julianDay, daylightsaving) = time.gmtime(t)
    #use gmtime since localtime does not allow to switch off daylighsavings correction!!! 149
    return (year, month, day, hh, mm, ss + secFract)