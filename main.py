# import tensorflow as tf

import numpy as np
import time
import pandas as pd
import sys

np.set_printoptions(threshold=sys.maxsize,precision=17,suppress=True)
#np.set_printoptions()
import copy

from SV_functions import read_alamanc_file, generate_satellite_parameters_for_30_seconds, subframe_creation, \
    generate_nav_bits_for_30_seconds
from SV_functions import convert, generate_SV_Codes, constdict
import SV_functions.gps_constants as constants
from Basic_functions import binary

# 1,2,3,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]
play_parameters = {
    "Valid PRN": [21  ,25 ,24 , 20 , 14 ,12,10,15],
    "time_parameters": {
        "seconds" : 603138,
        "Week" :1006,
    },
    "page_id": 1,
    "previous_word": np.zeros((32, 30)),
    "satellite_parameters": {},
    "previous_satellite_parameters" : {},
    "Nav_message": np.zeros((32, 1530)),
    "ephemeris_from_file" :read_alamanc_file.get_alamanac(filename="UPLOADED_ALAMANAC/March1_2019_yuma.txt"),
    "almanac_struct45" : {},
    "subframes_123_from_ephemeris" : {},
    "ephemeris" : {},
    "location_llh" : [18.516726, 73.856255, 500],
    "location_xyz" : [],
    "samplerate" : 5000000,
    "CA_Code" : generate_SV_Codes.GPS_Code_20()
}

x, y, z = convert.gps_to_ecef(18.516726, 73.856255, 500)
play_parameters["location_xyz"] = [x, y, z]
play_parameters["subframes_123_from_ephemeris"] = subframe_creation.ephemeris_to_subframe(
    play_parameters["ephemeris_from_file"])
play_parameters["almanac_struct45"] = subframe_creation.ephemeris_to_almanac_subframe(play_parameters["ephemeris_from_file"])


# Ephemeris checking
#print(play_parameters["ephemeris_from_file"][22])
new_ephemeris = read_alamanc_file.get_almanac_from_raw_navbits_new(filename="RAW_NAV_BITS/Data2.csv")
#print(new_ephemeris[22])

# Subframe 1 ,2, 3 check
#print(play_parameters["subframes_123_from_ephemeris"][10])
#new_subframe = subframe_creation.ephemeris_to_subframe( new_ephemeris )
new_subframe = subframe_creation.raw_nav_bits_to_subframe_structure_new(filename="RAW_NAV_BITS/Data2.csv")
#print(new_subframe[22])

# af0 and omega0 are not matching
almanac_list_4, almanac_list_5,raw_almanac_parameters = subframe_creation.raw_nav_bits_to_almanac_subframe(filename="RAW_NAV_BITS/Data2.csv")

print(play_parameters["almanac_struct45"][20])

print(almanac_list_4)
#print(raw_almanac_parameters)
#print(play_parameters["almanac_struct45"])
csv_dump= []

temp = []
temp.append("Sr. No.")
temp.append("Source")
temp.append("pid")
temp.append("actual_pid")
temp.append("Comment")
temp.append("Bits")
temp.append("pid")
temp.append("actual_pid")
temp.append("Comment")
temp.append("Bits")
csv_dump.append(temp)
#print(almanac_list_4)
#print(raw_almanac_parameters)
for i in range(1,len(almanac_list_4)):
    temp = []
    temp.append(i)
    #print(i)
    temp.append("From RAW NAV Bits")
    #print("xxxxxx")
    #print(almanac_list_4[i])
    temp.append(almanac_list_4[i]["pid"])
    temp.append(almanac_list_4[i]["actual_pid"])
    temp.append(almanac_list_4[i]["Comment"])
    temp.append(almanac_list_4[i]["Bits"])
    temp.append(almanac_list_5[i]["pid"])
    temp.append(almanac_list_5[i]["actual_pid"])
    temp.append(almanac_list_5[i]["Comment"])
    temp.append(almanac_list_5[i]["Bits"])
    #print(temp)
    csv_dump.append(temp)
    temp = []
    temp.append(i)
    # print(i)
    temp.append("From Alamanac")
    pid= binary(play_parameters["almanac_struct45"][i+19][4][62:68])
    temp.append(pid)
    if pid in constants.sub4mapping_SV_map.keys():
        actual_pid = constants.sub4mapping_actual_SV_map[pid]
        Comment = "Almanac for SV - " + str(actual_pid)
    else :
        actual_pid = "NA"
        Comment = "NA"
    temp.append(actual_pid)
    temp.append(Comment)
    temp.append(play_parameters["almanac_struct45"][i+19][4])
    pid = binary(play_parameters["almanac_struct45"][i+19][5][62:68])
    if pid in constants.sub4mapping_25_map.keys():
        actual_pid = constants.sub4mapping_25_map[pid]
        Comment = "Almanac for SV - " + str(actual_pid)
    else :
        actual_pid = "NA"
        Comment = "NA"
    temp.append(pid)
    temp.append(actual_pid)
    temp.append(Comment)
    temp.append(play_parameters["almanac_struct45"][i+19][5])
    csv_dump.append(temp)

from Basic_functions import dump_list_to_csv
flag  = dump_list_to_csv(filename="Output1.csv",dlist=csv_dump)
print(flag)

print(almanac_list_5[1]["Bits"])
print(play_parameters["almanac_struct45"][20][5])
#print(raw_almanac_parameters[1])

# almanac_list_4, almanac_list_5, ephemeris_list, ephemeris4_25, ephemeris5_25 = read_alamanc_file.return_almanac_sequence(filename="RAW_NAV_BITS/Data2.csv")
#print(ephemeris_list[0][19])


def renew_play_parameters(initial = False):

    # increment page id
    play_parameters["page_id"] = play_parameters["page_id"] + 1
    if play_parameters["page_id"] == 26:
        play_parameters["page_id"] = 1

    # increment towc by 30
    play_parameters["time_parameters"]["seconds"] = play_parameters["time_parameters"]["seconds"] + 30
    if play_parameters["time_parameters"]["seconds"] > constants.SECONDS_IN_WEEK :
        play_parameters["time_parameters"]["seconds"] = play_parameters["time_parameters"]["seconds"] - constants.SECONDS_IN_WEEK
        play_parameters["time_parameters"]["Week"] = play_parameters["time_parameters"]["Week"] + 1

    play_parameters["ephemeris"] = read_alamanc_file.add_time_parameters_to_ephemeris( copy.deepcopy( play_parameters["ephemeris_from_file"] ), play_parameters["time_parameters"])
    # set previous word as last 30 bits of nav message
    play_parameters["previous_word"] = play_parameters["Nav_message"][:, 1500:1530]

    # get navigation_bits for 30 seconds , initial last previous word as all zeros
    navigation_message = generate_nav_bits_for_30_seconds.generate_nav_for_30_sec(play_parameters [ "subframes_123_from_ephemeris"],
                                                                                  play_parameters[ "almanac_struct45"],
                                                                                  play_parameters["page_id"],
                                                                                  play_parameters ["time_parameters"]["seconds"],
                                                                                  play_parameters ["time_parameters"]["Week"],
                                                                                  play_parameters ["ephemeris"] )
    #print(navigation_message[22])
    play_parameters["Nav_message"] = generate_nav_bits_for_30_seconds.get_1500_nav_bit_array(navigation_message,
                                                                                             play_parameters[
                                                                                                 "previous_word"])

    if initial == False :
        play_parameters["previous_satellite_parameters"] = copy.deepcopy(play_parameters["satellite_parameters"])
    # get satellite parameters for 30 seconds
    play_parameters["satellite_parameters"] = generate_satellite_parameters_for_30_seconds.generate_satellite_parameters_for_30_seconds(
        play_parameters["ephemeris"],
        play_parameters["time_parameters"]["seconds"],
        play_parameters["location_xyz"],
        play_parameters["samplerate"])
    if initial == True :
        play_parameters["previous_satellite_parameters"] = copy.deepcopy(play_parameters["satellite_parameters"])
    # keep code phase and carr phase as last known
    for sv in play_parameters["Valid PRN"]:
        play_parameters["satellite_parameters"][sv]["Code_phase"] = play_parameters["previous_satellite_parameters"][sv]["Code_phase"]
        play_parameters["satellite_parameters"][sv]["Carr_phase"] = play_parameters["previous_satellite_parameters"][sv]["Carr_phase"]

    #print(play_parameters["satellite_parameters"][1])
    # print(play_parameters["satellite_parameters"][1])
    # get valid PRNs as per Azimuth and elevation
    # TO DO
renew_play_parameters(initial=True)

#print(play_parameters["ephemeris_from_file"][22])
#print(play_parameters["ephemeris"][22])
#print(new_ephemeris[22])


#print(new_ephemeris[22])




Multiplier = np.arange(play_parameters["samplerate"]) + 1

'''
with open("New_GPS_9.bin", 'ab+') as b:
    for iter in range(150 ) :
        buffer = np.zeros((play_parameters["samplerate"], 2))
        for sv in play_parameters["Valid PRN"]:
            CodePhase_Array = (Multiplier * play_parameters["satellite_parameters"][sv]['Code_Phase_Step']) + play_parameters["satellite_parameters"][sv]["Code_phase"]
            #print(CodePhase_Array[0:20460])
            Navbits = np.floor((CodePhase_Array / (constants.CA_SEQ_LEN * 20)))

            CodePhase_Array = CodePhase_Array - Navbits * (constants.CA_SEQ_LEN * 20)

            #print(CodePhase_Array[0:20460])
            Navbits = Navbits +  play_parameters["satellite_parameters"][sv]["Nav_Bit"]
            CarrPhase_Array = (Multiplier * play_parameters["satellite_parameters"][sv]['Carr_Phase_Step']) + play_parameters["satellite_parameters"][sv]["Carr_phase"]


            #print(Navbits[0:10]," ---- " ,  Navbits[len(Navbits)-10:len(Navbits)])
            #print(CodePhase_Array[0:10], " ---- ", CodePhase_Array[len(CodePhase_Array) - 10:len(CodePhase_Array)])
            #print("----------------------------------")
            

            # Take actual values from CA table and Nav bits
            Actual_codes = np.take(play_parameters["CA_Code"][sv], CodePhase_Array.astype(int))
            Actual_nav_bits = np.take(play_parameters["Nav_message"][sv], Navbits.astype(int))

            #print(Actual_nav_bits[0:20460])

            # replace the current codephase and navbit in play status
            play_parameters["satellite_parameters"][sv]["Nav_Bit"] = Navbits[-1]
            play_parameters["satellite_parameters"][sv]["Code_phase"] = CodePhase_Array[-1]
            play_parameters["satellite_parameters"][sv]["Carr_phase"] = CarrPhase_Array[-1]

            # Calculation for I and Q Components
            Real_Signal = Actual_nav_bits * Actual_codes * play_parameters["satellite_parameters"][sv]["gain"]
            I = Real_Signal * np.cos(2 * np.pi * CarrPhase_Array)
            Q = Real_Signal * np.sin(2 * np.pi * CarrPhase_Array)

            #print(np.shape(I))
            buffer[:, 0] += I
            buffer[:, 1] += Q

            # if Nav bit is greater than 1500 , get new satellite params and nav message
            if play_parameters["satellite_parameters"][sv]["Nav_Bit"] >= 1500:
                renew_play_parameters()
                print("TOWC Changed - " , play_parameters["time_parameters"]["seconds"] )

        print("Done Iter - ", iter, " Bits - ", play_parameters["satellite_parameters"][sv]["Nav_Bit"])
        b.write(np.array(buffer, dtype=np.int16))
'''
print("Done")

'''data=pd.read_excel('Mumbai3.xlsx')

ranges=data[['ranges_2','ranges_3','ranges_4','ranges_5','ranges_6','ranges_7']].values
dps=data[['Dopplers_2','Dopplers_3','Dopplers_4','Dopplers_5','Dopplers_6','Dopplers_7']].values

code_doppler=data[['Codephases_2','Codephases_3','Codephases_4','Codephases_5','Codephases_6','Codephases_7']].values
dopplers=(dps)#*(-1)
dopplers=np.asarray(dopplers,dtype=np.float64)
Fcode = ((1176.45 * 1000000 + dopplers) * 1.023*1000000) / (1176.45*1000000)

print(Fcode[1:3,:])
phase=np.array([0.61888,	0.36309,	0.62175,	0.61107,	0.77627,	0.29571])*np.pi;
phase=phase.reshape(1,6)
elev=data[['Elevation_2','Elevation_3','Elevation_4','Elevation_5','Elevation_6','Elevation_7']].values
azi=data[['Azimuth_2','Azimuth_3','Azimuth_4','Azimuth_5','Azimuth_6','Azimuth_7']].values
real_d=data[['SlantRange_2','SlantRange_3','SlantRange_4','SlantRange_5','SlantRange_6','SlantRange_7']].values

pr = ranges #+table2array(iono_error)+table2array(tropo_error);
time_1b=1/(1.023*1000000)
c = 299792458
time_pr= (pr/c)
bits=(time_pr) /time_1b
bits=((bits -(6*1023*20)))
print(bits[0,:])

bits=1023*20-bits


print(np.shape(bits))
print(np.shape(phase))
print(np.shape(real_d))


Navmessage=np.zeros((36008,6))

for sv_data in range(2,8):
    df = pd.read_csv(str(sv_data)+'_good.csv', header=None)
    sid=(df.values)
    Navmessage[7:36007,sv_data-2]=np.transpose(sid[:,0])
    #print(Navmessage[1:11,sv_data-2])
print(np.shape(Navmessage))
Navmessage[Navmessage == 0] = (-1)




sampling=5
fsamp=sampling*1e6
T=1/fsamp
print(fsamp)
print(T)

Incr_fcode=Fcode*T
Incr_phase=dopplers*T


All_codephase=np.zeros((7200,6),dtype=np.float64)
All_carrphase=np.zeros((7200,6),dtype=np.float64)
All_Navbits=np.zeros((7200,6),dtype=np.float64)

#print(np.mod(19000,20460))
All_codephase[0,:]=bits[0,:]
All_Navbits[0,:]=np.zeros((1,6))
All_carrphase[0,:]=phase[0,:]

for i in range(1,7200):
    All_codephase[i, :] = np.mod( (Incr_fcode[i-1,:] * (int(fsamp / 10)) + All_codephase[i-1,:] ), 20460)
    All_Navbits[i, :] = np.floor_divide((Incr_fcode[i-1, :] * (int(fsamp / 10)) + All_codephase[i - 1, :]), 20460) +All_Navbits[i-1,:]
    All_carrphase[i, :] = (Incr_phase[i-1, :] * (int(fsamp / 10)) + All_carrphase[i-1,:])

print(bits[0,:])
print(All_codephase[0:3,:])
#print(All_Navbits[0:3,:])
#print(All_carrphase[0:3,:])


ant_pat_db = np.array([
    0.00 , 0.00,  0.22 , 0.44 , 0.67 , 1.11 , 1.56 , 2.00  ,2.44  ,2.89 ,3.56 ,4.22 ,
    4.89  ,5.56  ,6.22 , 6.89 , 7.56 , 8.22 , 8.89  ,9.78 ,10.67 ,11.56 ,12.44 ,13.33 ,
    14.44 ,15.56 ,16.67 ,17.78 ,18.89 ,20.00 , 21.33 ,22.67 ,24.00 ,25.56 ,27.33 ,29.33 ,31.56])

ant_pat = (10.0 **(-ant_pat_db/20.0)).reshape(37)

path_loss = 30400000.0 / real_d
ibs = np.asarray(((90.0 - elev) / 5.0),dtype=int)
ant_gain = ant_pat[ibs]
gain = path_loss * ant_gain
gain=gain*700

#print(gain)



start = time.time()
codephase=np.zeros((1,6))
carrphase=np.zeros((1,6))
codephase[0,:]=bits[0,:]
tempphase=np.zeros((1,6))
tempphase[0,:]=bits[0,:]
carrphase[0,:]=phase[0,:]
curr_nav_bit=np.zeros((1,6))
print("-----------------------------------------")

sid = np.zeros((int(fsamp / 10), 2), dtype=np.float64)
Navbits = np.zeros((1, int(fsamp / 10)),dtype=int)
Arrange = np.arange(1, int(fsamp / 10)+1)
Arrange=np.reshape(Arrange, (1, int(fsamp / 10)))
Arrange=np.asarray(Arrange,dtype=np.float64)

result=np.zeros((int(fsamp/10),2),np.float64)
#result=np.reshape(6,int(fsamp/10),2)



arrange = np.arange(1,int(fsamp/10)+1,dtype=np.float64)
arrange.reshape(1,int(fsamp/10))
#print(arrange.shape)
Arrange_tf = tf.placeholder( shape=arrange.shape,dtype=tf.float64)
Incr_phase_tf= tf.placeholder(tf.float64, shape=(6,))
Incr_fcode_tf= tf.placeholder(tf.float64, shape=(6,))

All_carrphase_tf=tf.placeholder(tf.float64, shape=(6,))
All_codephase_tf=tf.placeholder(tf.float64, shape=(6,))
All_Navbits_tf=tf.placeholder(tf.float64, shape=(6,))
SV_tf = tf.placeholder( shape=(20460,6),dtype=tf.int64)
Navbits_tf= tf.placeholder( shape=(int(fsamp/10),),dtype=tf.float64)
Multiplier_tf=tf.placeholder( shape=(int(fsamp/10),),dtype=tf.float64)
gain_tf=tf.placeholder( shape=(6,),dtype=tf.float64)
Navmessage_tf=tf.placeholder( shape=(36008,6),dtype=tf.float64)
I_tf=tf.placeholder( shape=(500000,),dtype=tf.float64)
Q_tf=tf.placeholder( shape=(500000,),dtype=tf.float64)

def contf(Arrange_tf,Incr_fcode_tf,All_codephase_tf,All_Navbits_tf,SV_tf,Navmessage_tf,Incr_phase_tf,All_carrphase_tf,gain_tf,I_tf,Q_tf):
    for sv in range(0,6):
        Multiplier = Arrange_tf * Incr_fcode_tf[sv] + All_codephase_tf[sv]
        Navbits = tf.floordiv(Multiplier,20460)
        Multiplier = Multiplier - Navbits * 20460
        Navbits = Navbits + All_Navbits_tf[sv]

        Codephase=tf.cast(Multiplier,dtype=tf.int64)
        Codephase=tf.reshape(Codephase,[500000,1])
        Codes= tf.gather_nd(SV_tf[:,sv],Codephase)

        Codebits=tf.cast(Navbits,dtype=tf.int64)
        Codebits = tf.reshape(Codebits, [500000, 1])
        Bits=tf.gather_nd(Navmessage_tf[:,sv],Codebits)

        #print(Bits.get_shape())
        #print(Codes.get_shape())

        f = Arrange_tf * Incr_phase_tf[sv] + All_carrphase_tf[sv]
        Cos = tf.cos(2 * np.pi * f)
        Sin = tf.sin(2 * np.pi * f)

        I=  tf.cast(Bits,dtype=tf.float64) *tf.cast(Codes,dtype=tf.float64) * Cos * gain_tf[sv]
        Q = tf.cast(Bits, dtype=tf.float64) * tf.cast(Codes, dtype=tf.float64) * Sin * gain_tf[sv]
        #I=tf.ones(shape=(500000,),dtype=tf.float64)*2400
        #Q=tf.ones(shape=(500000,),dtype=tf.float64)*2400
        I_tf=tf.add(I_tf,I)
        Q_tf=tf.add(Q_tf,Q)


    #print((Navbits.get_shape()))
    return I_tf,Q_tf
f1=contf(Arrange_tf,Incr_fcode_tf,All_codephase_tf,All_Navbits_tf,SV_tf,Navmessage_tf, Incr_phase_tf,All_carrphase_tf,gain_tf,I_tf,Q_tf)

I1=np.zeros((500000,),dtype=np.float64)
Q1 = np.zeros((500000,), dtype=np.float64)

buffer = np.zeros((500000, 2))
delay_start=np.zeros((15, 2))
with open("Mum2.bin", 'ab+') as b:
    with tf.Session() as sess:
        start = time.time()
        #--------------------------added to compensate for delay--------------------------------------------
        #with tf.device("cpu:0"):
        #    b.write(np.array(delay_start, dtype=np.int16))
        #---------------------------------------------------------------------------------------------------
        for iter in range(0,1000):
            print(iter)
            buffer[:, 0],buffer[:, 1] = sess.run(f1, feed_dict={Arrange_tf: arrange,
                                                   Incr_fcode_tf:Incr_fcode[iter,:],All_codephase_tf:All_codephase[iter,:],
                                                   All_Navbits_tf:All_Navbits[iter,:],
                                                   SV_tf:SV,Navmessage_tf:Navmessage,
                                                   Incr_phase_tf: Incr_phase[iter, :],All_carrphase_tf: All_carrphase[iter,:],
                                                   gain_tf: gain[iter, :],
                                                   I_tf:I1,Q_tf:Q1
                                                                         })
            with tf.device("cpu:0"):
                b.write(np.array(buffer, dtype=np.int16))
            #np.savetxt("siddd.csv", np.array(buffer,dtype=np.int16), delimiter=",")
        end = time.time()
print("Time:" + str(end - start))
#print((Navbits))
sid=np.asarray([[1],[4], [2]])
print(print(sid.shape))'''
