# import tensorflow as tf

import numpy as np
import time
import pandas as pd
import sys

np.set_printoptions(threshold=sys.maxsize, precision=17, suppress=True)
# np.set_printoptions()
import copy

from SV_functions import generate_nav_bits_for_30_seconds
from SV_functions import convert, generate_SV_Codes, constdict
from datetime import datetime

from SV_functions import read_alamanc_file, subframe_creation
from SV_functions import convert, generate_SV_Codes, constdict

import SV_functions.gps_constants as constants
from SV_functions.calculate_range_sv import calculate_range_sv
from SV_functions.calculate_position_sv import calculate_position_sv
import SV_functions.constdict
import math
import copy


#[40.0096856, 116.478479, 100]
play_parameters = {
    "Valid PRN": [2, 9, 6, 19, 12, 25, 5, 29, 13],
    "satellite_parameters": [0],
    "location_llh": [40.0096856, 116.478479, 100],
    "location_xyz": [],
    "samplerate": 5000000,
    "CA_Code": generate_SV_Codes.GPS_Code_20(),
    "satellite_parameter_iter_step" : 0,
    "scenario_time": {
        "seconds": 6,
        "Week": 1006,
    },
    "satellite_parameters_refresh_rate": 1,
    "page_id": 1,
    "Nav_message": np.zeros((32, 1530)),
    "ephemeris_from_file": read_alamanc_file.get_alamanac(filename="UPLOADED_ALAMANAC/December_2018.txt"),
    "almanac_struct45": {},
    "subframes_123_from_ephemeris": {},
    "ephemeris": {},
    "initial_params":{},
    "debug" : False ,
    "duration" : 900,
    "output_file" : "pune_900.bin"
}


def initial_navbit_array_30(play_parameters):
    all_raw_nav_bits = np.zeros((32, 1530), dtype=np.int)

    x, y, z = convert.gps_to_ecef(play_parameters["location_llh"][0], play_parameters["location_llh"][1],
                                  play_parameters["location_llh"][2])
    play_parameters["location_xyz"] = [x, y, z]
    play_parameters["subframes_123_from_ephemeris"] = subframe_creation.ephemeris_to_subframe(
        play_parameters["ephemeris_from_file"])
    play_parameters["almanac_struct45"] = subframe_creation.ephemeris_to_almanac_subframe(
        play_parameters["ephemeris_from_file"])

    # update ephemeris mk and toe and toc for satellite parameter calculcation
    play_parameters["ephemeris"] = generate_nav_bits_for_30_seconds.add_time_parameters_to_ephemeris(
        copy.deepcopy(play_parameters["ephemeris_from_file"]), play_parameters["scenario_time"])

    # get navigation_bits for 30 seconds , initial last previous word as all zeros
    navigation_message = generate_nav_bits_for_30_seconds.generate_nav_for_30_sec(
        play_parameters["subframes_123_from_ephemeris"],
        play_parameters["almanac_struct45"],
        play_parameters["page_id"],
        play_parameters["scenario_time"]["seconds"],
        play_parameters["scenario_time"]["Week"],
        play_parameters["ephemeris"])

    navbit = generate_nav_bits_for_30_seconds.get_1500_nav_sting(navigation_message, play_parameters["Valid PRN"])

    for Valid_PRN in play_parameters["Valid PRN"]:
        raw_bits_array = np.fromstring(navbit[Valid_PRN], 'u1') - ord('0')
        all_raw_nav_bits[Valid_PRN - 1, 30:1530] = raw_bits_array
        all_raw_nav_bits[Valid_PRN - 1, 0:30] = play_parameters["Nav_message"][Valid_PRN - 1, 0:30]
    all_raw_nav_bits[all_raw_nav_bits == 0] = (-1)
    print("NAV BITS START TIME - ", play_parameters["scenario_time"]["seconds"])

    return all_raw_nav_bits

def next_navbit_array_30(play_parameters):
    all_raw_nav_bits = np.zeros((32, 1530), dtype=np.int)

    # increment scenario time
    play_parameters["scenario_time"]["seconds"] = play_parameters["scenario_time"]["seconds"] + 30

    # increment page id
    play_parameters["page_id"] = play_parameters["page_id"] + 1
    if play_parameters["page_id"] == 26:
        play_parameters["page_id"] = 1

    # update ephemeris mk and toe and toc for satellite parameter calculation
    play_parameters["ephemeris"] = generate_nav_bits_for_30_seconds.add_time_parameters_to_ephemeris(
        copy.deepcopy(play_parameters["ephemeris_from_file"]), play_parameters["scenario_time"])

    # get navigation_bits for 30 seconds , initial last previous word as all zeros
    navigation_message = generate_nav_bits_for_30_seconds.generate_nav_for_30_sec(
        play_parameters["subframes_123_from_ephemeris"],
        play_parameters["almanac_struct45"],
        play_parameters["page_id"],
        play_parameters["scenario_time"]["seconds"],
        play_parameters["scenario_time"]["Week"],
        play_parameters["ephemeris"])

    navbit = generate_nav_bits_for_30_seconds.get_1500_nav_sting(navigation_message, play_parameters["Valid PRN"])

    for Valid_PRN in play_parameters["Valid PRN"]:
        raw_bits_array = np.fromstring(navbit[Valid_PRN], 'u1') - ord('0')
        all_raw_nav_bits[Valid_PRN - 1, 30:1530] = raw_bits_array
        all_raw_nav_bits[Valid_PRN - 1, 0:30] = play_parameters["Nav_message"][Valid_PRN - 1, 0:30]
    all_raw_nav_bits[all_raw_nav_bits == 0] = (-1)
    print("NAV BITS CHNAGED TIME - ", play_parameters["scenario_time"]["seconds"])

    # CHANGE ACTIVE NAV BIT BY 1500
    # for Valid_PRN in play_parameters["Valid PRN"]:
    #    play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][Valid_PRN]["Nav_Bit"] = play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][Valid_PRN]["Nav_Bit"] - 1500


    return all_raw_nav_bits

def generate_satellite_parameters_for_30_seconds(play_parameters,initial = False):
    if initial == True :
        # initialise previous carr phase to zero
        play_parameters["initial_parameters"] = {}
        for sv in play_parameters["Valid PRN"]:
            play_parameters["initial_parameters"][sv] = {}
            play_parameters["initial_parameters"][sv]["Carr_phase"] = 0

    Eph = play_parameters["ephemeris"]
    scenario_time = play_parameters["scenario_time"]["seconds"]
    xyz = play_parameters["location_xyz"]
    samplerate = play_parameters["samplerate"]
    prnlist = play_parameters["Valid PRN"]

    refresh_rate = play_parameters["satellite_parameters_refresh_rate"]
    time_steps = int(30 / refresh_rate)
    T = 1 / samplerate
    scenario_time = scenario_time - 1
    all_satellite_parameters = []

    for time_step in range(0, time_steps):
        current_parameters = {}
        current_time = scenario_time + ((time_step + 1) * refresh_rate)
        for sv in prnlist:
            current_parameters[sv] = copy.deepcopy(SV_functions.constdict.satellite_parameter_template)
            old_time = current_time - refresh_rate
            old_rho = calculate_range_sv(Eph, sv, old_time, xyz)
            current_rho = calculate_range_sv(Eph, sv, current_time, xyz)
            rhorate = (current_rho[sv]["range"] - old_rho[sv]["range"]) / refresh_rate
            doppler = -rhorate / constants.LAMBDA_L1
            codephase = constants.CODE_FREQ + doppler * constants.CARR_TO_CODE

            # Initial code phase and data bit counters.
            time_for_transmission = current_rho[sv]["range"] / constants.SPEED_OF_LIGHT
            number_of_ca_code_bits = time_for_transmission / constants.TIME_FOR_1_CA_BIT

            # print(number_of_ca_code_bits)
            Codephase = (constants.CA_SEQ_LEN * 20) - (number_of_ca_code_bits % (constants.CA_SEQ_LEN * 20))
            Bit_Number = 30 - math.ceil(time_for_transmission / constants.TIME_FOR_1_NAV_BIT)

            current_parameters[sv]["SV"] = sv
            current_parameters[sv]["TOWC"] = current_time
            current_parameters[sv]["Pseudorange"] = current_rho[sv]["range"]
            current_parameters[sv]["coderate"] = codephase
            current_parameters[sv]["Dopplers"] = doppler
            current_parameters[sv]["Code_phase"] = Codephase
            current_parameters[sv]["Nav_Bit"] = Bit_Number

            # carry forward carr phase
            current_parameters[sv]["Carr_phase"] = current_parameters[sv]["Carr_phase"]

            current_parameters[sv]["Code_Phase_Step"] = current_parameters[sv]["coderate"] * T
            current_parameters[sv]["Carr_Phase_Step"] = current_parameters[sv]["Dopplers"] * T
            current_parameters[sv]["gain"] = int(2400 / len(prnlist))

            pos, vel, clk = calculate_position_sv(Eph, sv, current_time)

            current_parameters[sv]["check_data"]["pos"] = pos
            current_parameters[sv]["check_data"]["vel"] = vel
            current_parameters[sv]["check_data"]["clk"] = clk

        all_satellite_parameters.append(current_parameters)
    return all_satellite_parameters

play_parameters["Nav_message"] = initial_navbit_array_30(play_parameters)
play_parameters["satellite_parameters"] = generate_satellite_parameters_for_30_seconds(play_parameters,initial=True)


Multiplier = (np.arange( play_parameters["samplerate"]* (play_parameters["satellite_parameters_refresh_rate"] ) ) + 1).astype(int)


if play_parameters["debug"] == True :
    print("--------- Initial Params --------")
    print("Number of PRNs > 1500 = ", 0)
    for sv in play_parameters["Valid PRN"]:
        print("SV - ", sv, " Bit - ", play_parameters["satellite_parameters"][0][sv]["Nav_Bit"],
              "Code Phase - ", play_parameters["satellite_parameters"][0][sv]["Code_phase"],
              "Carr Phase - ", play_parameters["satellite_parameters"][0][sv]["Carr_phase"],
              "Pseudorange - ", play_parameters["satellite_parameters"][0][sv]["Pseudorange"],
              "Doppler - ", play_parameters["satellite_parameters"][0][sv]["Dopplers"],
              "Step - ", play_parameters["satellite_parameter_iter_step"],
              )
    print("-----------------")

start_time = datetime.now()

with open(play_parameters["output_file"], 'ab+') as b:
    duration = int((1/play_parameters["satellite_parameters_refresh_rate"]) * play_parameters["duration"])
    for iter in range(0,duration):
        buffer = np.zeros((int(play_parameters["samplerate"] * (play_parameters["satellite_parameters_refresh_rate"] ) ), 2))
        for sv in play_parameters["Valid PRN"]:
            CodePhase_Array = (Multiplier *
                               play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv]['Code_Phase_Step']) + \
                              play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv]["Code_phase"]

            Navbits = (np.floor((CodePhase_Array / (constants.CA_SEQ_LEN * 20)))).astype(int)
            CodePhase_Array = CodePhase_Array - Navbits * (constants.CA_SEQ_LEN * 20)
            # print(CodePhase_Array[0:20460])

            Navbits = Navbits + play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv]["Nav_Bit"]
            CarrPhase_Array = (Multiplier * play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv]['Carr_Phase_Step']) + \
                              play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv]["Carr_phase"]

            if play_parameters["satellite_parameter_iter_step"] + 1 < 30:
                # replace the current codephase and navbit in play status
                play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"] + 1][sv]["Nav_Bit"] = Navbits[-1]
                # play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"] + 1][sv]["Code_phase"] = CodePhase_Array[-1]
                play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"] + 1][sv]["Carr_phase"] = CarrPhase_Array[-1]
            else :
                # play_parameters["initial_parameters"][sv]["Nav_Bit"] = Navbits[-1]
                play_parameters["initial_parameters"][sv]["Carr_phase"] = CarrPhase_Array[-1]

            # Take actual values from CA table and Nav bits
            Actual_codes = np.take(play_parameters["CA_Code"][sv-1], CodePhase_Array.astype(int))
            Actual_nav_bits = np.take(play_parameters["Nav_message"][sv-1], Navbits.astype(int))

            # Calculation for I and Q Components
            Real_Signal = Actual_nav_bits * Actual_codes * play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv]["gain"]
            I = Real_Signal * np.cos(2 * np.pi * CarrPhase_Array)
            Q = Real_Signal * np.sin(2 * np.pi * CarrPhase_Array)

            # print(np.shape(I))
            buffer[:, 0] += I
            buffer[:, 1] += Q

        print("Time into Run - ", (iter+1))
        if play_parameters["debug"] == True :
            for sv in play_parameters["Valid PRN"]:
                print("SV - ", sv, " Bit - ",
                      play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv][
                          "Nav_Bit"],
                      "Code Phase - ",
                      play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv][
                          "Code_phase"],
                      "Carr Phase - ",
                      play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv][
                          "Carr_phase"],
                      "Pseudorange - ",
                      play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv][
                          "Pseudorange"],
                      "Doppler - ",
                      play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv][
                          "Dopplers"],
                      "Step - ", play_parameters["satellite_parameter_iter_step"],
                      )
        print("-----------------")

        play_parameters["satellite_parameter_iter_step"] = play_parameters["satellite_parameter_iter_step"] + 1
        if play_parameters["satellite_parameter_iter_step"] % 30 == 0:
            # change satellite parameters
            play_parameters["Nav_message"] = next_navbit_array_30(play_parameters)
            play_parameters["satellite_parameters"] = generate_satellite_parameters_for_30_seconds(play_parameters)
            play_parameters["satellite_parameter_iter_step"] = 0


        b.write(np.array(buffer, dtype=np.int16))
stop_time = datetime.now()
total_time = (stop_time - start_time).total_seconds()
print("TIME FOR RUN - ",total_time)

