# import tensorflow as tf

import numpy as np
import time
import pandas as pd
import sys

np.set_printoptions(threshold=sys.maxsize, precision=17, suppress=True)
# np.set_printoptions()
import copy

from SV_functions import read_alamanc_file, generate_satellite_parameters_for_30_seconds, subframe_creation, \
    generate_nav_bits_for_30_seconds
from SV_functions import convert, generate_SV_Codes, constdict
import SV_functions.gps_constants as constants

from Basic_functions import dump_list_to_csv

# 1,2,3,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]
#
play_parameters = {
    "Valid PRN": [2, 9, 6, 19, 17, 12, 25, 23, 5, 29, 13],
    "scenario_time": {
        "seconds": 6,
        "Week": 1006,
    },
    "page_id": 1,
    "previous_word": np.zeros((32, 30)),
    "satellite_parameters": {},
    "previous_satellite_parameters": {},
    "Nav_message": np.zeros((32, 1530)),
    "ephemeris_from_file": read_alamanc_file.get_alamanac(filename="UPLOADED_ALAMANAC/December_2018.txt"),
    "almanac_struct45": {},
    "subframes_123_from_ephemeris": {},
    "ephemeris": {},
    "location_llh": [40.0096856, 116.478479, 100],
    "location_xyz": [],
    "samplerate": 5000000,
    "satellite_parameters_refresh_rate": int(1 / constants.refresh_rate),
    "satellite_parameter_iter_step": 0,
    "CA_Code": generate_SV_Codes.GPS_Code_20()
}


def initialise_play_parameters():
    x, y, z = convert.gps_to_ecef(40.0096856, 116.478479, 100)
    print(x, y, z)
    play_parameters["location_xyz"] = [x, y, z]
    play_parameters["subframes_123_from_ephemeris"] = subframe_creation.ephemeris_to_subframe(
        play_parameters["ephemeris_from_file"])
    play_parameters["almanac_struct45"] = subframe_creation.ephemeris_to_almanac_subframe(
        play_parameters["ephemeris_from_file"])

    # update ephemeris mk and toe and toc for satellite parameter calculcation
    play_parameters["ephemeris"] = generate_nav_bits_for_30_seconds.add_time_parameters_to_ephemeris(
        copy.deepcopy(play_parameters["ephemeris_from_file"]), play_parameters["scenario_time"])

    # get satellite parameters for 30 seconds
    play_parameters[
        "satellite_parameters"] = generate_satellite_parameters_for_30_seconds.generate_satellite_parameters_for_30_seconds(
        play_parameters["ephemeris"],
        play_parameters["scenario_time"]["seconds"],
        play_parameters["location_xyz"],
        play_parameters["samplerate"])

    # get navigation_bits for 30 seconds , initial last previous word as all zeros
    navigation_message = generate_nav_bits_for_30_seconds.generate_nav_for_30_sec(
        play_parameters["subframes_123_from_ephemeris"],
        play_parameters["almanac_struct45"],
        play_parameters["page_id"],
        play_parameters["scenario_time"]["seconds"],
        play_parameters["scenario_time"]["Week"],
        play_parameters["ephemeris"])

    play_parameters["Nav_message"] = generate_nav_bits_for_30_seconds.get_1500_nav_bit_array(navigation_message,
                                                                                             play_parameters[
                                                                                                 "previous_word"])


# af0 and omega0 are not matching
def renew_play_parameters():
    # increment page id
    play_parameters["page_id"] = play_parameters["page_id"] + 1
    if play_parameters["page_id"] == 26:
        play_parameters["page_id"] = 1

    # Store previous satellite params
    play_parameters["previous_satellite_parameters"] = copy.deepcopy(play_parameters["satellite_parameters"])

    # update ephemeris mk and toe and toc for satellite parameter calculation
    play_parameters["ephemeris"] = generate_nav_bits_for_30_seconds.add_time_parameters_to_ephemeris(
        copy.deepcopy(play_parameters["ephemeris_from_file"]), play_parameters["scenario_time"])

    # get satellite parameters for 30 seconds
    play_parameters[
        "satellite_parameters"] = generate_satellite_parameters_for_30_seconds.generate_satellite_parameters_for_30_seconds(
        play_parameters["ephemeris"],
        play_parameters["scenario_time"]["seconds"],
        play_parameters["location_xyz"],
        play_parameters["samplerate"])

    # keep code phase and carr phase as last known
    for sv in play_parameters["Valid PRN"]:
        play_parameters["satellite_parameters"][sv][0]["Nav_Bit"] = \
        play_parameters["previous_satellite_parameters"][sv][play_parameters["satellite_parameter_iter_step"]][
            "Nav_Bit"] - 1500
        play_parameters["satellite_parameters"][sv][0]["Code_phase"] = \
        play_parameters["previous_satellite_parameters"][sv][play_parameters["satellite_parameter_iter_step"]][
            "Code_phase"]
        play_parameters["satellite_parameters"][sv][0]["Carr_phase"] = \
        play_parameters["previous_satellite_parameters"][sv][play_parameters["satellite_parameter_iter_step"]][
            "Carr_phase"]

    # set previous word as last 30 bits of nav message
    play_parameters["previous_word"] = play_parameters["Nav_message"][:, 1500:1530]

    # get navigation_bits for 30 seconds , initial last previous word as all zeros
    navigation_message = generate_nav_bits_for_30_seconds.generate_nav_for_30_sec(
        play_parameters["subframes_123_from_ephemeris"],
        play_parameters["almanac_struct45"],
        play_parameters["page_id"],
        play_parameters["scenario_time"]["seconds"],
        play_parameters["scenario_time"]["Week"],
        play_parameters["ephemeris"])

    play_parameters["Nav_message"] = generate_nav_bits_for_30_seconds.get_1500_nav_bit_array(navigation_message,
                                                                                             play_parameters[
                                                                                                 "previous_word"])

    # set satellite parameter step to 0
    play_parameters["satellite_parameter_iter_step"] = 0


initialise_play_parameters()
# print(play_parameters["satellite_parameters"][1])


Multiplier = np.arange(play_parameters["samplerate"] * constants.refresh_rate) + 1

print("--------- Initial Params --------")
print("Number of PRNs > 1500 = ", 0)
for sv in play_parameters["Valid PRN"]:
    print("SV - ", sv, " Bit - ", play_parameters["satellite_parameters"][sv][0]["Nav_Bit"],
          "Code Phase - ", play_parameters["satellite_parameters"][sv][0]["Code_phase"],
          "Carr Phase - ", play_parameters["satellite_parameters"][sv][0]["Carr_phase"],
          "Pseudorange - ", play_parameters["satellite_parameters"][sv][0]["Pseudorange"],
          "Doppler - ", play_parameters["satellite_parameters"][sv][0]["Dopplers"],
          "Step - ", play_parameters["satellite_parameter_iter_step"],
          )
print("Scenario Time - ", play_parameters["scenario_time"]["seconds"])
print("-----------------")

with open("try14.bin", 'ab+') as b:
    for iter in range(1500):

        # increment time by refresh rate
        play_parameters["scenario_time"]["seconds"] = play_parameters["scenario_time"][
                                                          "seconds"] + constants.refresh_rate
        if play_parameters["scenario_time"]["seconds"] > constants.SECONDS_IN_WEEK:
            play_parameters["scenario_time"]["seconds"] = play_parameters["scenario_time"][
                                                              "seconds"] - constants.SECONDS_IN_WEEK
            play_parameters["scenario_time"]["Week"] = play_parameters["scenario_time"]["Week"] + 1
        # -----------------------------------------------------------------------------------------------------------

        buffer = np.zeros(int(play_parameters["samplerate"] * constants.refresh_rate), 2)
        flag_renew_play_parameters = 0

        for sv in play_parameters["Valid PRN"]:

            CodePhase_Array = (Multiplier * play_parameters["satellite_parameters"][sv]['Code_Phase_Step']) + \
                              play_parameters["satellite_parameters"][sv]["Code_phase"]
            # print(CodePhase_Array[0:20460])
            Navbits = np.floor((CodePhase_Array / (constants.CA_SEQ_LEN * 20)))
            CodePhase_Array = CodePhase_Array - Navbits * (constants.CA_SEQ_LEN * 20)
            # print(CodePhase_Array[0:20460])

            Navbits = Navbits + play_parameters["satellite_parameters"][sv]["Nav_Bit"]
            CarrPhase_Array = (Multiplier * play_parameters["satellite_parameters"][sv]['Carr_Phase_Step']) + \
                              play_parameters["satellite_parameters"][sv]["Carr_phase"]

            # Take actual values from CA table and Nav bits
            Actual_codes = np.take(play_parameters["CA_Code"][sv], CodePhase_Array.astype(int))
            Actual_nav_bits = np.take(play_parameters["Nav_message"][sv], Navbits.astype(int))

            # Calculation for I and Q Components
            Real_Signal = Actual_nav_bits * Actual_codes * play_parameters["satellite_parameters"][sv]["gain"]
            I = Real_Signal * np.cos(2 * np.pi * CarrPhase_Array)
            Q = Real_Signal * np.sin(2 * np.pi * CarrPhase_Array)

            # print(np.shape(I))
            buffer[:, 0] += I
            buffer[:, 1] += Q

            # replace the current codephase and navbit in play status
            play_parameters["satellite_parameters"][sv]["Nav_Bit"] = Navbits[-1]
            play_parameters["satellite_parameters"][sv]["Code_phase"] = CodePhase_Array[-1]
            play_parameters["satellite_parameters"][sv]["Carr_phase"] = CarrPhase_Array[-1]

            # if Nav bit is greater than 1500 , get new satellite params and nav message
            if play_parameters["satellite_parameters"][sv][play_parameters["satellite_parameter_iter_step"]][
                "Nav_Bit"] >= 1500:
                flag_renew_play_parameters += 1

        if flag_renew_play_parameters != 0:
            print("-----------------")
            print("Number of PRNs > 1500 = ", flag_renew_play_parameters)
            renew_play_parameters()
            for sv in play_parameters["Valid PRN"]:
                print("SV - ", sv, " Bit - ",
                      play_parameters["satellite_parameters"][sv][play_parameters["satellite_parameter_iter_step"]][
                          "Nav_Bit"],
                      "Code Phase - ",
                      play_parameters["satellite_parameters"][sv][play_parameters["satellite_parameter_iter_step"]][
                          "Code_phase"],
                      "Carr Phase - ",
                      play_parameters["satellite_parameters"][sv][play_parameters["satellite_parameter_iter_step"]][
                          "Carr_phase"],
                      "Pseudorange - ",
                      play_parameters["satellite_parameters"][sv][play_parameters["satellite_parameter_iter_step"]][
                          "Pseudorange"],
                      "Doppler - ",
                      play_parameters["satellite_parameters"][sv][play_parameters["satellite_parameter_iter_step"]][
                          "Dopplers"],
                      )
            print("Scenario Time Changed - ", play_parameters["scenario_time"]["seconds"])
            print("-----------------")

        print("Done Iter - ", iter)
        b.write(np.array(buffer, dtype=np.int16))

print("Done")

