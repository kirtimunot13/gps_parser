#from pymap3d.src import pymap3d as pm
import Coordinate_functions as pm

lat=40.0096856
lon=116.478479
alt=100.0
x, y, z = pm.geodetic2ecef(lat, lon, alt)
print("lat-x",lat," ",x,"long_y",lon," ",y,"alt-z",alt,z)
x=-1802093.250824
y=15609245.793447
z=22008681.601778

u,v,w=pm.ecef2enu(x,y,z,40.0096856,116.478479,100.0)
print(u,v,w)

a,e,r=pm.enu2aer(u,v,w)
print(a,e,r)

from Time_functions import gpsFromUTC,UTCFromGps
gpsWeek, gpsSOW, gpsDay, gpsSOD=gpsFromUTC(2018,12,2,0,0,0)
print(gpsWeek%1024, gpsSOW, gpsDay, gpsSOD)

year, month, day, hh, mm, secFract=UTCFromGps(gpsWeek, gpsSOW, leapSecs=14)
print(year, month, day, hh, mm, secFract)



csv_dump = []
temp = [ "SV" , "TOWC" , "Psuedorange", "coderate","Dopplers","Code_phase","Nav_Bit", "Carr_phase","Carr_Phase_Step","Code_Phase_Step","gain",
         "Pos X", "Pos Y", "Pos Z" ,"Vel X", "Vel Y" , "Vel Z"]
csv_dump.append(temp)
for Valid_PRN in play_parameters["Valid PRN"]:
    temp = []
    temp.append(play_parameters["satellite_parameters"][Valid_PRN]["SV"])
    temp.append(play_parameters["satellite_parameters"][Valid_PRN]["TOWC"])
    temp.append(play_parameters["satellite_parameters"][Valid_PRN]["Pseudorange"])
    temp.append(play_parameters["satellite_parameters"][Valid_PRN]["coderate"])
    temp.append(play_parameters["satellite_parameters"][Valid_PRN]["Dopplers"])
    temp.append(play_parameters["satellite_parameters"][Valid_PRN]["Code_phase"])
    temp.append(play_parameters["satellite_parameters"][Valid_PRN]["Nav_Bit"])
    temp.append(play_parameters["satellite_parameters"][Valid_PRN]["Carr_phase"])
    temp.append(play_parameters["satellite_parameters"][Valid_PRN]["Code_Phase_Step"])
    temp.append(play_parameters["satellite_parameters"][Valid_PRN]["Carr_Phase_Step"])
    temp.append(play_parameters["satellite_parameters"][Valid_PRN]["gain"])

    temp.append(play_parameters["satellite_parameters"][Valid_PRN]["check_data"]["pos"][0])
    temp.append(play_parameters["satellite_parameters"][Valid_PRN]["check_data"]["pos"][1])
    temp.append(play_parameters["satellite_parameters"][Valid_PRN]["check_data"]["pos"][2])

    temp.append(play_parameters["satellite_parameters"][Valid_PRN]["check_data"]["vel"][0])
    temp.append(play_parameters["satellite_parameters"][Valid_PRN]["check_data"]["vel"][1])
    temp.append(play_parameters["satellite_parameters"][Valid_PRN]["check_data"]["vel"][2])

    csv_dump.append(temp)
dump_list_to_csv(filename="Satellite.csv",dlist= csv_dump)

