from SV_functions import constdict
import copy
import math
import pandas as pd
from Basic_functions import binary, twos_complement
import SV_functions.gps_constants as constants


# ****************ALM***************

# "D:\gps_parser\March1_2019_yuma.txt"
# MAKE SURE THEY ARE RELATIVE PATHS
# CREATE  DIRECTORY IN THE FOLDER STRUCTURE CALLED AS "UPLOADED_ALAMANAC" and refer to its files
def get_alamanac(filename):
    file = open(filename, "r")
    Content = file.read()
    CoList = Content.split("\n")

    id1 = []
    alm_data = []
    health = []
    e = []
    m0 = []
    i0 = []
    omegadot = []
    omega0 = []
    omega = []
    sqa = []
    af0 = []
    af1 = []
    toa = []
    wek = []

    def remove(string):
        return string.replace(" ", "")

    def data_list(line):
        d = []
        spl_word = ':'
        d.append(remove(line.partition(spl_word)[2]))
        return d

    for i in range(0, len(CoList)):
        # print(prn)
        c = CoList[i]
        x = []
        # print(c)
        if '********' not in c:
            if "ID:" in c:
                x = data_list(CoList[i])
                id1.append(int(x[0]))
            if "Health:" in c:
                x = data_list(CoList[i])
                health.append(float(x[0]))
            if 'Eccentricity:' in c:
                x = data_list(CoList[i])
                e.append(float(x[0]))
            if 'Mean' in c:
                x = data_list(CoList[i])
                m0.append(float(x[0]))
            if 'Orbital' in c:
                x = data_list(CoList[i])
                i0.append(float(x[0]))
            if 'Ascen(r/s):' in c:
                x = data_list(CoList[i])
                omegadot.append(float(x[0]))
            if 'Week(rad):' in c:
                x = data_list(CoList[i])
                omega0.append(float(x[0]))
            if 'Perigee(rad):' in c:
                x = data_list(CoList[i])
                omega.append(float(x[0]))
                # temp['Omega']
            if 'SQRT(A)' in c:
                x = data_list(CoList[i])
                sqa.append(float(x[0]))
                # temp['sqrt_A']
            if 'Af0(s):' in c:
                x = data_list(CoList[i])
                af0.append(float(x[0]))
                # temp['a_f0']
            if 'Af1(s/s):' in c:
                x = data_list(CoList[i])
                af1.append(float(x[0]))
                # temp['a_f1']
            if 'Time' in c:
                x = data_list(CoList[i])
                toa.append(float(x[0]))
                # temp['toa']
            if 'week:' in c:
                x = data_list(CoList[i])
                wek.append(int(x[0]))
                # temp['Week Number'] = CoList[i].partition(spl_word)[2]
        else:
            pass
    # CHANGE THIS CODE TO BRING IN EPH FORMAT
    tempn = copy.deepcopy(constdict.ephemeris)
    temp_keys = list(tempn.keys())
    # print(len(temp_keys))

    for i in range(0, len(id1)):
        temp = copy.deepcopy(constdict.ephemeris_template)
        temp['PRN'] = id1[i]
        temp['Health'] = health[i]
        temp['e'] = e[i]
        temp['M_0'] = m0[i]
        temp['I_0'] = i0[i]
        temp["delta_i"] = temp['I_0'] - 0.3 * constants.pi
        temp['omegaDot'] = omegadot[i]
        temp['Omega_0'] = omega0[i]
        temp['Omega'] = omega[i]
        temp['sqrt_A'] = sqa[i]
        temp['a_f0'] = af0[i]
        temp['a_f1'] = af1[i]
        temp['toa'] = toa[i]
        temp['Week Number'] = wek[i]
        # print(temp)
        # print(data)
        for j in range(0, len(temp_keys)):
            if temp_keys[j] == temp['PRN']:
                tempn.update({temp_keys[j]: temp})
    print("FOUND VALID ALAMANCS FOR SVs -", len(tempn))
    return tempn


# CODE TO GET EPHEMERIS FROM RAW NAV BITS
# RETURN ephemeris dict as in const_dict
# AGAIN CREATE DIRECTORY IN FOLDER STRUCTURE AS  "UPLOADED RAW NAV_BITS" and refer to its files

def get_almanac_from_raw_navbits_new(filename):
    alm_list = []
    Keysight_data = pd.read_csv(filename)
    BladeRF_data = Keysight_data.to_dict(orient="record")

    Parsed_data = {}  # copy.deepcopy(constdict.subframes)
    # Create a structure of 32 with in satellites each with 5 subframes

    for record in BladeRF_data:
        tempr = {}
        tempr["Bits"] = ""
        for i in range(1, 38):
            tempr["Bits"] += format(record['Raw Navbits – ' + str(i)], '08b')
        tempr['TOWC'] = record['TOWC (corresponding to start of the sub-frame) (s)']
        tempr["WN"] = record['Week No (corresponding to start of the sub-frame)']
        tempr["PRN"] = record["PRN"]

        tempr['subframe id'] = record['subframe id']
        # print(tempr["PRN"],tempr['subframe id'])

        if tempr["PRN"] in Parsed_data:
            if tempr['subframe id'] in Parsed_data[tempr["PRN"]]:
                pass
            else:
                Parsed_data[tempr["PRN"]][tempr["subframe id"]] = tempr
        else:
            if tempr['subframe id'] == 1:
                Parsed_data[tempr["PRN"]] = {}
                Parsed_data[tempr["PRN"]][tempr["subframe id"]] = tempr
    # print(Parsed_data)

    Valid_PRNs = list(Parsed_data.keys())
    print("Found PRNs - ", Valid_PRNs)

    ephemeris = copy.deepcopy(constdict.ephemeris)
    for Valid_PRN in Valid_PRNs:
        ephemeris_template = copy.deepcopy(constdict.ephemeris_template)
        ephemeris_template["PRN"] = Valid_PRN
        ephemeris_template["page id"] = "-"
        for subframe in range(1, 4):
            nav_bits = "10001011" + Parsed_data[Valid_PRN][subframe]["Bits"]
            # print(Parsed_data[Valid_PRN][subframe]['TOWC'])
            if subframe == 1:
                # print(Valid_PRN,"1 read",Parsed_data[Valid_PRN][subframe]['TOWC'])
                ephemeris_template["TOWC"] = binary(nav_bits[30:47])*6
                ephemeris_template["Week Number"] = binary(nav_bits[60:70])
                ephemeris_template["Accuracy"] = binary(nav_bits[72:76])
                # print(nav_bits[76:82])
                ephemeris_template["Health"] = binary(nav_bits[76:82])
                ephemeris_template["T_GD"] = binary(nav_bits[196:204]) * constants.POW2_M31
                ephemeris_template["IODC"] = binary(nav_bits[82:84] + nav_bits[210: 218])
                ephemeris_template["t_oc"] = binary(nav_bits[218:234]) * (2 ** 4)
                ephemeris_template["a_f2"] = twos_complement(nav_bits[240:248]) * constants.POW2_M55
                ephemeris_template["a_f1"] = round(twos_complement(nav_bits[248:264]) * constants.POW2_M43,20)
                ephemeris_template["a_f0"] = round(twos_complement(nav_bits[270:292]) * constants.POW2_M31,12)
                # New Code
                ephemeris_template["Code Type"] = nav_bits[70:72]  # 10 for CA Code
                ephemeris_template["L2P Flag"] = binary(nav_bits[90:91])  # Must be Zero
                ephemeris_template["Reserved 1"] = nav_bits[91:114]  # Must be alternate 0 and 1
                ephemeris_template["Reserved 2"] = nav_bits[120:144]  # Must be alternate 0 and 1
                ephemeris_template["Reserved 3"] = nav_bits[150:174]  # Must be alternate 0 and 1
                ephemeris_template["Reserved 4"] = nav_bits[180:196]  # Must be alternate 0 and 1
            elif subframe == 2:
                # print(Valid_PRN,"2 read",Parsed_data[Valid_PRN][subframe]['TOWC'])
                ephemeris_template["IODE_sf2"] = (nav_bits[60:68])
                ephemeris_template["C_rs"] = twos_complement(nav_bits[68:84]) * constants.POW2_M5
                ephemeris_template["Deltan"] = twos_complement(nav_bits[90:106]) * constants.POW2_M43 * constants.pi
                ephemeris_template["M_0"] = round(twos_complement(
                    nav_bits[106:114] + nav_bits[120:144]) * constants.POW2_M31 * constants.pi,9)
                '''if Valid_PRN == 22:
                    print("From Nav Bits - " ,ephemeris_template["M_0"])
                    print("From Nav Bits - ", nav_bits[106:114] + nav_bits[120:144])'''
                ephemeris_template["C_uc"] = twos_complement((nav_bits[150:166])) * constants.POW2_M29
                ephemeris_template["e"] = round(binary((nav_bits[166:174] + nav_bits[180:204])) * constants.POW2_M33,12)
                ephemeris_template["C_us"] = twos_complement(nav_bits[210:226]) * constants.POW2_M29
                ephemeris_template["sqrt_A"] = round(binary(nav_bits[226:234] + nav_bits[240:264]) * constants.POW2_M19,6)
                ephemeris_template["t_oe"] = binary(nav_bits[270:286]) * (2 ** 4)
                if Valid_PRN == 2 :
                    print(nav_bits[270:286])
                    print(ephemeris_template["t_oe"])

                # New Code
                ephemeris_template["FIT INTERVAL"] = binary(
                    nav_bits[286:287])  # if ephemeris is older than 4 hours then 1
                ephemeris_template["AODO"] = nav_bits[287:292]
            elif subframe == 3:
                # print(Valid_PRN,"3 read", Parsed_data[Valid_PRN][subframe]['TOWC'])
                ephemeris_template["C_ic"] = twos_complement(nav_bits[60:76]) * constants.POW2_M29
                ephemeris_template["Omega_0"] = round(twos_complement(nav_bits[76:84] + nav_bits[90:114]) * (
                            2 ** (-31)) * constants.pi,10)
                ephemeris_template["C_is"] = twos_complement(nav_bits[120:136]) * constants.POW2_M31 * constants.pi
                ephemeris_template["I_0"] = round(twos_complement(
                    nav_bits[136:144] + nav_bits[150:174]) * constants.POW2_M31 * constants.pi,10)
                ephemeris_template["delta_i"] = ephemeris_template['I_0'] - 0.3 * constants.pi

                ephemeris_template["C_rc"] = twos_complement(nav_bits[180:196]) * constants.POW2_M5
                ephemeris_template["Omega"] = round(twos_complement(
                    nav_bits[196:204] + nav_bits[210:234]) * constants.POW2_M31 * constants.pi,10)
                ephemeris_template["omegaDot"] = round(twos_complement(nav_bits[240:264]) * constants.POW2_M43 * constants.pi,18)
                '''if Valid_PRN == 22:
                    print("From Nav Bits - " ,ephemeris_template["omegaDot"])
                    print("From Nav Bits - ", nav_bits[240:264])'''
                ephemeris_template["IODE_sf3"] = (nav_bits[270:278])
                ephemeris_template["iDot"] = binary(nav_bits[278:292]) * constants.POW2_M43 * constants.pi
            else:
                pass
        alm_list.append(ephemeris_template)
        ephemeris[Valid_PRN] = ephemeris_template
    return ephemeris


def subframe_4_parser_SV(subframe4):
    ephemeris_template = copy.deepcopy(constdict.ephemeris_template)
    ephemeris_template["e"] = binary(subframe4[68:84]) * (2 ** (-21))
    ephemeris_template["toa"] = binary(subframe4[90:98]) * (2 ** 12)
    ephemeris_template["I_0"] = twos_complement(subframe4[98:114]) * (2 ** (-19))
    ephemeris_template["omegaDot"] = twos_complement(subframe4[120:136]) * (2 ** (-38)) * constants.pi
    ephemeris_template["sqrt_A"] = binary((subframe4[150:174])) * (2 ** (-11))
    ephemeris_template["Omega_0"] = twos_complement(subframe4[180:204]) * (2 ** (-23)) * constants.pi
    ephemeris_template["Omega"] = twos_complement(subframe4[210:234]) * (2 ** (-23)) * constants.pi
    ephemeris_template["M_0"] = twos_complement(subframe4[240:264]) * (2 ** (-23)) * constants.pi
    ephemeris_template["a_f0"] = twos_complement((subframe4[270:278] + subframe4[289:292])) * (2 ** (-20))
    ephemeris_template["a_f1"] = twos_complement(subframe4[278:289]) * (2 ** -38)
    ephemeris_template["Health"] = subframe4[136:144]  # all zeros
    return ephemeris_template


def subframe_5_parser_SV(subframe5):
    ephemeris_template = copy.deepcopy(constdict.ephemeris_template)

    ephemeris_template["e"] = binary(subframe5[68:84]) * (2 ** (-21))
    ephemeris_template["toa"] = binary(subframe5[90:98]) * (2 ** 12)
    ephemeris_template["I_0"] = twos_complement(subframe5[98:114]) * (2 ** (-19))
    ephemeris_template["omegaDot"] = twos_complement(subframe5[120:136]) * (2 ** (-38)) * constants.pi
    ephemeris_template["sqrt_A"] = binary((subframe5[150:174])) * (2 ** (-11))
    ephemeris_template["Omega_0"] = twos_complement(subframe5[180:204]) * (2 ** (-23)) * constants.pi
    ephemeris_template["Omega"] = twos_complement(subframe5[210:234]) * (2 ** (-23)) * constants.pi
    ephemeris_template["M_0"] = twos_complement(subframe5[240:264]) * (2 ** (-23)) * constants.pi
    ephemeris_template["a_f0"] = twos_complement((subframe5[270:278] + subframe5[289: 292])) * (2 ** (-20))
    ephemeris_template["a_f1"] = twos_complement(subframe5[278:289]) * (2 ** -38)
    # New Code
    ephemeris_template["Health"] = subframe5[136:144]  # all zeros
    return ephemeris_template


def subframe_4_parser_25(subframe4):
    ephemeris_template4_25 = copy.deepcopy(constdict.ephemeris_template4_25)
    ephemeris_template4_25["ALM4_A_SPOOF_SV1"] = binary((subframe4[68:72]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV2"] = binary((subframe4[72:76]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV3"] = binary((subframe4[76:80]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV4"] = binary((subframe4[80:84]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV5"] = binary((subframe4[90:94]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV6"] = binary((subframe4[94:98]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV7"] = binary((subframe4[98:102]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV8"] = binary((subframe4[102:106]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV9"] = binary((subframe4[106:110]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV10"] = binary((subframe4[110:114]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV11"] = binary((subframe4[120:124]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV12"] = binary((subframe4[124:128]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV13"] = binary((subframe4[128:132]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV14"] = binary((subframe4[132:136]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV15"] = binary((subframe4[136:140]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV16"] = binary((subframe4[140:144]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV17"] = binary((subframe4[150:154]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV18"] = binary((subframe4[154:158]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV19"] = binary((subframe4[158:162]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV20"] = binary((subframe4[162:166]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV21"] = binary((subframe4[166:170]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV22"] = binary((subframe4[170:174]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV23"] = binary((subframe4[180:184]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV24"] = binary((subframe4[184:188]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV25"] = binary((subframe4[188:192]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV26"] = binary((subframe4[192:196]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV27"] = binary((subframe4[196:200]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV28"] = binary((subframe4[200:204]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV29"] = binary((subframe4[210:214]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV30"] = binary((subframe4[214:218]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV31"] = binary((subframe4[218:222]))
    ephemeris_template4_25["ALM4_A_SPOOF_SV32"] = binary((subframe4[222:226]))
    ephemeris_template4_25["ALM4_sub4_Res1"] = subframe4[226:228]
    ephemeris_template4_25["ALM4_Health_SV25"] = binary((subframe4[228:234]))
    ephemeris_template4_25["ALM4_Health_SV26"] = binary((subframe4[240:246]))
    ephemeris_template4_25["ALM4_Health_SV27"] = binary((subframe4[246:252]))
    ephemeris_template4_25["ALM4_Health_SV28"] = binary((subframe4[252:258]))
    ephemeris_template4_25["ALM4_Health_SV29"] = binary((subframe4[258:264]))
    ephemeris_template4_25["ALM4_Health_SV30"] = binary((subframe4[270:276]))
    ephemeris_template4_25["ALM4_Health_SV31"] = binary((subframe4[276:282]))
    ephemeris_template4_25["ALM4_Health_SV32"] = binary((subframe4[282:288]))
    ephemeris_template4_25["ALM4_sub4_Res2"] = subframe4[288:292]
    # print(ephemeris_template4_25)
    return ephemeris_template4_25


def subframe_5_parser_25(subframe5):
    ephemeris_template5_25 = copy.deepcopy(constdict.ephemeris_template5_25)
    ephemeris_template5_25["toa"] = binary(subframe5[68:76])
    ephemeris_template5_25["Week Number"] = binary(subframe5[76:84])
    ephemeris_template5_25["Health_SV1"] = binary((subframe5[90:96]))
    ephemeris_template5_25["Health_SV2"] = binary((subframe5[96:102]))
    ephemeris_template5_25["Health_SV3"] = binary((subframe5[102:108]))
    ephemeris_template5_25["Health_SV4"] = binary((subframe5[108:114]))
    ephemeris_template5_25["Health_SV5"] = binary((subframe5[120:126]))
    ephemeris_template5_25["Health_SV6"] = binary((subframe5[126:132]))
    ephemeris_template5_25["Health_SV7"] = binary((subframe5[132:138]))
    ephemeris_template5_25["Health_SV8"] = binary((subframe5[138:144]))
    ephemeris_template5_25["Health_SV9"] = binary((subframe5[150:156]))
    ephemeris_template5_25["Health_SV10"] = binary((subframe5[156:162]))
    ephemeris_template5_25["Health_SV11"] = binary((subframe5[162:168]))
    ephemeris_template5_25["Health_SV12"] = binary((subframe5[168:174]))
    ephemeris_template5_25["Health_SV13"] = binary((subframe5[180:186]))
    ephemeris_template5_25["Health_SV14"] = binary((subframe5[186:192]))
    ephemeris_template5_25["Health_SV15"] = binary((subframe5[192:198]))
    ephemeris_template5_25["Health_SV16"] = binary((subframe5[198:204]))
    ephemeris_template5_25["Health_SV17"] = binary((subframe5[210:216]))
    ephemeris_template5_25["Health_SV18"] = binary((subframe5[216:222]))
    ephemeris_template5_25["Health_SV19"] = binary((subframe5[222:228]))
    ephemeris_template5_25["Health_SV20"] = binary((subframe5[228:234]))
    ephemeris_template5_25["Health_SV21"] = binary((subframe5[240:246]))
    ephemeris_template5_25["Health_SV22"] = binary((subframe5[246:252]))
    ephemeris_template5_25["Health_SV23"] = binary((subframe5[252:258]))
    ephemeris_template5_25["Health_SV24"] = binary((subframe5[258:264]))
    ephemeris_template5_25['sub5_Res1'] = binary(subframe5[271:277])
    ephemeris_template5_25['sub5_Res2'] = binary(subframe5[277:292])
    return ephemeris_template5_25


def return_almanac_sequence(filename):
    Keysight_data = pd.read_csv(filename)
    BladeRF_data = Keysight_data.to_dict(orient="record")

    Parsed_data = {}  # copy.deepcopy(constdict.subframes)
    # Create a structure of 32 with in satellites each with 5 subframes

    for record in BladeRF_data:
        tempr = {}
        tempr["Bits"] = ""
        for i in range(1, 38):
            # print(i)
            tempr["Bits"] += format(record['Raw Navbits – ' + str(i)], '08b')
        tempr['TOWC'] = record['TOWC (corresponding to start of the sub-frame) (s)']
        tempr["WN"] = record['Week No (corresponding to start of the sub-frame)']
        tempr["PRN"] = record["PRN"]

        tempr['subframe id'] = record['subframe id']
        # print(tempr["PRN"],tempr['subframe id'])

        if tempr["PRN"] in Parsed_data:
            if tempr['subframe id'] in Parsed_data[tempr["PRN"]]:
                pass
            else:
                Parsed_data[tempr["PRN"]][tempr["subframe id"]] = tempr
        else:
            if tempr['subframe id'] == 1:
                Parsed_data[tempr["PRN"]] = {}
                Parsed_data[tempr["PRN"]][tempr["subframe id"]] = tempr
    # print(Parsed_data)

    Valid_PRNs = list(Parsed_data.keys())
    print("Found PRNs - ", Valid_PRNs)
    ephemeris_list = []
    almanac_list_4 = []
    almanac_list_5 = []

    if len(Valid_PRNs) > 0:
        Valid_PRN = Valid_PRNs[1]
        ephemeris = copy.deepcopy(constdict.ephemeris)
        ephemeris4_25 = []
        ephemeris5_25 = []
        for record in BladeRF_data:

            tempr = {}
            tempr["Bits"] = "10001011"
            for i in range(1, 38):
                tempr["Bits"] += format(record['Raw Navbits – ' + str(i)], '08b')
            tempr['TOWC'] = record['TOWC (corresponding to start of the sub-frame) (s)']
            tempr["WN"] = record['Week No (corresponding to start of the sub-frame)']
            tempr["PRN"] = record["PRN"]
            tempr['subframe id'] = record['subframe id']

            if tempr['subframe id'] == 4 and tempr["PRN"] == Valid_PRN:
                pid = binary(tempr["Bits"][62:68])
                temp = {}
                if pid in constants.sub4mapping_SV_map.keys():
                    actual_pid = constants.sub4mapping_actual_SV_map[pid]
                    temp["Comment"] = "Almanac for SV - " + str(actual_pid)
                    ephemeris_template = subframe_4_parser_SV(tempr["Bits"])
                    ephemeris_template["PRN"] = actual_pid
                    ephemeris_template["page id"] = pid
                    ephemeris[actual_pid] = ephemeris_template


                elif pid in constants.sub4mapping_25_map.keys():
                    actual_pid = constants.sub4mapping_25_map[pid]
                    temp["Comment"] = "Sub 25 - All SV Health"
                    print("4_25", tempr["Bits"])
                    ephemeris_template = subframe_4_parser_25(tempr["Bits"])
                    ephemeris4_25.append(ephemeris_template)


                else:
                    actual_pid = "NA"
                    temp["Comment"] = tempr["Bits"]

                # print(ephemeris)
                temp["pid"] = pid
                temp["actual_pid"] = actual_pid
                temp["TOWC"] = tempr['TOWC']
                almanac_list_4.append(temp)

            if tempr['subframe id'] == 5 and tempr["PRN"] == Valid_PRN:

                pid = binary(tempr["Bits"][62:68])
                temp = {}
                if pid in constants.sub5mapping_SV_map.keys():
                    actual_pid = constants.sub5mapping_SV_map[pid]
                    temp["Comment"] = "Almanac for SV - " + str(actual_pid)
                    ephemeris_template = subframe_5_parser_SV(tempr["Bits"])
                    ephemeris_template["PRN"] = actual_pid
                    ephemeris_template["page id"] = pid
                    ephemeris[actual_pid] = ephemeris_template


                elif pid in constants.sub5mapping_25_map.keys():
                    actual_pid = constants.sub5mapping_25_map[pid]
                    temp["Comment"] = "Sub 25 - All SV Health"
                    print("5_25", tempr["Bits"])
                    ephemeris_template = subframe_5_parser_25(tempr["Bits"])
                    ephemeris5_25.append(ephemeris_template)

                else:
                    actual_pid = "NA"
                    temp["Comment"] = tempr["Bits"]

                temp["pid"] = pid
                temp["actual_pid"] = actual_pid
                temp["TOWC"] = tempr['TOWC']

                almanac_list_5.append(temp)
                ephemeris_list.append(ephemeris)
        return almanac_list_4, almanac_list_5, ephemeris_list, ephemeris4_25, ephemeris5_25

def irnss_get_almanac_from_raw_navbits_new(filename):
    alm_list=[]
    RNBB_data = pd.read_csv(filename)
    irnss_data = RNBB_data.to_dict(orient="record")

    Parsed_data =  {  }#copy.deepcopy(constdict.subframes)
    # Create a structure of 32 with in satellites each with 5 subframes

    for record in irnss_data:
        tempr = {}
        tempr["Bits"] = ""
        for i in range(1, 38):
            tempr["Bits"] += format(record['Raw Navbits – ' + str(i)], '08b')
        #tempr['TOWC'] = record['TOWC (corresponding to start of the sub-frame) (s)']
        tempr["WN"] = record['Week No (corresponding to start of the sub-frame)']
        tempr["PRN"] = record["PRN"]

        tempr['subframe id'] = record['subframe id']
        #print(tempr["PRN"],tempr['subframe id'])

        if tempr["PRN"] in Parsed_data :
            if tempr['subframe id'] in Parsed_data[tempr["PRN"]]:
                pass
            else :
                Parsed_data[tempr["PRN"]][tempr["subframe id"]] = tempr
        else :
            if tempr['subframe id'] == 1 :
                Parsed_data[tempr["PRN"]] = {}
                Parsed_data[ tempr["PRN"] ][ tempr["subframe id"] ] = tempr
    #print(Parsed_data)

    Valid_PRNs = list(Parsed_data.keys())
    print("Found PRNs - " , Valid_PRNs)

    ephemeris = copy.deepcopy(constdict.irnss_ephemeris)
    for Valid_PRN in Valid_PRNs :
        ephemeris_template = copy.deepcopy(constdict.ephemeris_template)
        ephemeris_template["PRN"] = Valid_PRN
        #ephemeris_template["msg id"] = "-"
        for subframe in range(1,3):
            nav_bits = Parsed_data[Valid_PRN][subframe]["Bits"]

            if subframe == 1 :
                ephemeris_template["TLM"] = (nav_bits[0:8])
                ephemeris_template["TOWC"] = binary(nav_bits[8:25]) *12
                ephemeris_template["Alert"] = (nav_bits[25:26])
                ephemeris_template["Autonav"] = (nav_bits[26:27])
                ephemeris_template["Spare"] = (nav_bits[29:30])
                #ephemeris_template["Subframe id"] = (nav_bits[27:29])

                ephemeris_template["Week Number"] = binary(nav_bits[30:40])
                ephemeris_template["a_f0"] = twos_complement(nav_bits[40:62]) * constants.POW2_M31 #Clk_bias
                ephemeris_template["a_f1"] = twos_complement(nav_bits[62:78]) * constants.POW2_M43 #Clk_drift
                ephemeris_template["a_f2"] = twos_complement(nav_bits[78:86]) * constants.POW2_M55 #Clk_drift_rate
                ephemeris_template["Health"] = binary(nav_bits[86:90]) #SV_accuracy
                ephemeris_template["t_oc"] = binary(nav_bits[90:106]) * 16  # Time_of_clk
                ephemeris_template["T_GD"] = twos_complement(nav_bits[106:114]) * constants.POW2_M31  # Ttl_Group_delay
                ephemeris_template["Deltan"] = twos_complement(nav_bits[114:136]) *constants.POW2_M41*constants.pi  # Mean_motion_delay
                ephemeris_template["IODC"] = binary(nav_bits[136:144]) #Issue_of_data_Ephemaries_Clk
                ephemeris_template["Reserved 1"] = binary(nav_bits[144:154])
                ephemeris_template["L5_flag"] = binary(nav_bits[154:155])
                ephemeris_template["S_flag"] = binary(nav_bits[155:156])
                ephemeris_template["c_uc"] = twos_complement(nav_bits[156:171]) * constants.POW2_M28  # Ampl_cosHarmonic_Arg_latitute
                ephemeris_template["c_us"] = twos_complement(nav_bits[171:186]) * constants.POW2_M28    # Ampl_sinHarmonic_Arg_latitute
                ephemeris_template["c_ic"] = twos_complement(nav_bits[186:201]) * constants.POW2_M28   # Ampl_cosHarmonic_angle_latitute
                ephemeris_template["c_is"] = twos_complement(nav_bits[201:216])*constants.POW2_M28    # Ampl_sinHarmonic_angle_latitute
                ephemeris_template["c_rc"] = twos_complement(nav_bits[216:231]) * constants.POW2_M4  # Ampl_cosHarmonic_orbit_radius
                ephemeris_template["c_rs"] = twos_complement(nav_bits[231:246]) * constants.POW2_M4  # Ampl_sinHarmonic_orbit_radius
                ephemeris_template["iDot"] = twos_complement(nav_bits[246:260]) * constants.POW2_M43*constants.pi  # Rate_inclination_angle
                ephemeris_template["Spare_data"] = binary(nav_bits[260:262])
                #ephemeris_template["CRC"] = binary(nav_bits[262:286])
                ephemeris_template["tail"] = binary(nav_bits[286:292])

            elif  subframe == 2 :
                ephemeris_template["TLM"] = (nav_bits[0:8])
                ephemeris_template["TOWC"] = binary(nav_bits[8:25]) * 12
                ephemeris_template["Alert"] = (nav_bits[25:26])
                ephemeris_template["Autonav"] = (nav_bits[26:27])
                ephemeris_template["Spare"] = (nav_bits[29:30])
                #ephemeris_template["Subframe id"] = (nav_bits[27:29])
                ephemeris_template["M_0"] = twos_complement(nav_bits[30:62]) * constants.POW2_M31 * constants.pi  # Mean_anamoly
                ephemeris_template["t_oe"] = binary(nav_bits[62:78]) * 16  # Time_Ephemeris
                ephemeris_template["e"] = binary(nav_bits[78:110]) *  constants.POW2_M33  # Eccentricities
                ephemeris_template["sqrt_A"] = binary(nav_bits[110:142]) *  constants.POW2_M19 # SQRT_Semi_Major_axis
                ephemeris_template["Omega_0"] = twos_complement(nav_bits[142:174]) * constants.POW2_M31 #Long_ascending_node
                ephemeris_template["Omega"] = twos_complement(nav_bits[174:206]) * constants.POW2_M31 * constants.pi #Arg_Perigee
                ephemeris_template["omegaDot"] = twos_complement(nav_bits[206:228]) * constants.POW2_M41 * constants.pi #Rate_RAAN
                ephemeris_template["I_0"] = twos_complement(nav_bits[228:260]) * constants.POW2_M31 * constants.pi   # Inclination
                ephemeris_template["Spare_data"] = binary(nav_bits[260:262])

                #ephemeris_template["CRC"] = binary(nav_bits[262:286])
                ephemeris_template["tail"] = binary(nav_bits[286:292])
            else:
                pass
        alm_list.append(ephemeris_template)
        ephemeris[Valid_PRN] = ephemeris_template
    return ephemeris

def irnss_get_almanac_from_generated_navbits_new(Parsed_data):
    alm_list = []
    Valid_PRNs = [2, 3, 4, 5, 6, 9]
    '''  list(Parsed_data.keys())
    print("Found PRNs - " , Valid_PRNs)'''

    ephemeris = copy.deepcopy(constdict.irnss_ephemeris)
    for Valid_PRN in Valid_PRNs :
        ephemeris_template = copy.deepcopy(constdict.ephemeris_template)
        ephemeris_template["PRN"] = Valid_PRN
        #ephemeris_template["msg id"] = "-"
        for subframe in range(1,3):
            nav_bits = Parsed_data[Valid_PRN][subframe][0:262]

            if subframe == 1 :
                ephemeris_template["TLM"] = (nav_bits[0:8])
                ephemeris_template["TOWC"] = binary(nav_bits[8:25]) *12
                ephemeris_template["Alert"] = (nav_bits[25:26])
                ephemeris_template["Autonav"] = (nav_bits[26:27])
                ephemeris_template["Spare"] = (nav_bits[29:30])
                ephemeris_template["Week Number"] = binary(nav_bits[30:40])
                ephemeris_template["a_f0"] = twos_complement(nav_bits[40:62]) * constants.POW2_M31 #Clk_bias
                ephemeris_template["a_f1"] = twos_complement(nav_bits[62:78]) * constants.POW2_M43 #Clk_drift
                ephemeris_template["a_f2"] = twos_complement(nav_bits[78:86]) * constants.POW2_M55 #Clk_drift_rate
                ephemeris_template["Health"] = binary(nav_bits[86:90]) #SV_accuracy
                ephemeris_template["t_oc"] = binary(nav_bits[90:106]) * 16  # Time_of_clk
                ephemeris_template["T_GD"] = twos_complement(nav_bits[106:114]) * constants.POW2_M31  # Ttl_Group_delay
                ephemeris_template["Deltan"] = twos_complement(nav_bits[114:136]) *constants.POW2_M41*constants.pi  # Mean_motion_delay
                ephemeris_template["IODC"] = binary(nav_bits[136:144]) #Issue_of_data_Ephemaries_Clk
                ephemeris_template["Reserved 1"] = binary(nav_bits[144:154])
                ephemeris_template["L5_flag"] = binary(nav_bits[154:155])
                ephemeris_template["S_flag"] = binary(nav_bits[155:156])
                ephemeris_template["c_uc"] = twos_complement(nav_bits[156:171]) * constants.POW2_M28  # Ampl_cosHarmonic_Arg_latitute
                ephemeris_template["c_us"] = twos_complement(nav_bits[171:186]) * constants.POW2_M28    # Ampl_sinHarmonic_Arg_latitute
                ephemeris_template["c_ic"] = twos_complement(nav_bits[186:201]) * constants.POW2_M28   # Ampl_cosHarmonic_angle_latitute
                ephemeris_template["c_is"] = twos_complement(nav_bits[201:216]) * constants.POW2_M28    # Ampl_sinHarmonic_angle_latitute
                ephemeris_template["c_rc"] = twos_complement(nav_bits[216:231]) * constants.POW2_M4  # Ampl_cosHarmonic_orbit_radius
                ephemeris_template["c_rs"] = twos_complement(nav_bits[231:246]) * constants.POW2_M4  # Ampl_sinHarmonic_orbit_radius
                ephemeris_template["iDot"] = twos_complement(nav_bits[246:260]) * constants.POW2_M43*constants.pi  # Rate_inclination_angle
                ephemeris_template["Spare_data"] = binary(nav_bits[260:262])
                #ephemeris_template["CRC"] = binary(nav_bits[262:286])
                #ephemeris_template["tail"] = binary(nav_bits[286:292])

            elif  subframe == 2 :
                ephemeris_template["TLM"] = (nav_bits[0:8])
                ephemeris_template["TOWC"] = binary(nav_bits[8:25]) * 12
                ephemeris_template["Alert"] = (nav_bits[25:26])
                ephemeris_template["Autonav"] = (nav_bits[26:27])
                ephemeris_template["Spare"] = (nav_bits[29:30])

                ephemeris_template["M_0"] = twos_complement(nav_bits[30:62]) * constants.POW2_M31 * constants.pi  # Mean_anamoly
                ephemeris_template["t_oe"] = binary(nav_bits[62:78]) * 16  # Time_Ephemeris
                ephemeris_template["e"] = binary(nav_bits[78:110]) *  constants.POW2_M33  # Eccentricities
                ephemeris_template["sqrt_A"] = binary(nav_bits[110:142]) *  constants.POW2_M19 # SQRT_Semi_Major_axis
                ephemeris_template["Omega_0"] = twos_complement(nav_bits[142:174]) * constants.POW2_M31 #Long_ascending_node
                ephemeris_template["Omega"] = twos_complement(nav_bits[174:206]) * constants.POW2_M31 * constants.pi #Arg_Perigee
                ephemeris_template["omegaDot"] = twos_complement(nav_bits[206:228]) * constants.POW2_M41 * constants.pi #Rate_RAAN
                ephemeris_template["I_0"] = twos_complement(nav_bits[228:260]) * constants.POW2_M31 * constants.pi   # Inclination
                ephemeris_template["Spare_data"] = binary(nav_bits[260:262])

                #ephemeris_template["CRC"] = binary(nav_bits[262:286])
                #ephemeris_template["tail"] = binary(nav_bits[286:292])
            else:
                pass
        alm_list.append(ephemeris_template)
        ephemeris[Valid_PRN] = ephemeris_template
    return ephemeris


def read_alm_param(filename):
    RNBB_data = pd.read_csv(filename)
    gps_data = RNBB_data.to_dict(orient="record")
    my_dict = gps_data[0]
    final_rec=[]

    for rec in range(len(gps_data)):
        temp = {}
        for i in range(1,12):
            xyz=[]
            sat_param = copy.deepcopy(constdict.satellite_parameter_template)
            sat_param['TOWC']=gps_data[rec]['TOWC (s)']
            sat_param['SV']=gps_data[rec]['PRN-'+str(i)]
            sat_param['Pseudorange']=gps_data[rec]['PR-'+str(i)+' (m)']
            sat_param['Dopplers']=gps_data[rec]['Doppler-'+str(i)+" (Hz)"]
            xyz.append(gps_data[rec]['Satellite X Position-'+str(i)+" (m)"])
            xyz.append(gps_data[rec]['Satellite Y Position-' + str(i) + " (m)"])
            xyz.append(gps_data[rec]['Satellite Z Position-' + str(i) + " (m)"])
            sat_param['check_data']['pos']=xyz
            sat_param['Azimuth'] = gps_data[rec]['Azimuth-' + str(i) + " (deg)"]
            sat_param['Elevation'] = gps_data[rec]['Elevation-' + str(i) + " (deg)"]
            temp[gps_data[rec]['PRN-'+str(i)]]=sat_param
        final_rec.append(temp)
    return final_rec


def get_almanac_from_raw_navbits_compare_new(filename):
    alm_list = []
    Keysight_data = pd.read_csv(filename)
    BladeRF_data = Keysight_data.to_dict(orient="record")

    Parsed_data = {}  # copy.deepcopy(constdict.subframes)
    # Create a structure of 32 with in satellites each with 5 subframes

    for record in BladeRF_data:
        tempr = {}
        tempr["Bits"] = ""
        for i in range(1, 38):
            tempr["Bits"] += format(record['Raw Navbits – ' + str(i)], '08b')
        tempr['TOWC'] = record['TOWC (corresponding to start of the sub-frame) (s)']
        tempr["WN"] = record['Week No (corresponding to start of the sub-frame)']
        tempr["PRN"] = record["PRN"]

        tempr['subframe id'] = record['subframe id']
        # print(tempr["PRN"],tempr['subframe id'])

        if tempr["PRN"] in Parsed_data:
            if tempr['subframe id'] in Parsed_data[tempr["PRN"]]:
                pass
            else:
                Parsed_data[tempr["PRN"]][tempr["subframe id"]] = tempr
        else:
            if tempr['subframe id'] == 1:
                Parsed_data[tempr["PRN"]] = {}
                Parsed_data[tempr["PRN"]][tempr["subframe id"]] = tempr


    Valid_PRNs = list(Parsed_data.keys())
    print("Found PRNs - ", Valid_PRNs)
    tlist=[]
    ephemeris = copy.deepcopy(constdict.ephemeris)
    for Valid_PRN in Valid_PRNs:
        ephemeris_template = {}
        bits=""
        #ephemeris_template["PRN"] = Valid_PRN
        for subframe in range(1, 6):
            nav_bits = "10001011" + Parsed_data[Valid_PRN][subframe]["Bits"]
            #print(Valid_PRN,nav_bits[0:300])
            bits=bits+nav_bits[0:300]
        ephemeris_template[Valid_PRN]=bits
        print(ephemeris_template)






