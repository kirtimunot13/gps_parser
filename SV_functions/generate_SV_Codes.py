import pandas as pd
import numpy as np

def GPS_Code():
    filename = "SV_functions/GPS_ca_table.xlsx"
    SV_data=pd.read_excel(filename,header=None)
    svnp=(SV_data.to_numpy()).transpose()
    #print(np.shape(svnp))
    #print(svnp[0,:])
    svnp  = svnp[1:1024,:] # delete first row
    svnp  = np.transpose(svnp)

    #print(np.shape(svnp))

    return svnp

def GPS_Code_20():
    filename = "SV_functions/GPS_ca_table.xlsx"
    SV_data=pd.read_excel(filename,header=None)
    svnp=(SV_data.to_numpy()).transpose()
    #print(np.shape(svnp))
    #print(svnp[0,:])
    svnp  = svnp[1:1024,:] # delete first row
    svnp  = np.transpose(svnp)
    svnp_20 = np.zeros((32,20460))
    for i in range(32):
        temp = svnp[i]
        for j in range(19):
            temp = np.append(temp,svnp[i])
        svnp_20[i, : ] = temp
        svnp_20 = np.floor(svnp_20)
    return svnp_20

