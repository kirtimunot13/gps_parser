import copy
import numpy as np

no_of_satellites = 32
no_of_satellites_irnss = 9

ephemeris_template = {
    'TOWC': "-",  # to be claculated
    'PRN': "-",  # to be inserted as per prn no
    'subframe id': "-",  # subframe bit
    "page id": "-",  # only considered for sub 4 & 5,
    "msg id": "-",  # only for subfrme 3 and 4 for irnss
    "PRN_ALM": "-",
    "Inter_Sig_Corr": "-",
    'preamble': '10001011',  # TLM fixed 8 bits
    'TLM_MSG': '01010101010101',  # TLM msg 14 bits
    'TLM_Reserved': '01',  # TLM reserverd 2 bits
    'HOW_Reserved': '10001100101011111',  # reserved copy as it is keysight # 17 bits
    'Alert_Antispoof': 0.0,  # antispoof copy as it is keysight
    'HOW_Page_id': '-',  # generate subframe id
    'Parity_bit_solve': 0.0, "Alert": "-", "Autonav": "-", "Spare": "-", "L5_flag": "-", "S_flag": "-",
    "Spare_data": "-", "CRC": "-", "tail": "-",
    "Week Number": "-",  # to be inserted
    "Accuracy": 0.0,  # as it is keysight
    "Health": 0.0,  # as it is like keysight
    "T_GD": 0.0,  # as it is like keysight is always 0
    "IODC": 4,  # as it is like keysight
    "t_oc": "-",  # ceil(TOWC/7200)* 7200
    "a_f2": 0.0,  # as it is like keysight is always 0
    "a_f1": "-",  # to be inserted
    "a_f0": "-",  # to be inserted
    "Code Type": "00",  # 10 for CA Code
    "L2P Flag": 0,  # as it is like keysight is always 0
    "e": "-",  # to be inserted
    "Reserved 1": "01010101010101010101010",  # as it is like keysight
    "Reserved 2": "010101010101010101010101",  # as it is like keysight
    "Reserved 3": "010101010101010101010101",  # as it is like keysight
    "Reserved 4": "0101010101010101",  # as it is like keysight
    "IODE_sf2": "00000100",  # as it is like keysight
    "C_rs": 0.0,  # as it is like keysight 0
    "Deltan": 0.0,  # as it is like keysight 0
    "M_0": "-",  # to be inserted almnac -Mean Anom(rad)
    "M_K": "-",  # to be calculated
    "C_uc": 0.0,  # as it is like keysight 0
    "C_us": 0.0,  # as it is like keysight 0
    "sqrt_A": "-",  # to be inserted
    "t_oe": "-",  # ceil(TOWC/7200)* 7200
    "FIT INTERVAL": 0.0,  # as it is like keysight to be added in parser
    "AODO": "11111",  # as it is like keysight  all 11111
    "C_ic": 0.0,  # as it is like keysight 0
    "Omega_0": "-",  # to be inserted almnac-  Right Ascen at Week(rad)
    "C_is": 0.0,  # as it is like keysight 0
    "I_0": "-",  # to be inserted almnac-Orbital Inclination(rad)
    "C_rc": 0.0,  # as it is like keysight 0
    "Omega": "-",  # to be inserted almnac-Argument of Perigee(rad)
    "omegaDot": "-",  # to be inserted almnac-Rate of Right Ascen(r/s)
    "IODE_sf3": "00000100",  # as it is like keysight
    "iDot": 0.0,  # as it is like keysight 0
    "toa": "-",  # to be inserted almnac -Time of Applicability(s)
    "delta_i": 0.0,
}

ephemeris_template4_25 = {
    'TOWC': "-",  # to be claculated
    'PRN': "-",  # to be inserted as per prn no
    'subframe id': "-",  # subframe bit
    "page id": "-",  # only considered for sub 4 & 5
    'preamble': '10001011',  # TLM fixed 8 bits
    'TLM_MSG': '01010101010101',  # TLM msg 14 bits
    'TLM_Reserved': '01',  # TLM reserverd 2 bits
    'HOW_Reserved': '10001100101011111',  # reserved copy as it is keysight # 17 bits
    'Alert_Antispoof': 0.0,  # antispoof copy as it is keysight
    'HOW_Page_id': '-',  # generate subframe id
    'Parity_bit_solve': 0.0,
    'ALM4_A_SPOOF_SV1': '-', 'ALM4_A_SPOOF_SV2': '-', 'ALM4_A_SPOOF_SV3': '-', 'ALM4_A_SPOOF_SV4': '-',
    'ALM4_A_SPOOF_SV5': '-', 'ALM4_A_SPOOF_SV6': '-', 'ALM4_A_SPOOF_SV7': '-', 'ALM4_A_SPOOF_SV8': '-',
    'ALM4_A_SPOOF_SV9': '-', 'ALM4_A_SPOOF_SV10': '-', 'ALM4_A_SPOOF_SV11': '-', 'ALM4_A_SPOOF_SV12': '-',
    'ALM4_A_SPOOF_SV13': '-', 'ALM4_A_SPOOF_SV14': '-', 'ALM4_A_SPOOF_SV15': '-', 'ALM4_A_SPOOF_SV16': '-',
    'ALM4_A_SPOOF_SV17': '-', 'ALM4_A_SPOOF_SV18': '-', 'ALM4_A_SPOOF_SV19': '-', 'ALM4_A_SPOOF_SV20': '-',
    'ALM4_A_SPOOF_SV21': '-', 'ALM4_A_SPOOF_SV22': '-', 'ALM4_A_SPOOF_SV23': '-', 'ALM4_A_SPOOF_SV24': '-',
    'ALM4_A_SPOOF_SV25': '-', 'ALM4_A_SPOOF_SV26': '-', 'ALM4_A_SPOOF_SV27': '-', 'ALM4_A_SPOOF_SV28': '-',
    'ALM4_A_SPOOF_SV29': '-', 'ALM4_A_SPOOF_SV30': '-', 'ALM4_A_SPOOF_SV31': '-', 'ALM4_A_SPOOF_SV32': '-',
    'ALM4_sub4_Res1': "-",
    'ALM4_Health_SV25': '-', 'ALM4_Health_SV26': '-', 'ALM4_Health_SV27': '-', 'ALM4_Health_SV28': '-',
    'ALM4_Health_SV29': '-', 'ALM4_Health_SV30': '-', 'ALM4_Health_SV31': '-', 'ALM4_Health_SV32': '-',
    'ALM4_sub4_Res2': "-"}
ephemeris_template5_25 = {
    'TOWC': "-",  # to be claculated
    'PRN': "-",  # to be inserted as per prn no
    'subframe id': "-",  # subframe bit
    "page id": "-",  # only considered for sub 4 & 5
    'preamble': '10001011',  # TLM fixed 8 bits
    'TLM_MSG': '01010101010101',  # TLM msg 14 bits
    'TLM_Reserved': '01',  # TLM reserverd 2 bits
    'HOW_Reserved': '10001100101011111',  # reserved copy as it is keysight # 17 bits
    'Alert_Antispoof': 0.0,  # antispoof copy as it is keysight
    'HOW_Page_id': '-',  # generate subframe id
    'Parity_bit_solve': 0.0,
    "Week Number": "-",  # to be inserted
    "toa": "-",  # to be inserted almnac -Time of Applicability(s)
    "Health_SV1": "-", "Health_SV2": "-", "Health_SV3": "-", "Health_SV4": "-", "Health_SV5": "-", "Health_SV6": "-",
    "Health_SV7": "-",
    "Health_SV8": "-", "Health_SV9": "-", "Health_SV10": "-", "Health_SV11": "-", "Health_SV12": "-",
    "Health_SV13": "-", "Health_SV14": "-",
    "Health_SV15": "-", "Health_SV16": "-", "Health_SV17": "-", "Health_SV18": "-", "Health_SV19": "-",
    "Health_SV20": "-", "Health_SV21": "-",
    "Health_SV22": "-", "Health_SV23": "-", "Health_SV24": "-", 'sub5_Res1': '-', 'sub5_Res2': '-'
}

ephemeris = {
    1: {}, 2: {}, 3: {}, 4: {}, 5: {}, 6: {}, 7: {}, 8: {}, 9: {}, 10: {},
    11: {}, 12: {}, 13: {}, 14: {}, 15: {}, 16: {}, 17: {}, 18: {}, 19: {}, 20: {},
    21: {}, 22: {}, 23: {}, 24: {}, 25: {}, 26: {}, 27: {}, 28: {}, 29: {}, 30: {},
    31: {}, 32: {}
}
irnss_ephemeris = {
    1: {}, 2: {}, 3: {}, 4: {}, 5: {}, 6: {}, 7: {}, 8: {}, 9: {}}
almanac = {1: {}, 2: {}, 3: {}, 4: {}, 5: {}, 6: {}, 7: {}, 8: {}, 9: {}, 10: {},
           11: {}, 12: {}, 13: {}, 14: {}, 15: {}, 16: {}, 17: {}, 18: {}, 19: {}, 20: {},
           21: {}, 22: {}, 23: {}, 24: {}, 25: {}}

almanac_dummy_payload1_57 = "100010110101010101010101pppppp100011001011101100010001pppppp011110010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010100pppppp"
almanac_dummy_payload6_57 = "100010110101010101010101pppppp100011001011101100010001pppppp001110010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010100pppppp"
almanac_dummy_payload11_57 = "100010110101010101010101pppppp100011001011101100010001pppppp001110010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010100pppppp"
almanac_dummy_payload16_57 = "100010110101010101010101pppppp100011001011101100010001pppppp001110010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010100pppppp"
almanac_dummy_payload21_57 = "100010110101010101010101pppppp100011001011101100010001pppppp001110010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010100pppppp"
almanac_dummy_payload12_62 = "100010110101010101010101pppppp100011001011110110010000pppppp011111100101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010100pppppp"
almanac_dummy_payload19_58 = "100010110101010101010101pppppp100011001011110110010000pppppp011110100101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010100pppppp"
almanac_dummy_payload20_59 = "100010110101010101010101pppppp100011001011110110010000pppppp011110110101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010100pppppp"
almanac_dummy_payload22_60 = "100010110101010101010101pppppp100011001011110110010000pppppp011111000101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010100pppppp"
almanac_dummy_payload23_61 = "100010110101010101010101pppppp100011001011110110010000pppppp011111010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010100pppppp"
almanac_dummy_payload24_62 = "100010110101010101010101pppppp100011001011110110010000pppppp011111100101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010100pppppp"
almanac_dummy_payload18_56 = "100010110101010101010101pppppp100011001100110010010000pppppp011110000000000000000000pppppp000000000000000000000000pppppp000000000000000000000000pppppp000000000000000000000000pppppp000000000000000000000000pppppp0000000000000000pppppppppppppp000100010011101100000011pppppp000100010101010101010100pppppp"
almanac_dummy_payload14_53 = "100010110101010101010101pppppp100011001100001010010000pppppp011101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010100pppppp"
almanac_dummy_payload15_54 = "100010110101010101010101pppppp100011001100001010010000pppppp011101100101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010100pppppp"
almanac_dummy_payload17_55 = "100010110101010101010101pppppp100011001100001010010000pppppp011101110101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010100pppppp"
almanac_dummy_payload13_52 = "100010110101010101010101pppppp100011001100000000010010pppppp011101000000000000000000pppppp000000000000000000000000pppppp000000000000000000000000pppppp000000000000000000000000pppppp000000000000000000000000pppppp000000000000000000000000pppppp000000000000000000000000pppppp000000000000000000000000pppppp"
almanac_dummy_payload5_4_0 = "100010110101010101010101pppppp100011001110100010010100pppppp010000000101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010101pppppp010101010101010101010111pppppp"
almanac_dummy_payload5_14_0 = "100010110101010101010101pppppp100011001100001100010101pppppp010000000101001110011100pppppp011110110000101111011110pppppp111111010100110000000000pppppp101000010000110011101111pppppp001000010010101001110101pppppp101100001001110000011010pppppp111111111111111110001100pppppp11110100000000000000101pppppp"
almanac_dummy_payload4_25 = "100010110101010101010101pppppp100011001101111000010010pppppp011111110001000100010001pppppp000100010001000100010001pppppp000100010001000100010001110001000100010001000100010001001110000100010001000100010001pppppp000100010001000101000000pppppp000000000000000000000000pppppp00000000000000000001010pppppp"
almanac_dummy_payload5_25 = "100010110101010101010101pppppp100011001101111010010100pppppp011100110111101111111010pppppp000000000000000000111111pppppp000000000000000000000000101001000000000000000000000000010110000000000000000000000000pppppp000000000000000000000000pppppp000000000000000000000000pppppp01010101010101010101011pppppp"

almanac_dummy_payload5_4_0_new = "100010110101010101010101111111100011001110100010010100100100010000000101010101010101011001010101010101010101010101101001010101010101010101010101101001010101010101010101010101101001010101010101010101010101101001010101010101010101010101101001010101010101010101010101101001010101010101010101010111001100"
almanac_dummy_payload5_14_0_new = "100010110101010101010101111111100011001100001100010101001100010000000101001110011100101001011110110000101111011110010001111111010100110000000000011010101000010000110011101111010000001000010010101001110101111010101100001001110000011010000111111111111111111110001100001101111101000000000000001011111000"
almanac_dummy_payload18_56_new = "100010110101010101010101111111100011001100110010010000100000011110000000000000000000111100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000011111010010010000100010011101100000011101011000100010101010101010100010000"
almanac_dummy_payload4_25_new = "100010110101010101010101111111100011001101111000010010000100011111110001000100010001010101000100010001000100010001001110000100010001000100010001110001000100010001000100010001001110000100010001000100010001110001000100010001000101000000000011000000000000000000000000111111000000000000000000010100011000"
almanac_dummy_payload5_25_new = "100010110101010101010101111111100011001101111010010100001100011100110111101111111010010000000000000000000000111111011110000000000000000000000000101001000000000000000000000000010110000000000000000000000000101001000000000000000000000000010110000000000000000000000000101001010101010101010101010111001100"
# almanac_dummy_payload="dummy Payload"
almanac_subframes = {
    1: {4: copy.deepcopy(almanac_dummy_payload1_57), 5: ""},
    2: {4: "", 5: ""},
    3: {4: "", 5: ""},
    4: {4: "", 5: copy.deepcopy(almanac_dummy_payload5_4_0_new)},
    5: {4: "", 5: ""},
    6: {4: copy.deepcopy(almanac_dummy_payload6_57), 5: ""},
    7: {4: "", 5: ""},
    8: {4: "", 5: ""},
    9: {4: "", 5: ""},
    10: {4: "", 5: ""},
    11: {4: copy.deepcopy(almanac_dummy_payload11_57), 5: ""},
    12: {4: copy.deepcopy(almanac_dummy_payload12_62), 5: ""},
    13: {4: copy.deepcopy(almanac_dummy_payload13_52), 5: ""},
    14: {4: copy.deepcopy(almanac_dummy_payload14_53), 5: copy.deepcopy(almanac_dummy_payload5_14_0_new)},
    15: {4: copy.deepcopy(almanac_dummy_payload15_54), 5: ""},
    16: {4: copy.deepcopy(almanac_dummy_payload16_57), 5: ""},
    17: {4: copy.deepcopy(almanac_dummy_payload17_55), 5: ""},
    18: {4: copy.deepcopy(almanac_dummy_payload18_56_new), 5: ""},
    19: {4: copy.deepcopy(almanac_dummy_payload19_58), 5: ""},
    20: {4: copy.deepcopy(almanac_dummy_payload20_59), 5: ""},
    21: {4: copy.deepcopy(almanac_dummy_payload21_57), 5: ""},
    22: {4: copy.deepcopy(almanac_dummy_payload22_60), 5: ""},
    23: {4: copy.deepcopy(almanac_dummy_payload23_61), 5: ""},
    24: {4: copy.deepcopy(almanac_dummy_payload24_62), 5: ""},
    25: {4: copy.deepcopy(almanac_dummy_payload4_25_new), 5: copy.deepcopy(almanac_dummy_payload5_25_new)}

}

irnss_dummy_payload3_2 = "10001011000101000001110110010000010100000001100011110000011111111000010000111100001000011110100100011011010010010011101000011011110100001110011000000111101100000100010101100010100110110001011101100000110011110000011010111000001101111101000111001000000000000000100100111001000010101111000000000000"
irnss_dummy_payload3_3 = "10001011000101000001110110010001101011111110101101010000000000100000000000010010000101010010101000010100011110001001011100010010000000111101100100000110111110111011000101010010101000010100010100000000000000000000000000000000000000000000000000000000000000000000110010100010000011100011110000000000"
irnss_dummy_payload3_4 = "10001011000101000001110110010001101011111110101101010000000000100000000000010010000101010010101000010100011110001001011100010010111111111110010000000000111100011000000101010010101000010100010000000000000000000000000000000000000000000000000000000000000000000001000011000111010010011011110000000000"
irnss_dummy_payload3_5 = "10001011000101000001110110010000010100000001100000101100010100010110001100001100000111000110100100000011010010001001101001000100110100100100011010010000001101000111101110100100100011010010001001101001000000110100100010111000010001001100001000111000000000000001010101100011100101101110000000000000"
irnss_dummy_payload3_6 = "10001011000101000001110110010000010100000001100000101100010100010110001100001100000111000110100100000011010010001001101001000100110100100100011010010000001101000111101110100100100011010010001001101001000000110100100010111000010001001100001000111000000000000001100101010001111100010011100000000000"
irnss_dummy_payload3_9 = "10001011000101000001110110010000010100000001100010110100011100111010001101111100001000000101100100011110110010010001100001000101110000100011111010010010011101001001011110100100110011010001010111101000101111110000011010010110001110001011000111011000000000000010010001110011100001110100100000000000"

irnss_almanac_subframes = {1: {3: "", 4: ""}, 2: {3: copy.deepcopy(irnss_dummy_payload3_2), 4: ""},
                           3: {3: copy.deepcopy(irnss_dummy_payload3_3), 4: ""}, 4: {3: "", 4: ""}, 5: {3: "", 4: ""},
                           6: {3: "", 4: ""}, 7: {3: "", 4: ""}, 9: {3: "", 4: ""}}
subframes = {
    1: {1: "", 2: "", 3: "", }, 2: {1: "", 2: "", 3: "", }, 3: {1: "", 2: "", 3: "", }, 4: {1: "", 2: "", 3: "", },
    5: {1: "", 2: "", 3: "", }, 6: {1: "", 2: "", 3: "", }, 7: {1: "", 2: "", 3: "", }, 8: {1: "", 2: "", 3: "", },
    9: {1: "", 2: "", 3: "", }, 10: {1: "", 2: "", 3: "", }, 11: {1: "", 2: "", 3: "", }, 12: {1: "", 2: "", 3: "", },
    13: {1: "", 2: "", 3: "", }, 14: {1: "", 2: "", 3: "", }, 15: {1: "", 2: "", 3: "", }, 16: {1: "", 2: "", 3: "", },
    17: {1: "", 2: "", 3: "", }, 18: {1: "", 2: "", 3: "", }, 19: {1: "", 2: "", 3: "", }, 20: {1: "", 2: "", 3: "", },
    21: {1: "", 2: "", 3: "", }, 22: {1: "", 2: "", 3: "", }, 23: {1: "", 2: "", 3: "", }, 24: {1: "", 2: "", 3: "", },
    25: {1: "", 2: "", 3: "", }, 26: {1: "", 2: "", 3: "", }, 27: {1: "", 2: "", 3: "", }, 28: {1: "", 2: "", 3: "", },
    29: {1: "", 2: "", 3: "", }, 30: {1: "", 2: "", 3: "", }, 31: {1: "", 2: "", 3: "", }, 32: {1: "", 2: "", 3: "", }

}
irnss_subframes = {1: {1: "", 2: "", }, 2: {1: "", 2: "", }, 3: {1: "", 2: "", }, 4: {1: "", 2: "", },
                   5: {1: "", 2: "", }, 6: {1: "", 2: "", }, 7: {1: "", 2: "", }, 8: {1: "", 2: "", },
                   9: {1: "", 2: "", }, }

satellite_parameter_template = {
    "SV": 0,
    "TOWC": 0,
    "Pseudorange": 0,
    "coderate": 0,
    "Dopplers": 0,
    "Azimuth": 0,
    "Elevation": 0,
    "Code_phase": 0,
    "Carr_phase": 0,
    "Nav_Bit": 0,
    "Carr_Phase_Step": 0,
    "Code_Phase_Step": 0,
    "gain": 0,
    "check_data" : { "pos" : 0 , "vel" : 0, "clk":0},
    "c/n" : 0,
}

play_parameters = {
    "Valid PRN": [],
    "time_parameters": {},
    "page ID": 1,
    "previous word": np.zeros((32, 30)),
    "satellite parameters": {},
    "Nav Message": np.zeros((32, 1530)),
    "main_ephemeris" : {},
    "main_alamnac_45" : {},
}

alm_struct = {
    'TOWC': "-",  # to be claculated
    'PRN': "-",  # to be inserted as per prn no
    'subframe id': "-",  # subframe bit
    "page id": "-",  # only considered for sub 4 & 5
    'preamble': '-',  # TLM fixed 8 bits
    'TLM_MSG': '-',
    'TLM_Reserved': '-',  # TLM reserverd 17 bits
    'HOW_Reserved': '-',  # reserved copy as it is
    'Alert_Antispoof': '-',  # antispoof copy as it is
    'HOW_Page_id': '-',  # generate subframe id
    'Parity_bit_solve': '-',
    "Week Number": "-",  # to be inserted
    "Accuracy": "-",  # as it is keysight
    "Health": "-",  # as it is like keysight
    "T_GD": "-",  # as it is like keysight is always 0
    "IODC": 122,  # as it is like keysight
    "t_oc": "-",  # ceil(TOWC/7200)* 7200
    "a_f2": "-",  # as it is like keysight is always 0
    "a_f1": "-",  # to be inserted
    "a_f0": "-",  # to be inserted
    "toa": "-",  # to be inserted almnac -Time of Applicability(s)
    "Code Type": "-",  # 10 for CA Code
    "L2P Flag": "-",  # as it is like keysight is always 0
    "e": "-",  # to be inserted
    "Reserved 1": "-",  # as it is like keysight
    "Reserved 2": "-",  # as it is like keysight
    "Reserved 3": "-",  # as it is like keysight
    "Reserved 4": "-",  # as it is like keysight
    "IODE_sf2": "-",  # as it is like keysight
    "C_rs": "-",  # as it is like keysight 0
    "Deltan": "-",  # as it is like keysight 0
    "M_0": "-",  # to be inserted almnac -Mean Anom(rad)
    "C_uc": "-",  # as it is like keysight 0
    "C_us": "-",  # as it is like keysight 0
    "sqrt_A": "-",  # to be inserted
    "t_oe": "-",  # ceil(TOWC/7200)* 7200
    "FIT INTERVAL": "-",  # as it is like keysight to be added in parser
    "AODO": "-",  # as it is like keysight  all 11111
    "C_ic": "-",  # as it is like keysight 0
    "Omega_0": "-",  # to be inserted almnac-  Right Ascen at Week(rad)
    "C_is": "-",  # as it is like keysight 0
    "I_0": "-",  # to be inserted almnac-Orbital Inclination(rad)
    "C_rc": "-",  # as it is like keysight 0
    "Omega": "-",  # to be inserted almnac-Argument of Perigee(rad)
    "omegaDot": "-",  # to be inserted almnac-Rate of Right Ascen(r/s)
    "IODE_sf3": "-",  # as it is like keysight
    "iDot": "-",  # as it is like keysight 0
    "ALM4_e": "-",
    "ALM4_toa": "-", "ALM4_Delta_I": "-", "ALM4_omegaDot": "-", "ALM4_Sqaure Root A": "-", "ALM4_Omega_0": "-",
    "ALM4_Omega": "-", "ALM4_M_0": "-", "ALM4_a_f0": "-", "ALM4_a_f1": "-", "ALM4_SV HEALTH_4": "-",
    'ALM4_A_SPOOF_SV1': '-', 'ALM4_A_SPOOF_SV2': '-', 'ALM4_A_SPOOF_SV3': '-', 'ALM4_A_SPOOF_SV4': '-',
    'ALM4_A_SPOOF_SV5': '-', 'ALM4_A_SPOOF_SV6': '-', 'ALM4_A_SPOOF_SV7': '-', 'ALM4_A_SPOOF_SV8': '-',
    'ALM4_A_SPOOF_SV9': '-', 'ALM4_A_SPOOF_SV10': '-', 'ALM4_A_SPOOF_SV11': '-', 'ALM4_A_SPOOF_SV12': '-',
    'ALM4_A_SPOOF_SV13': '-', 'ALM4_A_SPOOF_SV14': '-', 'ALM4_A_SPOOF_SV15': '-', 'ALM4_A_SPOOF_SV16': '-',
    'ALM4_A_SPOOF_SV17': '-', 'ALM4_A_SPOOF_SV18': '-', 'ALM4_A_SPOOF_SV19': '-', 'ALM4_A_SPOOF_SV20': '-',
    'ALM4_A_SPOOF_SV21': '-', 'ALM4_A_SPOOF_SV22': '-', 'ALM4_A_SPOOF_SV23': '-', 'ALM4_A_SPOOF_SV24': '-',
    'ALM4_A_SPOOF_SV25': '-', 'ALM4_A_SPOOF_SV26': '-', 'ALM4_A_SPOOF_SV27': '-', 'ALM4_A_SPOOF_SV28': '-',
    'ALM4_A_SPOOF_SV29': '-', 'ALM4_A_SPOOF_SV30': '-', 'ALM4_A_SPOOF_SV31': '-', 'ALM4_A_SPOOF_SV32': '-',
    'ALM4_sub4_Res1': "-",
    'ALM4_Health_SV25': '-', 'ALM4_Health_SV26': '-', 'ALM4_Health_SV27': '-', 'ALM4_Health_SV28': '-',
    'ALM4_Health_SV29': '-', 'ALM4_Health_SV30': '-', 'ALM4_Health_SV31': '-', 'ALM4_Health_SV32': '-',
    'ALM4_sub4_Res2': "-",
    'Data_id': '-', 'SV(PAGE)_id': '-',
    "ALM5_e": "-", "ALM5_toa": "-", "ALM5_Delta_I": "-", "ALM5_omegaDot": "-", "ALM5_Sqaure Root A": "-",
    "ALM5_Omega_0": "-", "ALM5_Omega": "-", "ALM5_M_0": "-", "ALM5_a_f0": "-", "ALM5_a_f1": "-",
    "ALM5_SV HEALTH_5": "-",
    'ALM5_Week Number': '-', "ALM5P25_toa": "-",
    'ALM5_Health_SV1': '-', 'ALM5_Health_SV2': '-', 'ALM5_Health_SV3': '-', 'ALM5_Health_SV4': '-',
    'ALM5_Health_SV5': '-', 'ALM5_Health_SV6': '-', 'ALM5_Health_SV7': '-', 'ALM5_Health_SV8': '-',
    'ALM5_Health_SV9': '-', 'ALM5_Health_SV10': '-', 'ALM5_Health_SV11': '-', 'ALM5_Health_SV12': '-',
    'ALM5_Health_SV13': '-', 'ALM5_Health_SV14': '-', 'ALM5_Health_SV15': '-', 'ALM5_Health_SV16': '-',
    'ALM5_Health_SV17': '-', 'ALM5_Health_SV18': '-', 'ALM5_Health_SV19': '-', 'ALM5_Health_SV20': '-',
    'ALM5_Health_SV21': '-', 'ALM5_Health_SV22': '-', 'ALM5_Health_SV23': '-', 'ALM5_Health_SV24': '-',
    'ALM5_sub5_Res1': "-", 'ALM5_sub5_Res2': "-"
}
dstructActual = {
    'TOWC': "Actual Value", 'PRN': "-", 'subframe id': "-", "page id": "-", 'preamble': '-',  # TLM fixed 8 bits
    'TLM_MSG': '-',
    'TLM_Reserved': '-',  # TLM reserverd 17 bits
    'HOW_Reserved': '-',  # reserved copy as it is
    'Alert_Antispoof': '-',  # antispoof copy as it is
    'HOW_Page_id': '-',  # generate subframe id
    "Parity_bit_solve": '-',
    "Week Number": "-", "Accuracy": "-", "Health": "-", "T_GD": "-", "IODC": "-", "t_oc": "-", "a_f2": "-", "a_f1": "-",
    "a_f0": "-", "toa": "-",  # to be inserted almnac -Time of Applicability(s)
    "Code Type": "-", "L2P Flag": "-", "Reserved 1": "-", "Reserved 2": "-", "Reserved 3": "-", "Reserved 4": "-",
    "IODE_sf2": "-", "C_rs": "-", "Deltan": "-", "M_0": "-", "C_uc": "-", "e": "-", "C_us": "-", "sqrt_A": "-",
    "t_oe": "-", "FIT INTERVAL": "-", "AODO": "-",
    "C_ic": "-", "Omega_0": "-", "C_is": "-", "I_0": "-", "C_rc": "-", "Omega": "-", "omegaDot": "-", "IODE_sf3": "-",
    "iDot": "-",
    "ALM4_e": "-", "ALM4_toa": "-", "ALM4_Delta_I": "-", "ALM4_omegaDot": "-", "ALM4_Sqaure Root A": "-",
    "ALM4_Omega_0": "-", "ALM4_Omega": "-", "ALM4_M_0": "-", "ALM4_a_f0": "-", "ALM4_a_f1": "-",
    "ALM4_SV HEALTH_4": "-",
    'ALM4_A_SPOOF_SV1': '-', 'ALM4_A_SPOOF_SV2': '-', 'ALM4_A_SPOOF_SV3': '-', 'ALM4_A_SPOOF_SV4': '-',
    'ALM4_A_SPOOF_SV5': '-', 'ALM4_A_SPOOF_SV6': '-', 'ALM4_A_SPOOF_SV7': '-', 'ALM4_A_SPOOF_SV8': '-',
    'ALM4_A_SPOOF_SV9': '-', 'ALM4_A_SPOOF_SV10': '-', 'ALM4_A_SPOOF_SV11': '-', 'ALM4_A_SPOOF_SV12': '-',
    'ALM4_A_SPOOF_SV13': '-', 'ALM4_A_SPOOF_SV14': '-', 'ALM4_A_SPOOF_SV15': '-', 'ALM4_A_SPOOF_SV16': '-',
    'ALM4_A_SPOOF_SV17': '-', 'ALM4_A_SPOOF_SV18': '-', 'ALM4_A_SPOOF_SV19': '-', 'ALM4_A_SPOOF_SV20': '-',
    'ALM4_A_SPOOF_SV21': '-', 'ALM4_A_SPOOF_SV22': '-', 'ALM4_A_SPOOF_SV23': '-', 'ALM4_A_SPOOF_SV24': '-',
    'ALM4_A_SPOOF_SV25': '-', 'ALM4_A_SPOOF_SV26': '-', 'ALM4_A_SPOOF_SV27': '-', 'ALM4_A_SPOOF_SV28': '-',
    'ALM4_A_SPOOF_SV29': '-', 'ALM4_A_SPOOF_SV30': '-', 'ALM4_A_SPOOF_SV31': '-', 'ALM4_A_SPOOF_SV32': '-',
    'ALM4_sub4_Res1': "-",
    'ALM4_Health_SV25': '-', 'ALM4_Health_SV26': '-', 'ALM4_Health_SV27': '-', 'ALM4_Health_SV28': '-',
    'ALM4_Health_SV29': '-', 'ALM4_Health_SV30': '-', 'ALM4_Health_SV31': '-', 'ALM4_Health_SV32': '-',
    "ALM5_e": "-", "ALM5_toa": "-", "ALM5_Delta_I": "-", "ALM5_omegaDot": "-", "ALM5_Sqaure Root A": "-",
    "ALM5_Omega_0": "-", "ALM5_Omega": "-", "ALM5_M_0": "-", "ALM5_a_f0": "-", "ALM5_a_f1": "-",
    "ALM5_SV HEALTH_5": "-",
    'ALM4_sub4_Res2': "-",
    'Data_id': '-', 'SV(PAGE)_id': '-',
    'ALM5_Week Number': '-', "ALM5P25_toa": "-",
    'ALM5_Health_SV1': '-', 'ALM5_Health_SV2': '-', 'ALM5_Health_SV3': '-', 'ALM5_Health_SV4': '-',
    'ALM5_Health_SV5': '-', 'ALM5_Health_SV6': '-', 'ALM5_Health_SV7': '-', 'ALM5_Health_SV8': '-',
    'ALM5_Health_SV9': '-', 'ALM5_Health_SV10': '-', 'ALM5_Health_SV11': '-', 'ALM5_Health_SV12': '-',
    'ALM5_Health_SV13': '-', 'ALM5_Health_SV14': '-', 'ALM5_Health_SV15': '-', 'ALM5_Health_SV16': '-',
    'ALM5_Health_SV17': '-', 'ALM5_Health_SV18': '-', 'ALM5_Health_SV19': '-', 'ALM5_Health_SV20': '-',
    'ALM5_Health_SV21': '-', 'ALM5_Health_SV22': '-', 'ALM5_Health_SV23': '-', 'ALM5_Health_SV24': '-',
    'ALM5_sub5_Res1': "-", 'ALM5_sub5_Res2': "-"
}
dstruct = {
    'TOWC': "-", 'PRN': "-", 'subframe id': "-", "page id": "-",
    'preamble': '-',  # TLM fixed 8 bits
    'TLM_MSG': '-',
    'TLM_Reserved': '-',  # TLM reserverd 17 bits
    'HOW_Reserved': '-',  # reserved copy as it is
    'Alert_Antispoof': '-',  # antispoof copy as it is
    'HOW_Page_id': '-',  # generate subframe id
    'Parity_bit_solve': '-',
    "Week Number": "-", "Accuracy": "-", "Health": "-", "T_GD": "-", "IODC": "-", "t_oc": "-", "a_f2": "-", "a_f1": "-",
    "a_f0": "-", "toa": "-",  # to be inserted almnac -Time of Applicability(s)
    "Code Type": "-", "L2P Flag": "-", "Reserved 1": "-", "Reserved 2": "-", "Reserved 3": "-", "Reserved 4": "-",
    "IODE_sf2": "-", "C_rs": "-", "Deltan": "-", "M_0": "-", "C_uc": "-", "e": "-", "C_us": "-", "sqrt_A": "-",
    "t_oe": "-", "FIT INTERVAL": "-", "AODO": "-",
    "C_ic": "-", "Omega_0": "-", "C_is": "-", "I_0": "-", "C_rc": "-", "Omega": "-", "omegaDot": "-", "IODE_sf3": "-",
    "iDot": "-",
    "ALM4_e": "-", "ALM4_toa": "-", "ALM4_Delta_I": "-", "ALM4_omegaDot": "-", "ALM4_Sqaure Root A": "-",
    "ALM4_Omega_0": "-", "ALM4_Omega": "-", "ALM4_M_0": "-", "ALM4_a_f0": "-", "ALM4_a_f1": "-",
    "ALM4_SV HEALTH_4": "-",
    'ALM4_A_SPOOF_SV1': '-', 'ALM4_A_SPOOF_SV2': '-', 'ALM4_A_SPOOF_SV3': '-', 'ALM4_A_SPOOF_SV4': '-',
    'ALM4_A_SPOOF_SV5': '-', 'ALM4_A_SPOOF_SV6': '-', 'ALM4_A_SPOOF_SV7': '-', 'ALM4_A_SPOOF_SV8': '-',
    'ALM4_A_SPOOF_SV9': '-', 'ALM4_A_SPOOF_SV10': '-', 'ALM4_A_SPOOF_SV11': '-', 'ALM4_A_SPOOF_SV12': '-',
    'ALM4_A_SPOOF_SV13': '-', 'ALM4_A_SPOOF_SV14': '-', 'ALM4_A_SPOOF_SV15': '-', 'ALM4_A_SPOOF_SV16': '-',
    'ALM4_A_SPOOF_SV17': '-', 'ALM4_A_SPOOF_SV18': '-', 'ALM4_A_SPOOF_SV19': '-', 'ALM4_A_SPOOF_SV20': '-',
    'ALM4_A_SPOOF_SV21': '-', 'ALM4_A_SPOOF_SV22': '-', 'ALM4_A_SPOOF_SV23': '-', 'ALM4_A_SPOOF_SV24': '-',
    'ALM4_A_SPOOF_SV25': '-', 'ALM4_A_SPOOF_SV26': '-', 'ALM4_A_SPOOF_SV27': '-', 'ALM4_A_SPOOF_SV28': '-',
    'ALM4_A_SPOOF_SV29': '-', 'ALM4_A_SPOOF_SV30': '-', 'ALM4_A_SPOOF_SV31': '-', 'ALM4_A_SPOOF_SV32': '-',
    'ALM4_sub4_Res1': "-",
    'ALM4_Health_SV25': '-', 'ALM4_Health_SV26': '-', 'ALM4_Health_SV27': '-', 'ALM4_Health_SV28': '-',
    'ALM4_Health_SV29': '-', 'ALM4_Health_SV30': '-', 'ALM4_Health_SV31': '-', 'ALM4_Health_SV32': '-',
    'ALM4_sub4_Res2': "-",
    'Data_id': '-', 'SV(PAGE)_id': '-',
    "ALM5_e": "-", "ALM5_toa": "-", "ALM5_Delta_I": "-", "ALM5_omegaDot": "-", "ALM5_Sqaure Root A": "-",
    "ALM5_Omega_0": "-", "ALM5_Omega": "-", "ALM5_M_0": "-", "ALM5_a_f0": "-", "ALM5_a_f1": "-",
    "ALM5_SV HEALTH_5": "-",
    'ALM5_Week Number': '-', "ALM5P25_toa": "-",
    'ALM5_Health_SV1': '-', 'ALM5_Health_SV2': '-', 'ALM5_Health_SV3': '-', 'ALM5_Health_SV4': '-',
    'ALM5_Health_SV5': '-', 'ALM5_Health_SV6': '-', 'ALM5_Health_SV7': '-', 'ALM5_Health_SV8': '-',
    'ALM5_Health_SV9': '-', 'ALM5_Health_SV10': '-', 'ALM5_Health_SV11': '-', 'ALM5_Health_SV12': '-',
    'ALM5_Health_SV13': '-', 'ALM5_Health_SV14': '-', 'ALM5_Health_SV15': '-', 'ALM5_Health_SV16': '-',
    'ALM5_Health_SV17': '-', 'ALM5_Health_SV18': '-', 'ALM5_Health_SV19': '-', 'ALM5_Health_SV20': '-',
    'ALM5_Health_SV21': '-', 'ALM5_Health_SV22': '-', 'ALM5_Health_SV23': '-', 'ALM5_Health_SV24': '-',
    'ALM5_sub5_Res1': "-", 'ALM5_sub5_Res2': "-"
}
dstructActual = {
    'TOWC': "Actual Value", 'PRN': "-", 'subframe id': "-", "page id": "-",
    'preamble': '-',  # TLM fixed 8 bits
    'TLM_MSG': '-',
    'TLM_Reserved': '-',  # TLM reserverd 17 bits
    'HOW_Reserved': '-',  # reserved copy as it is
    'Alert_Antispoof': '-',  # antispoof copy as it is
    'HOW_Page_id': '-',  # generate subframe id
    'Parity_bit_solve': '-',
    "Week Number": "-", "Accuracy": "-", "Health": "-", "T_GD": "-", "IODC": "-", "t_oc": "-", "a_f2": "-", "a_f1": "-",
    "a_f0": "-",
    "Code Type": "-", "L2P Flag": "-", "Reserved 1": "-", "Reserved 2": "-", "Reserved 3": "-", "Reserved 4": "-",
    "IODE_sf2": "-", "C_rs": "-", "Deltan": "-", "M_0": "-", "C_uc": "-", "e": "-", "C_us": "-", "sqrt_A": "-",
    "t_oe": "-", "FIT INTERVAL": "-", "AODO": "-",
    "C_ic": "-", "Omega_0": "-", "C_is": "-", "I_0": "-", "C_rc": "-", "Omega": "-", "omegaDot": "-", "IODE_sf3": "-",
    "iDot": "-",
    "ALM4_e": "-", "ALM4_toa": "-", "ALM4_Delta_I": "-", "ALM4_omegaDot": "-", "ALM4_Sqaure Root A": "-",
    "ALM4_Omega_0": "-", "ALM4_Omega": "-", "ALM4_M_0": "-", "ALM4_a_f0": "-", "ALM4_a_f1": "-",
    "ALM4_SV HEALTH_4": "-",
    'ALM4_A_SPOOF_SV1': '-', 'ALM4_A_SPOOF_SV2': '-', 'ALM4_A_SPOOF_SV3': '-', 'ALM4_A_SPOOF_SV4': '-',
    'ALM4_A_SPOOF_SV5': '-', 'ALM4_A_SPOOF_SV6': '-', 'ALM4_A_SPOOF_SV7': '-', 'ALM4_A_SPOOF_SV8': '-',
    'ALM4_A_SPOOF_SV9': '-', 'ALM4_A_SPOOF_SV10': '-', 'ALM4_A_SPOOF_SV11': '-', 'ALM4_A_SPOOF_SV12': '-',
    'ALM4_A_SPOOF_SV13': '-', 'ALM4_A_SPOOF_SV14': '-', 'ALM4_A_SPOOF_SV15': '-', 'ALM4_A_SPOOF_SV16': '-',
    'ALM4_A_SPOOF_SV17': '-', 'ALM4_A_SPOOF_SV18': '-', 'ALM4_A_SPOOF_SV19': '-', 'ALM4_A_SPOOF_SV20': '-',
    'ALM4_A_SPOOF_SV21': '-', 'ALM4_A_SPOOF_SV22': '-', 'ALM4_A_SPOOF_SV23': '-', 'ALM4_A_SPOOF_SV24': '-',
    'ALM4_A_SPOOF_SV25': '-', 'ALM4_A_SPOOF_SV26': '-', 'ALM4_A_SPOOF_SV27': '-', 'ALM4_A_SPOOF_SV28': '-',
    'ALM4_A_SPOOF_SV29': '-', 'ALM4_A_SPOOF_SV30': '-', 'ALM4_A_SPOOF_SV31': '-', 'ALM4_A_SPOOF_SV32': '-',
    'ALM4_sub4_Res1': "-",
    'ALM4_Health_SV25': '-', 'ALM4_Health_SV26': '-', 'ALM4_Health_SV27': '-', 'ALM4_Health_SV28': '-',
    'ALM4_Health_SV29': '-', 'ALM4_Health_SV30': '-', 'ALM4_Health_SV31': '-', 'ALM4_Health_SV32': '-',
    "ALM5_e": "-", "ALM5_toa": "-", "ALM5_Delta_I": "-", "ALM5_omegaDot": "-", "ALM5_Sqaure Root A": "-",
    "ALM5_Omega_0": "-", "ALM5_Omega": "-", "ALM5_M_0": "-", "ALM5_a_f0": "-", "ALM5_a_f1": "-",
    "ALM5_SV HEALTH_5": "-",
    'ALM4_sub4_Res2': "-",
    'Data_id': '-', 'SV(PAGE)_id': '-',
    'ALM5_Week Number': '-', "ALM5P25_toa": "-",
    'ALM5_Health_SV1': '-', 'ALM5_Health_SV2': '-', 'ALM5_Health_SV3': '-', 'ALM5_Health_SV4': '-',
    'ALM5_Health_SV5': '-', 'ALM5_Health_SV6': '-', 'ALM5_Health_SV7': '-', 'ALM5_Health_SV8': '-',
    'ALM5_Health_SV9': '-', 'ALM5_Health_SV10': '-', 'ALM5_Health_SV11': '-', 'ALM5_Health_SV12': '-',
    'ALM5_Health_SV13': '-', 'ALM5_Health_SV14': '-', 'ALM5_Health_SV15': '-', 'ALM5_Health_SV16': '-',
    'ALM5_Health_SV17': '-', 'ALM5_Health_SV18': '-', 'ALM5_Health_SV19': '-', 'ALM5_Health_SV20': '-',
    'ALM5_Health_SV21': '-', 'ALM5_Health_SV22': '-', 'ALM5_Health_SV23': '-', 'ALM5_Health_SV24': '-',
    'ALM5_sub5_Res1': "-", 'ALM5_sub5_Res2': "-"
}
distructcalc = {
    'TOWC': "Calculated Value", 'PRN': "-", 'subframe id': "-", "page id": "-",
    'preamble': '-',  # TLM fixed 8 bits
    'TLM_MSG': '-',
    'TLM_Reserved': '-',  # TLM reserverd 17 bits
    'HOW_Reserved': '-',  # reserved copy as it is
    'Alert_Antispoof': '-',  # antispoof copy as it is
    'HOW_Page_id': '-',  # generate subframe id
    'Parity_bit_solve': '-',
    "Week Number": "-", "Accuracy": "-", "Health": "-", "T_GD": "-", "IODC": "-", "t_oc": "-", "a_f2": "-", "a_f1": "-",
    "a_f0": "-", "toa": "-",  # to be inserted almnac -Time of Applicability(s)
    "Code Type": "-", "L2P Flag": "-", "Reserved 1": "-", "Reserved 2": "-", "Reserved 3": "-", "Reserved 4": "-",
    "IODE_sf2": "-", "C_rs": "-", "Deltan": "-", "M_0": "-", "C_uc": "-", "e": "-", "C_us": "-", "sqrt_A": "-",
    "t_oe": "-", "FIT INTERVAL": "-", "AODO": "-",
    "C_ic": "-", "Omega_0": "-", "C_is": "-", "I_0": "-", "C_rc": "-", "Omega": "-", "omegaDot": "-", "IODE_sf3": "-",
    "iDot": "-",
    "ALM4_e": "-", "ALM4_toa": "-", "ALM4_Delta_I": "-", "ALM4_omegaDot": "-", "ALM4_Sqaure Root A": "-",
    "ALM4_Omega_0": "-", "ALM4_Omega": "-", "ALM4_M_0": "-", "ALM4_a_f0": "-", "ALM4_a_f1": "-",
    "ALM4_SV HEALTH_4": "-",
    'ALM4_A_SPOOF_SV1': '-', 'ALM4_A_SPOOF_SV2': '-', 'ALM4_A_SPOOF_SV3': '-', 'ALM4_A_SPOOF_SV4': '-',
    'ALM4_A_SPOOF_SV5': '-', 'ALM4_A_SPOOF_SV6': '-', 'ALM4_A_SPOOF_SV7': '-', 'ALM4_A_SPOOF_SV8': '-',
    'ALM4_A_SPOOF_SV9': '-', 'ALM4_A_SPOOF_SV10': '-', 'ALM4_A_SPOOF_SV11': '-', 'ALM4_A_SPOOF_SV12': '-',
    'ALM4_A_SPOOF_SV13': '-', 'ALM4_A_SPOOF_SV14': '-', 'ALM4_A_SPOOF_SV15': '-', 'ALM4_A_SPOOF_SV16': '-',
    'ALM4_A_SPOOF_SV17': '-', 'ALM4_A_SPOOF_SV18': '-', 'ALM4_A_SPOOF_SV19': '-', 'ALM4_A_SPOOF_SV20': '-',
    'ALM4_A_SPOOF_SV21': '-', 'ALM4_A_SPOOF_SV22': '-', 'ALM4_A_SPOOF_SV23': '-', 'ALM4_A_SPOOF_SV24': '-',
    'ALM4_A_SPOOF_SV25': '-', 'ALM4_A_SPOOF_SV26': '-', 'ALM4_A_SPOOF_SV27': '-', 'ALM4_A_SPOOF_SV28': '-',
    'ALM4_A_SPOOF_SV29': '-', 'ALM4_A_SPOOF_SV30': '-', 'ALM4_A_SPOOF_SV31': '-', 'ALM4_A_SPOOF_SV32': '-',
    'ALM4_sub4_Res1': "-",
    'ALM4_Health_SV25': '-', 'ALM4_Health_SV26': '-', 'ALM4_Health_SV27': '-', 'ALM4_Health_SV28': '-',
    'ALM4_Health_SV29': '-', 'ALM4_Health_SV30': '-', 'ALM4_Health_SV31': '-', 'ALM4_Health_SV32': '-',
    'ALM4_sub4_Res2': "-",
    'Data_id': '-', 'SV(PAGE)_id': '-',
    "ALM5_e": "-", "ALM5_toa": "-", "ALM5_Delta_I": "-", "ALM5_omegaDot": "-", "ALM5_Sqaure Root A": "-",
    "ALM5_Omega_0": "-", "ALM5_Omega": "-", "ALM5_M_0": "-", "ALM5_a_f0": "-", "ALM5_a_f1": "-",
    "ALM5_SV HEALTH_5": "-",
    'ALM5_Week Number': '-', "ALM5P25_toa": "-",
    'ALM5_Health_SV1': '-', 'ALM5_Health_SV2': '-', 'ALM5_Health_SV3': '-', 'ALM5_Health_SV4': '-',
    'ALM5_Health_SV5': '-', 'ALM5_Health_SV6': '-', 'ALM5_Health_SV7': '-', 'ALM5_Health_SV8': '-',
    'ALM5_Health_SV9': '-', 'ALM5_Health_SV10': '-', 'ALM5_Health_SV11': '-', 'ALM5_Health_SV12': '-',
    'ALM5_Health_SV13': '-', 'ALM5_Health_SV14': '-', 'ALM5_Health_SV15': '-', 'ALM5_Health_SV16': '-',
    'ALM5_Health_SV17': '-', 'ALM5_Health_SV18': '-', 'ALM5_Health_SV19': '-', 'ALM5_Health_SV20': '-',
    'ALM5_Health_SV21': '-', 'ALM5_Health_SV22': '-', 'ALM5_Health_SV23': '-', 'ALM5_Health_SV24': '-',
    'ALM5_sub5_Res1': "-", 'ALM5_sub5_Res2': "-"

}
