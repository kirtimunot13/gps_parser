import SV_functions.gps_constants as constants
import math


def calculate_position_sv(Eph, sv, t):
    # Calculate Position
    A = Eph[sv]["sqrt_A"] * Eph[sv]["sqrt_A"]
    n = math.sqrt(constants.GM_EARTH / (A * A * A)) + Eph[sv]["Deltan"]
    sq1e2 = math.sqrt(1.0 - Eph[sv]["e"] * Eph[sv]["e"])
    omgkdot = Eph[sv]["omegaDot"] - constants.OMEGA_EARTH
    #Eph[sv]["t_oe"] = 0

    tk_temp = t - Eph[sv]["t_oe"]

    tk = tk_temp
    if tk_temp > constants.SECONDS_IN_HALF_WEEK:
        tk = tk_temp - constants.SECONDS_IN_WEEK
    elif tk_temp < (-constants.SECONDS_IN_HALF_WEEK):
        tk = tk_temp + constants.SECONDS_IN_WEEK
    else:
        pass

    mk = Eph[sv]["M_K"] + n * tk
    #mk = Eph[sv]["M_K"]
    # Kepler Equation
    ek = mk
    ekold = ek + 1.0
    OneMinusecosE = 0  # Suppress the uninitialized warning.
    while abs(ek - ekold) > 1.0e-14:
        ekold = ek
        OneMinusecosE = 1.0 - Eph[sv]["e"] * math.cos(ekold)
        ek = ek + (mk - ekold + Eph[sv]["e"] * math.sin(ekold)) / OneMinusecosE

    sek = math.sin(ek)
    cek = math.cos(ek)
    ekdot = n / OneMinusecosE
    relativistic = -4.442807633e-10 * Eph[sv]["e"] * Eph[sv]["sqrt_A"] * sek

    pk = math.atan2(sq1e2 * sek, cek - Eph[sv]["e"]) + Eph[sv]["Omega"]  # Angle of perigee
    pkdot = sq1e2 * ekdot / OneMinusecosE
    s2pk = math.sin(2.0 * pk)
    c2pk = math.cos(2.0 * pk)
    uk = pk + Eph[sv]["C_us"] * s2pk + Eph[sv]["C_uc"] * c2pk
    suk = math.sin(uk)
    cuk = math.cos(uk)
    ukdot = pkdot * (1.0 + 2.0 * (Eph[sv]["C_us"] * c2pk - Eph[sv]["C_uc"] * s2pk))

    rk = A * OneMinusecosE + Eph[sv]["C_rc"] * c2pk + Eph[sv]["C_rs"] * s2pk
    rkdot = A * Eph[sv]["e"] * sek * ekdot + 2.0 * pkdot * (Eph[sv]["C_rs"] * c2pk - Eph[sv]["C_rc"] * s2pk)

    ik = Eph[sv]["I_0"] + Eph[sv]["iDot"] * tk + Eph[sv]["C_ic"] * c2pk + Eph[sv][
        "C_is"] * s2pk  # "I_0" -> inc0 #idot - >
    sik = math.sin(ik)
    cik = math.cos(ik)
    ikdot = Eph[sv]["iDot"] + 2.0 * pkdot * (Eph[sv]["C_is"] * c2pk - Eph[sv]["C_ic"] * s2pk)

    xpk = rk * cuk
    ypk = rk * suk
    xpkdot = rkdot * cuk - ypk * ukdot
    ypkdot = rkdot * suk + xpk * ukdot

    ok = Eph[sv]["Omega_0"] + tk * omgkdot - constants.OMEGA_EARTH * Eph[sv]["t_oe"]
    sok = math.sin(ok)
    cok = math.cos(ok)

    pos = [0,0,0]
    vel = [0,0,0]
    pos[0] = xpk * cok - ypk * cik * sok
    pos[1] = xpk * sok + ypk * cik * cok
    pos[2] = ypk * sik

    tmp = ypkdot * cik - ypk * sik * ikdot

    vel[0] = -omgkdot * pos[1] + xpkdot * cok - tmp * sok
    vel[1] = omgkdot * pos[0] + xpkdot * sok + tmp * cok
    vel[2] = ypk * cik * ikdot + ypkdot * sik

    # Satellite clock correction
    tk_temp = t - Eph[sv]["t_oc"]

    if tk_temp > constants.SECONDS_IN_HALF_WEEK:
        tk = tk_temp - constants.SECONDS_IN_WEEK
    elif tk_temp < (-constants.SECONDS_IN_HALF_WEEK):
        tk = tk_temp + constants.SECONDS_IN_WEEK
    else :
        tk = tk_temp

    clk = [0,0]
    clk[0] = Eph[sv]["a_f0"] + tk * (Eph[sv]["a_f1"] + tk * Eph[sv]["a_f2"]) + relativistic - Eph[sv]["T_GD"]
    clk[1] = Eph[sv]["a_f1"] + 2.0 * tk * Eph[sv]["a_f2"]

    return pos,vel,clk