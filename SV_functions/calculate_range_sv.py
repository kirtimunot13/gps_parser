import SV_functions.gps_constants as constants
from SV_functions.calculate_position_sv import calculate_position_sv
import math
import numpy as np

def calculate_range_sv(Eph, sv, t, xyz):
    pos, vel, clk = calculate_position_sv(Eph, sv, t)
    Rho = {}

    # convert xyz, pos , vel , clk as numpy as np. arrays for calculations
    pos = np.asarray(pos)
    vel = np.asarray(vel)
    clk = np.asarray(clk)
    xyz = np.asarray(xyz)

    #los = pos - xyz
    #norm_los = math.sqrt( los[0] * los[0] + los[1] * los[2] + los[3] * los[3] )
    norm_los = np.linalg.norm(pos-xyz)
    tau = norm_los / constants.SPEED_OF_LIGHT

    #Extrapolate the satellite position backwards to the transmission time.
    pos = pos - (vel * tau)

    #Earth rotation correction. The change in velocity can be neglected.
    xrot = pos[0] + pos[1] * constants.OMEGA_EARTH * tau
    yrot = pos[1] - pos[0] * constants.OMEGA_EARTH * tau
    pos[0] = xrot
    pos[1] = yrot

    #New observer to satellite vector and satellite range.
    los=pos-xyz
    #psuedo_range =math.sqrt( los[0] * los[0] + los[1] * los[2] + los[3] * los[3] )
    psuedo_range = np.linalg.norm(pos-xyz)
    Rho[sv] = {}
    Rho[sv]["slantRange"] = psuedo_range

    #pseudorange
    Rho[sv]["range"] = psuedo_range - constants.SPEED_OF_LIGHT * clk[0]

    #ps_rate
    temp = [0,0,0]
    for i in range(len(temp)):
        temp = (vel[i] * los[i])
    temp = np.sum(temp) / psuedo_range
    Rho[sv]["rate"] = temp
    Rho[sv]["t"] = t

    #calculate az and elevation

    return Rho












