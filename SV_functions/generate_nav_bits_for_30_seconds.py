

from Basic_functions import dump_list_to_csv
import SV_functions.constdict
import copy,math
from Basic_functions import tows_binary_data,binary_data,binary,twos_complement
from SV_functions.read_alamanc_file import get_alamanac,return_almanac_sequence
from SV_functions.subframe_creation import ephemeris_to_subframe,ephemeris_to_almanac_subframe,return_almanac_sequence_from_ephemeris #,add_time_and_parity
from SV_functions.parity_code import get_subframe_parity,flip_words_with_d30_as_1

import SV_functions.gps_constants as constants
import numpy as np

def generate_nav_for_30_sec(subframes_from_almanac123, almnac_struct45, page_id, towc, Week_no , ephemeris):
    #subframe_all= {copy.deepcopy(constdict.subframes)}
    for svid in range(1,32):
        if subframes_from_almanac123[svid][1]!='':
            # add subframes 3,4
            #subframes_from_almanac123[svid].update(almnac_struct45[page_id])

            sv_towc = towc

            toe = math.ceil(( sv_towc  / 7200)) * 7200
            toc = math.ceil(( sv_towc  / 7200)) * 7200


            for subframe in range(1,6):
                sv_towc = (sv_towc + 6)


                if subframe==1:
                    subframes_from_almanac123[svid][subframe]= (binary_data(16, toc/16 , 0,1)).join([ subframes_from_almanac123[svid][subframe][:218],  subframes_from_almanac123[svid][subframe][234:]])
                    subframes_from_almanac123[svid][subframe] = (binary_data(10, Week_no, 0, 1)).join([subframes_from_almanac123[svid][subframe][:60], subframes_from_almanac123[svid][subframe][70:]])

                if subframe == 2:
                    subframes_from_almanac123[svid][subframe]= (binary_data(16, toe/16 , 0,1)).join([ subframes_from_almanac123[svid][subframe][:270],  subframes_from_almanac123[svid][subframe][286:]])
                    # also add M_K in corresponding subframes
                    tbits = (tows_binary_data(32, (ephemeris[svid]['M_K']), 31, constants.pi))
                    subframes_from_almanac123[svid][subframe] = (tbits[0:8]).join([subframes_from_almanac123[svid][subframe][:106], subframes_from_almanac123[svid][subframe][114:]])
                    subframes_from_almanac123[svid][subframe] = (tbits[8:32]).join([subframes_from_almanac123[svid][subframe][:120], subframes_from_almanac123[svid][subframe][144:]])
                    '''if svid == 22 :
                        print("Calculated : ", ephemeris[svid]['M_K'])
                        print("Calculated :" ,tbits)
                        print("Calculated :" ,twos_complement( tbits ) * constants.POW2_M31 * constants.pi  )'''
                if subframe == 3 :
                   pass

                if subframe == 4 :
                    # create array of 300 zeros
                    # add data ID 60 to 62 as 01
                    # add page id as per mapping

                    zero_message = "0".zfill(300)
                    #zero_message = ''.join(str(x) for x in zero_message)
                    zero_message = '10001011'.join([zero_message[:0], zero_message[8:]])
                    zero_message = "100".join([zero_message[:49], zero_message[52:]])
                    zero_message = "01".join([zero_message[:60], zero_message[62:]])
                    zero_message = binary_data(6, 63, 0, 1).join([zero_message[:62], zero_message[68:]])
                    subframes_from_almanac123[svid][subframe] = zero_message

                if subframe == 5:
                    # create array of 300 zeros
                    # add data ID 60 to 62 as 01
                    # add page id as per mapping

                    zero_message = "0".zfill(300)
                    #zero_message = ''.join(str(x) for x in zero_message)
                    zero_message = '10001011'.join([zero_message[:0], zero_message[8:]])
                    zero_message = "101".join([zero_message[:49], zero_message[52:]])
                    zero_message = "01".join([zero_message[:60], zero_message[62:]])
                    zero_message = binary_data(6, 51, 0, 1).join([zero_message[:62], zero_message[68:]])
                    zero_message = binary_data(8, ephemeris[svid]["toa"], 12, 1).join([zero_message[:90], zero_message[98:]])
                    subframes_from_almanac123[svid][subframe] = zero_message

                subframes_from_almanac123[svid][subframe] = (binary_data(17, sv_towc/6  , 0, 1)).join([subframes_from_almanac123[svid][subframe][:30], subframes_from_almanac123[svid][subframe][47:]])

                subframes_from_almanac123[svid][subframe] = get_subframe_parity(subframes_from_almanac123[svid][subframe])
                subframes_from_almanac123[svid][subframe] = flip_words_with_d30_as_1(subframes_from_almanac123[svid][subframe])

    return subframes_from_almanac123

def add_time_parameters_to_ephemeris(ephemeris, time_parameters):
    # Calculate toe and toc as ceil of 7200
    t_oc = math.ceil( (time_parameters["seconds"]) / 7200) * 7200
    for sv in range(1, 33):
        if ephemeris[sv] == {}:
            #print("No ephemeris found :", sv)
            pass
        else:
            ephemeris[sv]["TOWC"] = time_parameters["seconds"] + 30
            ephemeris[sv]['t_oc'] = t_oc
            ephemeris[sv]['t_oe'] = t_oc
            ephemeris[sv]['Week Number'] = time_parameters["Week"]
            # Calculate Mk from M0
            n = math.sqrt(constants.GM_EARTH / ((float(ephemeris[sv]["sqrt_A"])) ** 6))

            tk = float(ephemeris[sv]['t_oe']) - float(ephemeris[sv]['toa'])
            Mk = float(ephemeris[sv]['M_0']) + n * (tk)
            multiplier = int(Mk / constants.pi)
            if multiplier % 2 != 0:
                Mk = Mk - (multiplier) * constants.pi + constants.pi
            else:
                Mk = Mk - (multiplier) * constants.pi
            ephemeris[sv]['M_K'] = round(Mk,9)


    return ephemeris

def get_1500_nav_bit_array(navigation_message, previous_word):
    Nav_message = np.zeros((32, 1530))
    for sv in range(1,32):
        if navigation_message[sv][1] != "" :
            temp = ""
            for subframe in range(1,6):
                temp += navigation_message[sv][subframe]
            new_message = np.fromstring(temp, 'u1') - ord('0')
            Nav_message[sv, :] = np.append( previous_word[sv],  new_message )
             #= np.append(first_part , next_word[sv])
        else :
            print( "Ignored SV - ", sv )
    Nav_message[ Nav_message == 0] = (-1)
    return Nav_message

def get_1500_nav_sting(navigation_message, prnlist):
    Nav_message = {}
    for Valid_PRN in prnlist:
        temp = ""
        for subframe in range(1,6):
            #print(Valid_PRN,subframe)
            temp += navigation_message[Valid_PRN][subframe]
        Nav_message[Valid_PRN] = temp
    return Nav_message

