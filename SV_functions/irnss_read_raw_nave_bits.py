import pandas as pd
import copy
from SV_functions import constdict,gps_constants as constants
from Basic_functions import binary,twos_complement

def irnss_get_almanac_from_raw_navbits_new(filename):
    alm_list=[]
    RNBB_data = pd.read_csv(filename)
    irnss_data = RNBB_data.to_dict(orient="record")

    Parsed_data =  {  }#copy.deepcopy(constdict.subframes)
    # Create a structure of 32 with in satellites each with 5 subframes

    for record in irnss_data:
        tempr = {}
        tempr["Bits"] = ""
        for i in range(1, 38):
            tempr["Bits"] += format(record['Raw Navbits – ' + str(i)], '08b')
        tempr['TOWC'] = record['TOWC (corresponding to start of the sub-frame) (s)']
        tempr["WN"] = record['Week No (corresponding to start of the sub-frame)']
        tempr["PRN"] = record["PRN"]

        tempr['subframe id'] = record['subframe id']
        #print(tempr["PRN"],tempr['subframe id'])

        if tempr["PRN"] in Parsed_data :
            if tempr['subframe id'] in Parsed_data[tempr["PRN"]]:
                pass
            else :
                Parsed_data[tempr["PRN"]][tempr["subframe id"]] = tempr
        else :
            if tempr['subframe id'] == 1 :
                Parsed_data[tempr["PRN"]] = {}
                Parsed_data[ tempr["PRN"] ][ tempr["subframe id"] ] = tempr
    #print(Parsed_data)

    Valid_PRNs = list(Parsed_data.keys())
    print("Found PRNs - " , Valid_PRNs)

    ephemeris = copy.deepcopy(constdict.irnss_ephemeris)
    for Valid_PRN in Valid_PRNs :
        ephemeris_template = copy.deepcopy(constdict.irnss_ephemeris_template)
        ephemeris_template["PRN"] = Valid_PRN
        #ephemeris_template["msg id"] = "-"
        for subframe in range(1,3):
            nav_bits = "10001011"+Parsed_data[Valid_PRN][subframe]["Bits"]
            ephemeris_template["TLM"] = (nav_bits[0:8])
            ephemeris_template["TOWC"] = binary(nav_bits[8:25]) * 12
            # print(nav_bits[76:82])
            ephemeris_template["Alert"] = (nav_bits[25:26])
            ephemeris_template["Autonav"] = (nav_bits[26:27])
            ephemeris_template["Spare"] = (nav_bits[29:30])
            if subframe == 1 :
                #print(Valid_PRN,"1 read",Parsed_data[Valid_PRN][subframe]['TOWC'])
                ephemeris_template["subframe id"] = 1
                ephemeris_template["WN"] = binary(nav_bits[30:40])
                ephemeris_template["af0"] = twos_complement(nav_bits[40:62]) * constants.POW2_M31 #Clk_bias
                ephemeris_template["af1"] = twos_complement(nav_bits[62:78]) * constants.POW2_M43 #Clk_drift
                ephemeris_template["af2"] = twos_complement(nav_bits[78:86]) * constants.POW2_M55 #Clk_drift_rate
                ephemeris_template["svhlth"] = binary(nav_bits[86:90]) #SV_accuracy
                ephemeris_template["toc"] = binary(nav_bits[90:106]) * 16  # Time_of_clk
                ephemeris_template["tgd"] = twos_complement(nav_bits[106:114]) * constants.POW2_M31  # Ttl_Group_delay
                ephemeris_template["deltan"] = twos_complement(nav_bits[114:136]) *constants.POW2_M41*constants.pi  # Mean_motion_delay
                ephemeris_template["iode"] = binary(nav_bits[136:144]) #Issue_of_data_Ephemaries_Clk
                ephemeris_template["reserved"] = binary(nav_bits[144:154])
                ephemeris_template["L5_flag"] = binary(nav_bits[154:155])
                ephemeris_template["S_flag"] = binary(nav_bits[155:156])
                ephemeris_template["cuc"] = twos_complement(nav_bits[156:171]) * constants.POW2_M28  # Ampl_cosHarmonic_Arg_latitute
                ephemeris_template["cus"] = twos_complement(nav_bits[171:186]) * constants.POW2_M28    # Ampl_sinHarmonic_Arg_latitute
                ephemeris_template["cic"] = twos_complement(nav_bits[186:201]) * constants.POW2_M28   # Ampl_cosHarmonic_angle_latitute
                ephemeris_template["cis"] = twos_complement(nav_bits[201:216])*constants.POW2_M28    # Ampl_sinHarmonic_angle_latitute
                ephemeris_template["crc"] = twos_complement(nav_bits[216:231]) * constants.POW2_M4  # Ampl_cosHarmonic_orbit_radius
                ephemeris_template["crs"] = twos_complement(nav_bits[231:246]) * constants.POW2_M43  # Ampl_sinHarmonic_orbit_radius
                ephemeris_template["idot"] = twos_complement(nav_bits[246:260]) * constants.POW2_M43*constants.pi  # Rate_inclination_angle
                ephemeris_template["spare_data"] = binary(nav_bits[260:262])
                ephemeris_template["CRC"] = binary(nav_bits[262:286])
                ephemeris_template["tail"] = binary(nav_bits[286:292])


            elif  subframe == 2 :
                ephemeris_template["subframe id"] = 2
                ephemeris_template["m0"] = twos_complement(nav_bits[30:62]) * constants.POW2_M31 * constants.pi  # Mean_anamoly
                ephemeris_template["toe"] = binary(nav_bits[62:78]) * 16  # Time_Ephemeris
                ephemeris_template["ecc"] = binary(nav_bits[78:110]) *  constants.POW2_M33  # Eccentricities
                ephemeris_template["sqrta"] = binary(nav_bits[110:142]) *  constants.POW2_M19 # SQRT_Semi_Major_axis
                ephemeris_template["omg0"] = twos_complement(nav_bits[142:174]) * constants.POW2_M31 #Long_ascending_node
                ephemeris_template["aop"] = twos_complement(nav_bits[174:206]) * constants.POW2_M31 * constants.pi #Arg_Perigee
                ephemeris_template["omgdot"] = twos_complement(nav_bits[206:228]) * constants.POW2_M31 * constants.pi #Rate_RAAN
                ephemeris_template["inc0"] = twos_complement(nav_bits[228:260]) * constants.POW2_M31 * constants.pi   # Inclination
                ephemeris_template["spare_data"] = binary(nav_bits[260:262])

                ephemeris_template["crc"] = binary(nav_bits[262:286])
                ephemeris_template["tail"] = binary(nav_bits[286:292])

            else:
                pass
        alm_list.append(ephemeris_template)
        ephemeris[Valid_PRN] = ephemeris_template
    return ephemeris

