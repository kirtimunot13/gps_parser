import SV_functions.gps_constants as constants
from SV_functions.calculate_range_sv import calculate_range_sv
from SV_functions.read_alamanc_file import read_alm_param
import SV_functions.constdict
import math
import copy

from SV_functions.calculate_position_sv import calculate_position_sv
from Basic_functions.dump_list_to_csv import dump_list_to_csv


def generate_satellite_parameters_for_30_seconds(Eph, scenario_time, xyz , samplerate ):
    satellite_parameters = {}
    # print("IN Satellite Parameters")
    # print(t)
    # print("-----------------------------")

    time_steps = int(30 / constants.refresh_rate)
    T = 1 / samplerate

    for sv in range(1, 33):

        if Eph[sv] == {}:
            pass
        else:
            # ref = [0, 0, 0]
            # Rho_ref = calculate_range_sv(Eph, sv, scenario_time , ref)
            # phase = (2 * Rho_start[sv]["range"] - Rho_ref[sv]["range"]) / constants.LAMBDA_L1

            satellite_parameters[sv] = {}

            old_rho = calculate_range_sv(Eph, sv, scenario_time, xyz)
            for time_step in range(0,time_steps):
                satellite_parameters[sv][time_step] = copy.deepcopy(SV_functions.constdict.satellite_parameter_template)

                new_time = scenario_time + ( (time_step+1) * constants.refresh_rate )
                current_rho = calculate_range_sv(Eph, sv, new_time, xyz)
                rhorate = (current_rho[sv]["range"] - old_rho[sv]["range"]) / constants.refresh_rate
                doppler = -rhorate / constants.LAMBDA_L1
                codephase = constants.CODE_FREQ + doppler * constants.CARR_TO_CODE

                # Initial code phase and data bit counters.
                time_for_transmission = current_rho[sv]["range"] / constants.SPEED_OF_LIGHT
                number_of_ca_code_bits = time_for_transmission / constants.TIME_FOR_1_CA_BIT

                # print(number_of_ca_code_bits)
                Codephase = (constants.CA_SEQ_LEN * 20) - number_of_ca_code_bits % (constants.CA_SEQ_LEN * 20)
                Bit_Number = 30 - math.ceil(time_for_transmission / constants.TIME_FOR_1_NAV_BIT)

                old_rho = current_rho

                satellite_parameters[sv][time_step]["SV"] = sv
                satellite_parameters[sv][time_step]["TOWC"] = new_time
                satellite_parameters[sv][time_step]["Pseudorange"] = current_rho[sv]["range"]
                satellite_parameters[sv][time_step]["coderate"] = codephase
                satellite_parameters[sv][time_step]["Dopplers"] = doppler
                satellite_parameters[sv][time_step]["Code_phase"] = Codephase
                satellite_parameters[sv][time_step]["Nav_Bit"] = Bit_Number
                satellite_parameters[sv][time_step]["Carr_phase"] = 0
                satellite_parameters[sv][time_step]["Code_Phase_Step"] = satellite_parameters[sv][time_step]["coderate"] * T
                satellite_parameters[sv][time_step]["Carr_Phase_Step"] = satellite_parameters[sv][time_step]["Dopplers"] * T
                satellite_parameters[sv][time_step]["gain"] = 700

                pos, vel, clk = calculate_position_sv(Eph, sv, new_time)

                satellite_parameters[sv][time_step]["check_data"]["pos"] = pos
                satellite_parameters[sv][time_step]["check_data"]["vel"] = vel
                satellite_parameters[sv][time_step]["check_data"]["clk"] = clk






            #print("CORRECT CODE")
            # satellite_parameters[sv]["Azimuth"].append(Rho[sv]["az"])
            # satellite_parameters[sv]["Elevation"].append(Rho[sv]["elev"])
            # satellite_parameters[sv]["Slant_Range"].append(Rho[sv]["slantRange"])
            # satellite_parameters[sv]["Rate"].append(Rho[sv]["rate"])
    # print(satellite_parameters.keys())
    # print("----------------------")

    return satellite_parameters

#print_satellite_parameters(Valid_PRNs=[1,2],satellite_parameters=play_parameters["satellite_parameters"])

def print_satellite_parameters(Valid_PRNs,satellite_parameters):
    csv_dump = []
    temp_dump = []
    # Append Headings
    for Valid_PRN in Valid_PRNs :
        temp_dump.append("SV_" + str(Valid_PRN))
        temp_dump.append("Time_" + str(Valid_PRN))
        temp_dump.append("Pseudorange_" + str(Valid_PRN))
        temp_dump.append("coderate_" + str(Valid_PRN))
        temp_dump.append("Dopplers_" + str(Valid_PRN))
        temp_dump.append("Code_phase_" + str(Valid_PRN))
        temp_dump.append("Code_phase_step_" + str(Valid_PRN))
        temp_dump.append("Carr_phase_step_" + str(Valid_PRN))
        temp_dump.append("gain_" + str(Valid_PRN))

        temp_dump.append("pos_X_" + str(Valid_PRN))
        temp_dump.append("pos_Y_" + str(Valid_PRN))
        temp_dump.append("pos_Z_" + str(Valid_PRN))

        temp_dump.append("vel_X_" + str(Valid_PRN))
        temp_dump.append("vel_Y_" + str(Valid_PRN))
        temp_dump.append("vel_Z_" + str(Valid_PRN))

    csv_dump.append(temp_dump)
    # Append Data
    time_steps = int(30 / constants.refresh_rate)
    for time_step in range(0,time_steps) :
        temp_dump = []
        for sv in Valid_PRNs:
            temp_dump.append( satellite_parameters[sv][time_step]["SV"] )
            temp_dump.append(satellite_parameters[sv][time_step]["TOWC"])
            temp_dump.append(satellite_parameters[sv][time_step]["Pseudorange"])
            temp_dump.append(satellite_parameters[sv][time_step]["coderate"])
            temp_dump.append(satellite_parameters[sv][time_step]["Dopplers"])
            temp_dump.append(satellite_parameters[sv][time_step]["Code_phase"])
            temp_dump.append(satellite_parameters[sv][time_step]["Code_Phase_Step"])
            temp_dump.append(satellite_parameters[sv][time_step]["Carr_Phase_Step"])
            temp_dump.append(satellite_parameters[sv][time_step]["gain"])

            temp_dump.append(satellite_parameters[sv][time_step]["check_data"]["pos"][0])
            temp_dump.append(satellite_parameters[sv][time_step]["check_data"]["pos"][1])
            temp_dump.append(satellite_parameters[sv][time_step]["check_data"]["pos"][2])

            temp_dump.append(satellite_parameters[sv][time_step]["check_data"]["vel"][0])
            temp_dump.append(satellite_parameters[sv][time_step]["check_data"]["vel"][0])
            temp_dump.append(satellite_parameters[sv][time_step]["check_data"]["vel"][0])
        csv_dump.append(temp_dump)
    dump_list_to_csv(filename="Satellite_Parameters.csv",dlist=csv_dump)


def get_current_satellite_params(Eph, scenario_time, xyz , samplerate):
    satellite_parameters = {}
    # print("IN Satellite Parameters")
    # print(t)
    # print("-----------------------------")
    sat_param=read_alm_param(filename="RAW_NAV_BITS/SATB_L1.csv")

    T = 1 / samplerate

    for sv in range(1, 33):
        if Eph[sv] == {}:
            pass
        else:
            satellite_parameters[sv] = copy.deepcopy(SV_functions.constdict.satellite_parameter_template)
            new_time = scenario_time + constants.refresh_rate
            sat_keys = list(sat_param[0].keys())
            for i in range(len(sat_param)):
                temp_data = {}
                satellite_parameters[sv] = copy.deepcopy(SV_functions.constdict.satellite_parameter_template)
                for sv in sat_keys:
                    if sat_param[i][sv]['TOWC'] == scenario_time:
                        satellite_parameters[sv]['Dopplers'] = sat_param[i][sv]['Dopplers']
                        codephase = constants.CODE_FREQ + sat_param[i][sv]['Dopplers'] * constants.CARR_TO_CODE
                        # Initial code phase and data bit counters.
                        time_for_transmission = sat_param[i][sv]['Pseudorange'] / constants.SPEED_OF_LIGHT
                        number_of_ca_code_bits = time_for_transmission / constants.TIME_FOR_1_CA_BIT

                        # print(number_of_ca_code_bits)
                        Codephase = (constants.CA_SEQ_LEN * 20) - number_of_ca_code_bits % (constants.CA_SEQ_LEN * 20)
                        Bit_Number = 30 - math.ceil(time_for_transmission / constants.TIME_FOR_1_NAV_BIT)

                        satellite_parameters[sv]["SV"] = sat_param[i][sv]['SV']
                        satellite_parameters[sv]["TOWC"] = new_time
                        satellite_parameters[sv]["Pseudorange"] = sat_param[i][sv]['Pseudorange']
                        satellite_parameters[sv]["coderate"] = codephase  # donek
                        satellite_parameters[sv]["Dopplers"] =sat_param[i][sv]['Dopplers']
                        satellite_parameters[sv]["Code_phase"] = Codephase
                        satellite_parameters[sv]["Nav_Bit"] = Bit_Number
                        satellite_parameters[sv]["Carr_phase"] = 0
                        satellite_parameters[sv]["Code_Phase_Step"] = satellite_parameters[sv]["coderate"] * T
                        satellite_parameters[sv]["Carr_Phase_Step"] = satellite_parameters[sv]["Dopplers"] * T
                        satellite_parameters[sv]["gain"] = 700

                        pos, vel, clk = calculate_position_sv(Eph, sv, new_time)

                        satellite_parameters[sv]["check_data"]["pos"] = pos
                        satellite_parameters[sv]["check_data"]["vel"] = vel
                        satellite_parameters[sv]["check_data"]["clk"] = clk

    '''
    
    new_time = scenario_time + constants.refresh_rate
            current_rho = calculate_range_sv(Eph, sv, new_time, xyz)
            rhorate = (current_rho[sv]["range"] - old_rho[sv]["range"]) / constants.refresh_rate
            doppler = rhorate / constants.LAMBDA_L1
            codephase = constants.CODE_FREQ + doppler * constants.CARR_TO_CODE

            # Initial code phase and data bit counters.
            time_for_transmission = current_rho[sv]["range"] / constants.SPEED_OF_LIGHT
            number_of_ca_code_bits = time_for_transmission / constants.TIME_FOR_1_CA_BIT

            # print(number_of_ca_code_bits)
            Codephase = (constants.CA_SEQ_LEN * 20) - number_of_ca_code_bits % (constants.CA_SEQ_LEN * 20)
            Bit_Number = 30 - math.ceil(time_for_transmission / constants.TIME_FOR_1_NAV_BIT)

            satellite_parameters[sv]["SV"] = sv
            satellite_parameters[sv]["TOWC"] = new_time
            satellite_parameters[sv]["Pseudorange"] = current_rho[sv]["range"]
            satellite_parameters[sv]["coderate"] = codephase  #donek
            satellite_parameters[sv]["Dopplers"] = doppler
            satellite_parameters[sv]["Code_phase"] = Codephase
            satellite_parameters[sv]["Nav_Bit"] = Bit_Number
            satellite_parameters[sv]["Carr_phase"] = 0
            satellite_parameters[sv]["Code_Phase_Step"] = satellite_parameters[sv]["coderate"] * T
            satellite_parameters[sv]["Carr_Phase_Step"] = satellite_parameters[sv]["Dopplers"] * T
            satellite_parameters[sv]["gain"] = 700

            pos, vel, clk = calculate_position_sv(Eph, sv, new_time)

            satellite_parameters[sv]["check_data"]["pos"] = pos
            satellite_parameters[sv]["check_data"]["vel"] = vel
            satellite_parameters[sv]["check_data"]["clk"] = clk
        #print("CORRECT CODE")
            # satellite_parameters[sv]["Azimuth"].append(Rho[sv]["az"])
            # satellite_parameters[sv]["Elevation"].append(Rho[sv]["elev"])
            # satellite_parameters[sv]["Slant_Range"].append(Rho[sv]["slantRange"])
            # satellite_parameters[sv]["Rate"].append(Rho[sv]["rate"])
    # print(satellite_parameters.keys())
    # print("----------------------")
    
    '''
    return satellite_parameters

