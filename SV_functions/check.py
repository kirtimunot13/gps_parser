#s="10001011001100010100101100001010100101111111101110100111111010010010011011000000000000111000100100110111000010110010101110101011100101011101011000000001101101001010010110110010000001011011011100001110001011111111111101111001001100010100100000001010101100110101000001001111000000011111100000000000"
s="10001011000101000001110100001000010100011010100011000010111010000111011111011100000001000010110110001010000011110010101110101110000000000101010010101011100101110111011111111101111100110111000001000000100101111111111101010011010000010100010111011111100001000000000000010001000111010111110000010000"
s1=s

s=s[0:262]
print(len(s))
gen="1100001100100110011111011";

def crc_remainder(input_bitstring, polynomial_bitstring, initial_filler):
    '''
    Calculates the CRC remainder of a string of bits using a chosen polynomial.
    initial_filler should be '1' or '0'.
    '''
    polynomial_bitstring = polynomial_bitstring.lstrip('0')
    len_input = len(input_bitstring)
    initial_padding = initial_filler * (len(polynomial_bitstring) - 1)
    input_padded_array = list(input_bitstring + initial_padding)
    while '1' in input_padded_array[:len_input]:
        cur_shift = input_padded_array.index('1')
        for i in range(len(polynomial_bitstring)):
            input_padded_array[cur_shift + i] = str(int(polynomial_bitstring[i] != input_padded_array[cur_shift + i]))
    return ''.join(input_padded_array)[len_input:]

q=crc_remainder(s,gen,'0')
print(s1[262:286])
print(q)
