import pandas as pd
import math
import csv

from Basic_functions import tows_binary_data,binary_data,binary,ones_complement
'''
GETS PARITY OF 1 WORD of 30 bits
'''


def get_parity_bits(actual_bits, bit_29, bit_30):
    p = [0, 0, 0, 0, 0, 0]
    p[0] = bit_29 ^ int(actual_bits[0]) ^ int(actual_bits[1]) ^ int(actual_bits[2]) ^ int(actual_bits[4]) ^ int(
        actual_bits[5]) ^ int(actual_bits[9]) ^ int(actual_bits[10]) ^ int(actual_bits[11]) ^ int(
        actual_bits[12]) ^ int(actual_bits[13]) ^ int(actual_bits[16]) ^ int(actual_bits[17]) ^ int(
        actual_bits[19]) ^ int(actual_bits[22])
    p[1] = bit_30 ^ int(actual_bits[1]) ^ int(actual_bits[2]) ^ int(actual_bits[3]) ^ int(actual_bits[5]) ^ int(
        actual_bits[6]) ^ int(actual_bits[10]) ^ int(actual_bits[11]) ^ int(actual_bits[12]) ^ int(
        actual_bits[13]) ^ int(actual_bits[14]) ^ int(actual_bits[17]) ^ int(actual_bits[18]) ^ int(
        actual_bits[20]) ^ int(actual_bits[23])
    p[2] = bit_29 ^ int(actual_bits[0]) ^ int(actual_bits[2]) ^ int(actual_bits[3]) ^ int(actual_bits[4]) ^ int(
        actual_bits[6]) ^ int(actual_bits[7]) ^ int(actual_bits[11]) ^ int(actual_bits[12]) ^ int(
        actual_bits[13]) ^ int(actual_bits[14]) ^ int(actual_bits[15]) ^ int(actual_bits[18]) ^ int(
        actual_bits[19]) ^ int(actual_bits[21])
    p[3] = bit_30 ^ int(actual_bits[1]) ^ int(actual_bits[3]) ^ int(actual_bits[4]) ^ int(actual_bits[5]) ^ int(
        actual_bits[7]) ^ int(actual_bits[8]) ^ int(actual_bits[12]) ^ int(actual_bits[13]) ^ int(
        actual_bits[14]) ^ int(actual_bits[15]) ^ int(actual_bits[16]) ^ int(actual_bits[19]) ^ int(
        actual_bits[20]) ^ int(actual_bits[22])
    p[4] = bit_30 ^ int(actual_bits[0]) ^ int(actual_bits[2]) ^ int(actual_bits[4]) ^ int(actual_bits[5]) ^ int(
        actual_bits[6]) ^ int(actual_bits[8]) ^ int(actual_bits[9]) ^ int(actual_bits[13]) ^ int(actual_bits[14]) ^ int(
        actual_bits[15]) ^ int(actual_bits[16]) ^ int(actual_bits[17]) ^ int(actual_bits[20]) ^ int(
        actual_bits[21]) ^ int(actual_bits[23])
    p[5] = bit_29 ^ int(actual_bits[2]) ^ int(actual_bits[4]) ^ int(actual_bits[5]) ^ int(actual_bits[7]) ^ int(
        actual_bits[8]) ^ int(actual_bits[9]) ^ int(actual_bits[10]) ^ int(actual_bits[12]) ^ int(
        actual_bits[14]) ^ int(actual_bits[18]) ^ int(actual_bits[21]) ^ int(actual_bits[23]) ^ int(actual_bits[22])

    parity = str(p[0]) + str(p[1]) + str(p[2]) + str(p[3]) + str(p[4]) + str(p[5])
    return parity


'''
ADJUSTS T BITS IN WORD 2 and 10; returns full 30 bits
'''


def generate_parity(actual_bits, bit_29, bit_30, word_number):
    # If word 1 or 9 then code to adjust word 1,8 parities as zero
    if word_number == 1 or word_number == 9:
        calculate_temp = bit_30 ^ int(actual_bits[0]) ^ int(actual_bits[2]) ^ int(actual_bits[4]) ^ int(
            actual_bits[5]) ^ int(actual_bits[6]) ^ int(actual_bits[8]) ^ int(actual_bits[9]) ^ int(
            actual_bits[13]) ^ int(actual_bits[14]) ^ int(actual_bits[15]) ^ int(actual_bits[16]) ^ int(
            actual_bits[17]) ^ int(actual_bits[20]) ^ int(actual_bits[21])  # int(actual_bits[23])
        # calculation for bit 23
        if calculate_temp == 1:
            temp = list(actual_bits)
            temp[23] = '1'
            actual_bits = "".join(temp)
        else:
            temp = list(actual_bits)
            temp[23] = '0'
            actual_bits = "".join(temp)

        # calculation for bit 24
        calculate_temp = bit_29 ^ int(actual_bits[2]) ^ int(actual_bits[4]) ^ int(actual_bits[5]) ^ int(
            actual_bits[7]) ^ int(actual_bits[8]) ^ int(actual_bits[9]) ^ int(actual_bits[10]) ^ int(
            actual_bits[12]) ^ int(actual_bits[14]) ^ int(actual_bits[18]) ^ int(actual_bits[21]) ^ int(
            actual_bits[23])  # int(actual_bits[22])
        if calculate_temp == 1:
            temp = list(actual_bits)
            temp[22] = '1'
            actual_bits = "".join(temp)
        else:
            temp = list(actual_bits)
            temp[22] = '0'
            actual_bits = "".join(temp)

    parity = get_parity_bits(actual_bits, bit_29, bit_30)

    actual_bits = actual_bits[:-6]
    actual_bits = actual_bits + parity
    return actual_bits


'''
CALCULATES PARITY OF ALL SUBFRAMES AND RETURNS ALL 300 bits
'''
def get_subframe_parity(subframe):
     # split subframe into 30 words
     words = [subframe[i:i + 30] for i in range(0, len(subframe), 30)]
     bit_29 = 0
     bit_30 = 0
     subframe = ""
     for word_number in range(len(words)):
         if word_number == 0 :
             bit_29 = 0
             bit_30 = 0
         #print(word_number)
         #print(words[word_number])
         words[word_number] = generate_parity(words[word_number], bit_29, bit_30, word_number)
         bit_29 = int(words[word_number][28])
         bit_30 = int(words[word_number][29])

         subframe += words[word_number]

     return subframe


def flip_words_with_d30_as_1(subframe):
    words = [subframe[i:i + 30] for i in range(0, len(subframe), 30)]
    subframe = ""
    bit_30 = 0
    for word_number in range(len(words)):

        if word_number == 0 :
             bit_30 = 0
        else :
            bit_30 = int(words[word_number - 1 ][29])
        if bit_30 == 1 :
            temp = words[word_number]
            words[word_number] =  ones_complement.ones_complement(words[word_number][:-6]) + words[word_number][-6:]

            #print("FLIPPED - ", temp ,words[word_number])

        subframe += words[word_number]
    return subframe

if __name__ == "__main__":
    dummy_string = "101010101010111010101010101010"
    dummy_string = generate_parity(dummy_string, 0, 0, 1)
    print(dummy_string)
