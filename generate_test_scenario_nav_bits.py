# import tensorflow as tf

import numpy as np
import time
import pandas as pd
import sys

np.set_printoptions(threshold=sys.maxsize, precision=17, suppress=True)
# np.set_printoptions()
import copy

from SV_functions import read_alamanc_file, generate_satellite_parameters_for_30_seconds, subframe_creation, \
    generate_nav_bits_for_30_seconds
from SV_functions import convert, generate_SV_Codes, constdict
import SV_functions.gps_constants as constants


def initialise_play_parameters(play_parameters):
    x, y, z = convert.gps_to_ecef(play_parameters["location_llh"][0], play_parameters["location_llh"][1],
                                  play_parameters["location_llh"][2])
    play_parameters["location_xyz"] = [x, y, z]
    play_parameters["subframes_123_from_ephemeris"] = subframe_creation.ephemeris_to_subframe(
        play_parameters["ephemeris_from_file"])
    play_parameters["almanac_struct45"] = subframe_creation.ephemeris_to_almanac_subframe(
        play_parameters["ephemeris_from_file"])

    # update ephemeris mk and toe and toc for satellite parameter calculcation
    play_parameters["ephemeris"] = generate_nav_bits_for_30_seconds.add_time_parameters_to_ephemeris(
        copy.deepcopy(play_parameters["ephemeris_from_file"]), play_parameters["scenario_time"])

    # get navigation_bits for 30 seconds , initial last previous word as all zeros
    navigation_message = generate_nav_bits_for_30_seconds.generate_nav_for_30_sec(
        play_parameters["subframes_123_from_ephemeris"],
        play_parameters["almanac_struct45"],
        play_parameters["page_id"],
        play_parameters["scenario_time"]["seconds"],
        play_parameters["scenario_time"]["Week"],
        play_parameters["ephemeris"])
    print(navigation_message[11])

    to_return = generate_nav_bits_for_30_seconds.get_1500_nav_sting(navigation_message, play_parameters["Valid PRN"])
    return to_return

def generate_next_nav_bits(play_parameters):
    # increment scenario time
    play_parameters["scenario_time"]["seconds"] = play_parameters["scenario_time"]["seconds"] + 30

    # increment page id
    play_parameters["page_id"] = play_parameters["page_id"] + 1
    if play_parameters["page_id"] == 26:
        play_parameters["page_id"] = 1

    # update ephemeris mk and toe and toc for satellite parameter calculation
    play_parameters["ephemeris"] = generate_nav_bits_for_30_seconds.add_time_parameters_to_ephemeris(
        copy.deepcopy(play_parameters["ephemeris_from_file"]), play_parameters["scenario_time"])

    # get navigation_bits for 30 seconds , initial last previous word as all zeros
    navigation_message = generate_nav_bits_for_30_seconds.generate_nav_for_30_sec(
        play_parameters["subframes_123_from_ephemeris"],
        play_parameters["almanac_struct45"],
        play_parameters["page_id"],
        play_parameters["scenario_time"]["seconds"],
        play_parameters["scenario_time"]["Week"],
        play_parameters["ephemeris"])
    to_return = generate_nav_bits_for_30_seconds.get_1500_nav_sting(navigation_message, play_parameters["Valid PRN"])
    return to_return

def get_raw_nav_bits(play_parameters=None):
    if play_parameters is None:
        play_parameters = {
            "Valid PRN": [7, 8, 27, 9, 22,16,4,3],
            "scenario_time": {
                "seconds": 209970,
                "Week": 89,
            },
            "page_id": 25,
            "previous_word": np.zeros((32, 30)),
            "satellite_parameters": {},
            "previous_satellite_parameters": {},
            "Nav_message": np.zeros((32, 1530)),
            "ephemeris_from_file": read_alamanc_file.get_alamanac(filename="UPLOADED_ALAMANAC/live_signal_almanac.txt"),
            "almanac_struct45": {},
            "subframes_123_from_ephemeris": {},
            "ephemeris": {},
            "location_llh": [18.42361111, 73.75750000, 550],
            "location_xyz": [],
            "samplerate": 5000000,
            "satellite_parameters_refresh_rate": int(1 / constants.refresh_rate),
            "satellite_parameter_iter_step": 0,
            "CA_Code": generate_SV_Codes.GPS_Code_20()
        }
    all_raw_nav_bits = np.zeros((32, 15030), dtype=np.int)
    navbit = initialise_play_parameters(play_parameters)
    print("TIME - ", play_parameters["scenario_time"]["seconds"], " -  ", len(navbit[play_parameters["Valid PRN"][0]]))
    for iter in range(9):
        next_navbit = generate_next_nav_bits(play_parameters)

        for Valid_PRN in play_parameters["Valid PRN"]:
            #print(len(navbit[Valid_PRN]))
            navbit[Valid_PRN] += next_navbit[Valid_PRN]
            #print(len(navbit[Valid_PRN]))
        print("TIME - ", play_parameters["scenario_time"]["seconds"], " -  ", len(navbit[Valid_PRN]))

    for Valid_PRN in play_parameters["Valid PRN"]:
        raw_bits_array = np.fromstring(navbit[Valid_PRN], 'u1') - ord('0')

        all_raw_nav_bits[Valid_PRN - 1, 30:15030] = raw_bits_array

    all_raw_nav_bits[all_raw_nav_bits == 0] = (-1)
    return all_raw_nav_bits

if __name__ == "__main__":
    nav_bits = get_raw_nav_bits()
