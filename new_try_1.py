# import tensorflow as tf

import numpy as np
import time
import pandas as pd
import sys

np.set_printoptions(threshold=sys.maxsize, precision=17, suppress=True)
# np.set_printoptions()
import copy

from SV_functions import read_alamanc_file, generate_satellite_parameters_for_30_seconds, subframe_creation, \
    generate_nav_bits_for_30_seconds
from SV_functions import convert, generate_SV_Codes, constdict
import SV_functions.gps_constants as constants
import generate_test_scenario

import generate_test_scenario_sat_param


play_parameters = {
    "Valid PRN": [2, 9, 6, 19, 12, 25, 5, 29, 13],
    "satellite_parameters": {},
    "Nav_message": np.zeros((32, 6530)),
    "location_llh": [40.0096856, 116.478479, 100],
    "samplerate": 5000000,
    "CA_Code": generate_SV_Codes.GPS_Code_20(),
    "satellite_parameter_iter_step" : 0,
}


def initialise_play_parameters():
    play_parameters["Nav_message"] = generate_test_scenario.get_raw_nav_bits(filename="RAW_NAV_BITS/test_navigation_bits.csv",
                                     prnlist=play_parameters["Valid PRN"])
    play_parameters["satellite_parameters"] = generate_test_scenario_sat_param.get_satellite_parameters()
initialise_play_parameters()

Multiplier = (np.arange(play_parameters["samplerate"]) + 1).astype(int)
#print(Multiplier[-1:])

print("--------- Initial Params --------")
print("Number of PRNs > 1500 = ", 0)
for sv in play_parameters["Valid PRN"]:
    print("SV - ", sv, " Bit - ", play_parameters["satellite_parameters"][0][sv]["Nav_Bit"],
          "Code Phase - ", play_parameters["satellite_parameters"][0][sv]["Code_phase"],
          "Carr Phase - ", play_parameters["satellite_parameters"][0][sv]["Carr_phase"],
          "Pseudorange - ", play_parameters["satellite_parameters"][0][sv]["Pseudorange"],
          "Doppler - ", play_parameters["satellite_parameters"][0][sv]["Dopplers"],
          "Step - ", play_parameters["satellite_parameter_iter_step"],
          )
print("-----------------")


with open("try_2.bin", 'ab+') as b:
    for iter in range(200):
        buffer = np.zeros((int(play_parameters["samplerate"]), 2))
        for sv in play_parameters["Valid PRN"]:
            CodePhase_Array = (Multiplier *
                               play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv]['Code_Phase_Step']) + \
                              play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv]["Code_phase"]

            Navbits = (np.floor((CodePhase_Array / (constants.CA_SEQ_LEN * 20)))).astype(int)
            CodePhase_Array = CodePhase_Array - Navbits * (constants.CA_SEQ_LEN * 20)
            # print(CodePhase_Array[0:20460])

            Navbits = Navbits + play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv]["Nav_Bit"]
            CarrPhase_Array = (Multiplier * play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv]['Carr_Phase_Step']) + \
                              play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv]["Carr_phase"]

            # replace the current codephase and navbit in play status
            play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"] + 1][sv][
                "Nav_Bit"] = Navbits[-1]
            #play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"] + 1][sv][
            #    "Code_phase"] = CodePhase_Array[-1]
            play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"] + 1][sv][
                "Carr_phase"] = CarrPhase_Array[-1]

            # Take actual values from CA table and Nav bits
            Actual_codes = np.take(play_parameters["CA_Code"][sv-1], CodePhase_Array.astype(int))
            Actual_nav_bits = np.take(play_parameters["Nav_message"][sv-1], Navbits.astype(int))

            # Calculation for I and Q Components
            Real_Signal = Actual_nav_bits * Actual_codes * play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv]["gain"]
            I = Real_Signal * np.cos(2 * np.pi * CarrPhase_Array)
            Q = Real_Signal * np.sin(2 * np.pi * CarrPhase_Array)

            # print(np.shape(I))
            buffer[:, 0] += I
            buffer[:, 1] += Q
        play_parameters["satellite_parameter_iter_step"] = play_parameters["satellite_parameter_iter_step"] + 1
        print("Done Iter - ", iter)
        for sv in play_parameters["Valid PRN"]:
            print("SV - ", sv, " Bit - ", play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv]["Nav_Bit"],
                  "Code Phase - ", play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv]["Code_phase"],
                  "Carr Phase - ", play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv]["Carr_phase"],
                  "Pseudorange - ", play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv]["Pseudorange"],
                  "Doppler - ", play_parameters["satellite_parameters"][play_parameters["satellite_parameter_iter_step"]][sv]["Dopplers"],
                  "Step - ", play_parameters["satellite_parameter_iter_step"],
                  )
        print("-----------------")
        b.write(np.array(buffer, dtype=np.int16))

print("Done")

